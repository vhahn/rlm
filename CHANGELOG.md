# Changelog

Noteable changes will be documented here.

## v0.3.1
### Interface
- New font.
- Darken items of different type when hovering over an item in the inventory, shop, and equipment menu.
- Add more informations to the death message.

### Balance
- Give elite bugs more external slots instead of weapon slots.

### Bugs
- Fix log message color for monsters killed by other characters than the player.

## v0.3
### Key Changes
- Add a basic tutorial
- Activatable components
- Ability to leave a planet without finishing the mission (adds a temporary penalty that lowers gained gold and shop prices)
- Added a title screen

### Content / Gameplay
- New mech types
- A new class
- New skills
- New components
- A new mission

### Interface
- Change the way the screen is transformed to a mapping onto a sphere by using shaders (faster).
- Add title screen
- Add item descriptions
- Add a welcome message for new characters

### Balance
- Increase enemy movement speed slightly.
- Reduce the Beam Cannon's accuracy to -1 and range to 3.

## v0.2.2
### Key Changes
- Use GLES2 renderer.

### Content / Gameplay
- Two new planet types: swamp and toxic
- Two new enemy types
- Several new components
- Heavy weapon mods.
- A new bounty mission.
- Most monsters have a 10% chance to spawn as elite with additional random components.
- Made default monsters a bit stronger.
- Slightly increased metal alloy price.
- Reduced missile damage and increased missile cool down time.

### Interface
- Show the status effects that weapons apply in their tooltip.

### Balance
- Reduced enemy level scaling
- Increased enemy exp. Elites give the double amount of exp.
- Increased enemy mass limit.

### Bugs
- When moving, last cursor position remains no longer highlighted.
- Update tile outlines when changing a map.
- Fixed a bug that prevented to inspect enemies after changing the map.

## v0.2.1
### Key Changes
- Moved to Godot 3.1.

### Interface
- Ability to view the components of enemies.
- Waiting centers the camera on the player.

### Bugs
- Added missing translations.
- Fixed music audio bus.

## v0.2
### Content / Gameplay
- 7 new missions.
- Add a new starting class specialized in using missiles.
- Scale exp gained from enemies with their level.
- You can no longer leave a planet while enemies are nearby.

### Balance
- Increase hit rates.
- Increase melee weapon accuracy and slightly reduce ranged weapon accuracy.
- Make space bugs a bit stronger.
- Can't pick up items when inventory is full.
- It takes more time until stealth is restored due to noise.

### Interface
- Complete GUI rework.
- Reduce delay after moving/waiting.
- Add titles for the player depending on used equipment and skills.
- Show prices in shop icons.
- Support screen drag for scrolling.
- Add a transition when moving.
- Add a save button to the main menu.
- Display game version in main menu.
- Add species, class and mission descriptions.

### Bugs
- Fixed issue where target under fire did not moved.
- Fixed issue where a weapon in cooldown can be fired.
- Save/load weapon cooldowns correctly.
- Fixed issue that allowed enemies to jump over each other.
- Fixed issue where sprites face the wrong direction while moving.
- The player and all other objects now spawn in a connected area, fixing the issue where the player spawned in water or on an island.
- Fixed an issue where the mission status was not displayed correctly or not marked as finished after loading.
- Fixed a issue where a too long path not crossing the seem of the map was generated when moving.
- Hide 'continue' menu button while dead.
- Do not hide the planet selection window.
- Clear old status icons after loading a game.
- Fixed log message shown when a status effect ends.

