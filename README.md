# Planetary Sanitations Inc.

Roguelike mech dungeon crawler where you can build a modular mech and explore planets. See https://v-ktor.itch.io/planetary-sanitations-inc for binaries.

These are the project files for Godot 3.1. It's meant to be opened or run using Godot's project manager. See https://godotengine.org/

