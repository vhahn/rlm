extends Node

const MECH_PARTS = {
	"main":[
		"light_mech","heavy_mech"],
	"arm":[
		"light_arm","medium_arm","heavy_arm"],
	"hand":[
		"dagger","long_sword","great_sword","spear",
		"chain_gun","rifle","shotgun",
		"small_shield","medium_shield","large_shield",
		"weapon_mount","heavy_weapon_mount"],
	"handle":[
		"rocket_handle","tungsten_spikes"],
	"barrel":[
		"barrel_extension","laser_sight","bayonet"],
	"weapon":[
		"missile_launcher","swarm_missile_launcher","emp_missile_launcher"],
	"leg":[
		"light_leg","heavy_leg"],
	"propulsion":[
		"foot","heavy_foot","wheels","hover_jets"],
	"head":[
		"light_head","armored_head"],
	"external":[
		"light_armor","heavy_armor","reinforcement"],
	"internal":[
		"radar","ecm","shielding_device","targeting_computer"]
}
const NON_ESSENTIAL_MECH_PARTS = [
	"small_shield","medium_shield","large_shield",
	"rocket_handle","tungsten_spikes","barrel_extension","laser_sight","bayonet",
	"light_armor","heavy_armor","reinforcement",
	"light_stealth_armor","combat_stealth_armor",
	"ecm","shielding_device","targeting_computer",
	"support_thrusters","combat_thrusters"
]
const INVENTORY_DUMMY = [
	{"type":"light_mech","amount":1},
	{"type":"light_arm","amount":2},
	{"type":"light_leg","amount":2},
	{"type":"small_shield","amount":1},
	{"type":"light_head","amount":1},
	{"type":"light_armor","amount":3}
]
const INVENTORY_RAT = [
	{"type":"rat_body","amount":1},
	{"type":"rat_head","amount":1},
	{"type":"rat_claws","amount":1},
	{"type":"rat_feet","amount":1}
]
const INVENTORY_CYBERNETIC_RAT = [
	{"type":"cybernetic_rat_body","amount":1},
	{"type":"rat_head","amount":1},
	{"type":"rat_claws","amount":1},
	{"type":"rat_feet","amount":1}
]
const INVENTORY_BAT = [
	{"type":"bat_body","amount":1},
	{"type":"bat_head","amount":1},
	{"type":"bat_teeth","amount":1},
	{"type":"bat_wings","amount":2},
	{"type":"radar","amount":1}
]
const INVENTORY_SPIDER_ACID = [
	{"type":"spider_body","amount":1},
	{"type":"spider_head","amount":1},
	{"type":"spider_teeth","amount":1},
	{"type":"spider_acid_spray","amount":1},
	{"type":"spider_feet","amount":2}
]
const INVENTORY_SPIDER_WEB = [
	{"type":"spider_body","amount":1},
	{"type":"spider_head","amount":1},
	{"type":"spider_teeth","amount":1},
	{"type":"spider_web","amount":1},
	{"type":"spider_feet","amount":2}
]
const INVENTORY_SLIME_ACID = [
	{"type":"slime_body","amount":1},
	{"type":"slime_touch","amount":1},
	{"type":"slime_acid_spray","amount":1},
	{"type":"slime_propulsion","amount":1}
]
const INVENTORY_TENTACLE = [
	{"type":"tentacle_body","amount":1},
	{"type":"slime_touch","amount":1},
	{"type":"slime_acid_spray","amount":1},
	{"type":"tentacle_spores","amount":1},
	{"type":"slime_propulsion","amount":1}
]
const INVENTORY_BUG = [
	{"type":"bug_body","amount":1},
	{"type":"bug_head","amount":1},
	{"type":"bug_teeth","amount":1},
	{"type":"bug_feet","amount":3},
	{"type":"light_armor","amount":2}
]
const INVENTORY_DRAGON = [
	{"type":"dragon_body","amount":1},
	{"type":"dragon_head","amount":1},
	{"type":"dargon_fire_breath","amount":1},
	{"type":"dragon_claws","amount":1},
	{"type":"dargon_fire_blast","amount":1},
	{"type":"dragon_wings","amount":2},
	{"type":"light_armor","amount":2},
	{"type":"targeting_computer","amount":1}
]


var attacked_from = {}


# enemy creation

func create_default(level,experience,pos,n,faction,species,equipment,s,i,d,ai_type="stupid"):
	# Create a weak enemy.
	var ofs = 0
	var ID = 1
	var enemy
	var name = n
	while Main.characters.has(name+str(ID)):
		ID += 1
	name += str(ID)
	equipment = equipment.duplicate()
	for i in range(equipment.size()):
		equipment[i] = equipment[i].duplicate()
		if equipment[i].has("level"):
			equipment[i]["level"] = max(round(equipment[i]["level"]+level*rand_range(0.8,1.0)+rand_range(-1.0,1.0)),1)
		else:
			equipment[i]["level"] = max(round(level*rand_range(0.8,1.0)+rand_range(-1.0,1.0)),1)
		if !equipment[i].has("amount"):
			equipment[i]["amount"] = 1
	
	enemy = Main.Character.new(n.to_upper(),max(round(level),1),experience*(0.5+0.5*max(level,1)),{"str":s,"int":i,"dex":d},{},{},equipment.duplicate(),Main.Part.new("empty",0,"/",null),faction,species,pos,ai_type,0,0,[],null,0,0,0.25)
	for i in range(enemy.inventory.size()):
		var equiped = false
		for _k in range(enemy.inventory[i-ofs]["amount"]):
			equiped = Main.equip(enemy,i-ofs)
		ofs += int(equiped)
	
	enemy.update()
	enemy.hp = enemy.stats["hp"]
	enemy.sp = enemy.stats["sp"]
	enemy.ep = enemy.stats["ep"]
	Main.characters[name] = enemy
	return enemy

func create_elite(level,experience,pos,n,faction,species,equipment,s,i,d,ai_type="stupid"):
	# Create a strong enemy with additional randomized equipment.
	var ofs = 0
	var ID = 1
	var enemy
	var name = n
	while Main.characters.has(name+str(ID)):
		ID += 1
	name += str(ID)
	equipment = equipment.duplicate()
	for i in range(equipment.size()):
		equipment[i] = equipment[i].duplicate()
		if equipment[i].has("level"):
			equipment[i]["level"] = max(round(equipment[i]["level"]+level*rand_range(0.8,1.1)+rand_range(-1.0,1.0)),1)
		else:
			equipment[i]["level"] = max(round(level*rand_range(0.8,1.1)+rand_range(-1.0,1.0)),1)
		if !equipment[i].has("amount"):
			equipment[i]["amount"] = 1
	
	equipment = fill_slots(equipment,level)
	enemy = Main.Character.new(n.to_upper(),max(round(level),1),experience*(0.5+0.5*max(level,1)),{"str":s,"int":i,"dex":d},{},{},equipment.duplicate(),Main.Part.new("empty",0,"/",null),faction,species,pos,ai_type,0,0,[],null,0,0,0.25)
	for i in range(enemy.inventory.size()):
		var equiped
		for _k in range(enemy.inventory[i-ofs]["amount"]):
			equiped = Main.equip(enemy,i-ofs)
		ofs += int(equiped)
	
	enemy.update()
	enemy.hp = enemy.stats["hp"]
	enemy.sp = enemy.stats["sp"]
	enemy.ep = enemy.stats["ep"]
	Main.characters[name] = enemy
	return enemy

func create_summon(level,experience,pos,n,faction,species,equipment,s,i,d,duration,ai_type="stupid"):
	# Create a weak enemy.
	var ofs = 0
	var ID = 1
	var enemy
	var name = n
	while Main.characters.has(name+str(ID)):
		ID += 1
	name += str(ID)
	equipment = equipment.duplicate()
	for i in range(equipment.size()):
		equipment[i] = equipment[i].duplicate()
		if equipment[i].has("level"):
			equipment[i]["level"] = max(round(equipment[i]["level"]+level*rand_range(0.8,1.0)+rand_range(-1.0,1.0)),1)
		else:
			equipment[i]["level"] = max(round(level*rand_range(0.8,1.0)+rand_range(-1.0,1.0)),1)
		if !equipment[i].has("amount"):
			equipment[i]["amount"] = 1
	
	enemy = Main.Character.new(n.to_upper(),max(round(level),1),experience*(0.5+0.5*max(level,1)),{"str":s,"int":i,"dex":d},{},{},equipment.duplicate(true),Main.Part.new("empty",0,"/",null),faction,species,pos,ai_type,0,0,[],null,0,0,0.25,null,true,duration)
	for i in range(enemy.inventory.size()):
		var equiped = false
		for _k in range(enemy.inventory[i-ofs]["amount"]):
			equiped = Main.equip(enemy,i-ofs)
		ofs += int(equiped)
	
	enemy.update()
	enemy.hp = enemy.stats["hp"]
	enemy.sp = enemy.stats["sp"]
	enemy.ep = enemy.stats["ep"]
	Main.characters[name] = enemy
	return enemy


# standard enemies

func create_rat(level,pos):
	if randf()<0.1:
		return create_elite(level-2,20,pos,"rat","monsters","RAT",INVENTORY_RAT,6,3,5)
	else:
		return create_default(level,10,pos,"rat","monsters","RAT",INVENTORY_RAT,6,3,5)

func create_bat(level,pos):
	if randf()<0.1:
		return create_elite(level-2,16,pos,"bat","monsters","BAT",INVENTORY_BAT,4,5,6)
	else:
		return create_default(level,8,pos,"bat","monsters","BAT",INVENTORY_BAT,4,5,6)

func create_spider(level,pos):
	if randf()<0.5:
		if randf()<0.1:
			return create_elite(level-3,30,pos,"spider","monsters","SPIDER",INVENTORY_SPIDER_ACID,5,4,6)
		else:
			return create_default(level,15,pos,"spider","monsters","SPIDER",INVENTORY_SPIDER_ACID,5,4,6)
	else:
		if randf()<0.1:
			return create_elite(level-3,30,pos,"spider","monsters","SPIDER",INVENTORY_SPIDER_WEB,5,4,6)
		else:
			return create_default(level,15,pos,"spider","monsters","SPIDER",INVENTORY_SPIDER_WEB,5,4,6)

func create_slime(level,pos):
	if randf()<0.1:
		return create_elite(level-3,34,pos,"slime","monsters","SLIME",INVENTORY_SLIME_ACID,5,6,4)
	else:
		return create_default(level,17,pos,"slime","monsters","SLIME",INVENTORY_SLIME_ACID,5,6,4)

func create_tentacle(level,pos):
	if randf()<0.1:
		return create_elite(level-4,50,pos,"tentacle","monsters","SLIME",INVENTORY_TENTACLE,6,9,4)
	else:
		return create_default(level,25,pos,"tentacle","monsters","SLIME",INVENTORY_TENTACLE,6,9,4)

func create_bug(level,pos):
	if randf()<0.1:
		return create_elite(level-3,40,pos,"bug","monsters","BUG",INVENTORY_BUG,6,4,4)
	else:
		return create_default(level,20,pos,"bug","monsters","BUG",INVENTORY_BUG,6,4,4)

func create_dragon(level,pos):
	if randf()<0.1:
		return create_elite(level-4,56,pos,"dragon","monsters","DRAGON",INVENTORY_DRAGON,8,8,6)
	else:
		return create_default(level,28,pos,"dragon","monsters","DRAGON",INVENTORY_DRAGON,8,8,6)

# elite enemies

func create_cybernetic_rat(level,pos):
	return create_elite(level,45,pos,"rat","monsters","RAT",INVENTORY_CYBERNETIC_RAT,8,4,6)

# dummy

func create_dummy(level,pos):
	return create_default(level,4,pos,"dummy","robot","DUMMY",INVENTORY_DUMMY,1,1,1,"dummy")



# Create a mech with random equipment.

func fill_slots(equipment,level):
	# Fill all slots with random equipment.
	var used_slots = {}
	var ID = 0
	equipment = equipment.duplicate()
	for eq in equipment:
		# Count slots already occupied by predefined equipment.
		if !Parts.items[eq["type"]].has("slot"):
			continue
		var s = Parts.items[eq["type"]]["slot"]
		if used_slots.has(s):
			used_slots[s] += 1
		else:
			used_slots[s] = 1
	while ID<equipment.size():
		# Fill slots of following parts.
		if Parts.items[equipment[ID]["type"]].has("slots"):
			for s in Parts.items[equipment[ID]["type"]]["slots"]:
				if used_slots.has(s) && used_slots[s]>0:
					used_slots[s] -= 1
					continue
				if MECH_PARTS.has(s):
					# Add a random component.
					equipment.push_back({"type":MECH_PARTS[s][randi()%MECH_PARTS[s].size()],"level":max(round(level*rand_range(0.8,1.1)+rand_range(-1.0,1.0)),1)})
		ID += 1
	return equipment

func create_mech(level,pos,target_value,faction,name="mech",equipment=[]):
	var level_scale
	var remove = []
	var value = 0
	var has_main = false
	equipment = equipment.duplicate()
	for eq in equipment:
		if Parts.items[eq["type"]]["slot"]=="main":
			has_main = true
			break
	if !has_main:
		# Add a chassis.
		equipment.push_back({"type":MECH_PARTS["main"][randi()%MECH_PARTS["main"].size()],"level":level})
	equipment = fill_slots(equipment,level)
	# Scale component levels in order to reach the target price.
	for eq in equipment:
		value += Parts.get_total_price(eq)
	level_scale = clamp(target_value/value,1.0/level,1.5)
	value = 0
	for i in range(equipment.size()):
		equipment[i] = equipment[i].duplicate()
		equipment[i]["level"] = max(round(equipment[i]["level"]*level_scale*rand_range(0.9,1.1)),1)
		value += Parts.get_total_price(equipment[i])
	if value>target_value:
		# Remove some unimportant components in order to reach the target price.
		for i in range(equipment.size()):
			if equipment[i]["type"] in NON_ESSENTIAL_MECH_PARTS:
				remove.push_back(i)
		while 0.9*value>target_value && remove.size()>0:
			var i = randi()%remove.size()
			value -= Parts.get_total_price(equipment[remove[i]])
			equipment.remove(remove[i])
			for j in range(i+1,remove.size()):
				remove[j] -= 1
			remove.remove(i)
	if value>target_value:
		# Rescale component levels in order to reach the target price.
		level_scale = clamp(target_value/value,1.0/level,1.0)
		value = 0
		for eq in equipment:
			eq["level"] = max(round(eq["level"]*level_scale*rand_range(0.9,1.1)),1)
			value += Parts.get_total_price(eq)
	
	return create_default(level,50*(1.0+value/1000.0),pos,name,faction,"MECH",equipment,int(10+level/4),int(10+level/4),int(10+level/4))


# AI

func stupid(name):
	# walk to next hostile object and attack when within attack range
	# wait or walk randomly when no hostile objects visible
	var hostile = []
	var min_dist = 100
	var nearest
	var faction = Main.characters[name].faction
	var position = Main.characters[name].position
	var attacks = []
	var to_dist = 100
	var target_pos
	var dir
	var to_pos
	var path
	
	# find visible hostile objects
	for nm in Main.characters.keys():
		if Main.characters[nm].faction!=faction && Main.is_visible(name,nm):
			var dist = Main.get_distance(position,Main.characters[nm].position)
			hostile.push_back(nm)
			if dist<min_dist:
				nearest = nm
				min_dist = dist
	
	if hostile.size()<1:
		# no hostile objects visible
		if attacked_from.has(name):
			var pos = attacked_from[name][randi()%attacked_from[name].size()]
			var dist = Main.get_distance(position,pos)
			attacked_from.erase(name)
			if randf()<0.33:
				# attack blind
				attacks = []
				for action in Main.characters[name].actions:
					if action["type"]=="attack" && (!action.has("ep_cost") || Main.characters[name].ep>=action["ep_cost"]) && dist<=action["range"]:
						attacks.push_back(action)
				if attacks.size()>0 && Main.get_character_at_pos(pos)!=null:
					Main.attack(name,Main.get_character_at_pos(pos),attacks[randi()%attacks.size()])
					return
			elif randf()<0.5 && Main.characters[name].movement_time<10.0:
				# move to where the attack came from
				dir = (pos-Main.characters[name].position)
				if abs(dir.x)>abs(dir.x-Generate.width):
					dir.x -= Generate.width
				elif abs(dir.x)>abs(dir.x+Generate.width):
					dir.x += Generate.width
				if abs(dir.y)>abs(dir.y-Generate.height):
					dir.y -= Generate.height
				elif abs(dir.y)>abs(dir.y+Generate.height):
					dir.y += Generate.height
				dir = dir.normalized()
				dir = Vector2(round(dir.x+rand_range(-0.25,0.25)),round(dir.y+rand_range(-0.25,0.25)))
				
				Main.move(name,dir)
		elif randf()<0.5 && Main.characters[name].movement_time<10.0:
			Main.move(name,Generate.DIRECTIONS[randi()%Generate.DIRECTIONS.size()])
		
		if Main.characters[name].action==null:
			Main.wait(name)
		return
	
	target_pos = Main.characters[nearest].position
	for action in Main.characters[name].actions:
		if action["type"]=="attack" && (!action.has("ep_cost") || Main.characters[name].ep>=action["ep_cost"]) && min_dist<=action["range"]:
			attacks.push_back(action)
	if attacks.size()>0:
		# use a random available attack skill
		Main.attack(name,nearest,attacks[randi()%attacks.size()])
		return
	
	# find a free position next to the target
	for p in Generate.DIRECTIONS:
		var pos = target_pos+p
		pos.x = fposmod(pos.x,Generate.width)
		pos.y = fposmod(pos.y,Generate.height)
		if !Main.is_empty(pos):
			continue
		
		var dist = Main.get_distance(position,pos)
		if dist<to_dist:
			to_pos = pos
			to_dist = dist
	
	if to_pos!=null:
		# find path to target position
		path = Generate._get_path(Main.characters[name],to_pos)
		if path.size()>1:
			Main.characters[name].path = Array(path)
			Main.characters[name].path.pop_front()
	
	if dir!=null && Main.characters[name].movement_time<10.0:
		Main.move(name,dir)
	
	if Main.characters[name].action==null:
		Main.wait(name)

func dummy(_name):
	# Does absolutely nothing.
	pass
