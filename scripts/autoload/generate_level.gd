extends Node

const TILE_VEC_X = Vector2(1.92,1.11)
const TILE_VEC_Y = Vector2(-1.92,1.11)
const DIRECTIONS = [Vector2(-1,-1),Vector2(-1,0),Vector2(0,-1),Vector2(1,1),Vector2(0,1),Vector2(1,0)]

const OBSTACLES = {
	"grass01":["tree01","tree02","tree03","tree04","mountains01"],
	"dirt01":["tree01","tree02","mountains01"],
	"desert01":["cactus01","cactus02"],
	"tundra01":["tree01_snow","tree02_snow","mountains01_snow"],
	"rock01":["mountains01"]
}
const SHOP_TILES = {
	"city":["building01"]
}
const OBSTACLE_CHANCE = {
	"tundra01":0.05,"dirt01":0.1,"grass01":0.2,"desert01":0.05,"rock01":0.1
}
const TILES_UNPASSABLE = ["ocean01","lava01","toxic01"]
const ZONE_TILES = {
	"terran":["tundra01","dirt01","grass01","grass01","dirt01","desert01","grass01","ocean01","grass01","grass01","tundra01","tundra01"],
	"warm":["tundra01","ocean01","ocean01","desert01","dirt01","grass01","grass01","rock01","tundra01","ocean01","tundra01"],
	"desert":["ocean01","desert01","rock01","desert01","desert01","dirt01","ocean01"],
	"ice_ball":["tundra01","tundra01","tundra01","dirt01","grass01","ocean01","tundra01","tundra01","dirt01","tundra01","tundra01","tundra01"],
	"lava":["lava01","lava01","rock01","desert01","rock01","lava01"],
	"swamp":["ocean01","swamp01","swamp01","grass01","swamp01","dirt01","swamp01","ocean01"],
	"toxic":["toxic01","swamp01","dirt01","swamp01","toxic01","swamp01","dirt01","swamp01","toxic01"],
	"city":["metal01"]
}
const DROPS_STD = [
	"metal_alloys","metal_alloys","metal_alloys","metal_alloys",
	"dagger","short_sword","long_sword","great_sword",
	"rocket_handle","tungsten_spikes","blade_armor",
	"chain_gun","rifle",
	"barrel_extension","silencer","laser_sight","bayonet",
	"missile_launcher","swarm_missile_launcher","emp_missile_launcher",
	"shock_absorber","mass_optimization",
	"weapon_mount","heavy_weapon_mount",
	"small_shield",
	"light_mech","light_tank",
	"light_arm","medium_arm","heavy_arm",
	"light_leg","heavy_leg",
	"light_head","armored_head",
	"light_armor","light_armor","light_stealth_armor","light_stealth_armor","reinforcement",
	"foot","wheels","hover_jets",
	"radar","ecm","targeting_computer","battery",
	"support_thrusters",
	"smoke_bomb","emergency_shield","portable_turret"
]
const DROPS_RARE = [
	"metal_alloys",
	"rapier","axe",
	"spear","scythe",
	"shock_discharger",
	"pulse_laser",
	"sniper_rifle","laser_rifle","shotgun",
	"heavy_bayonet","incendiary_ammo","piercing_ammo",
	"incendiary_missile_launcher","heavy_missile_launcher",
	"reloading_mechanism",
	"large_weapon_mount",
	"heavy_mech","scout_mech","tripple_mech",
	"medium_shield","large_shield",
	"turret_head",
	"large_arm",
	"shoulder",
	"heavy_foot",
	"heavy_armor","combat_stealth_armor",
	"shielding_device","large_battery",
	"combat_thrusters",
	"large_portable_turret","attack_drone"
]
const MECHS = ["light_mech","light_tank","heavy_mech","scout_mech","tripple_mech"]
const COMMON_DROPS = {
	"terran":[]+DROPS_STD,
	"warm":[]+DROPS_STD,
	"desert":[]+DROPS_STD+DROPS_RARE+["acid_blaster","shock_discharger","combat_thrusters"],
	"ice_ball":[]+DROPS_STD+DROPS_RARE+["flamer","freezer","scythe"],
	"swamp":[]+DROPS_STD+DROPS_RARE+["piercing_ammo","spear","turret_head"],
	"lava":[]+DROPS_STD+DROPS_RARE+["flamer","freezer","incendiary_ammo","reloading_mechanism"],
	"toxic":[]+DROPS_STD+DROPS_RARE+["rapier","axe","beam_cannon","reloading_mechanism"],
	"city":[]+DROPS_STD+MECHS
}
const GOLD = {
	"terran":20,
	"warm":40,
	"desert":70,
	"ice_ball":70,
	"swamp":85,
	"lava":100,
	"toxic":120,
	"city":0
}


var ground_tiles = {}
var wall_tiles = {}
var blocked = {}
var regions = []
var shops = {}
var items = {}
var control_points = []
var astar
var width = 0
var height = 0
var drops = []
var gold = 0
var level = 1
var type
var main_region = []

var tile_button = preload("res://scenes/tiles/button.tscn")


func is_walkable(pos):
	pos = Vector2(fposmod(pos.x,width),fposmod(pos.y,height))
	return ground_tiles.has(pos) && (!blocked.has(pos) || !blocked[pos]) && !wall_tiles.has(pos)


func update_tiles():
	# Move tiles to the right position on the screen.
	for pos in ground_tiles.keys():
		var t = Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y))
		var screen_pos = pos-Main.position
		if abs(screen_pos.x)>abs(screen_pos.x-width):
			screen_pos.x -= width
		elif abs(screen_pos.x)>abs(screen_pos.x+width):
			screen_pos.x += width
		if abs(screen_pos.y)>abs(screen_pos.y-height):
			screen_pos.y -= height
		elif abs(screen_pos.y)>abs(screen_pos.y+height):
			screen_pos.y += height
		screen_pos += Main.position
		
		t.position = 16*(screen_pos.x*TILE_VEC_X+screen_pos.y*TILE_VEC_Y)
		if Main.get_character_at_pos(pos)==null:
			t.get_node("Outlines").hide()
	
	for character in Main.characters.keys():
		var c = Main.characters[character]
		var t = "Tile_"+str(c.position.x)+"_"+str(c.position.y)
		var visible = Main.is_visible("player",character)
		if c.node!=null && Main.tiles.has_node(t):
			t = Main.tiles.get_node(t)
			c.node.position = t.get_position()
		if visible && ((!c.node.is_visible() && c.node.get_node("Animation").current_animation!="fade_in") || c.node.get_node("Animation").current_animation=="fade_out"):
			c.node.get_node("Animation").clear_queue()
			c.node.get_node("Animation").queue("fade_in")
		elif !visible && ((c.node.is_visible() && c.node.get_node("Animation").current_animation!="fade_out") || c.node.get_node("Animation").current_animation=="fade_in"):
			c.node.get_node("Animation").clear_queue()
			c.node.get_node("Animation").queue("fade_out")


func update_map():
	astar = AStar.new()
	for x in range(width):
		for y in range(height):
			var t = ground_tiles[Vector2(x,y)]
			blocked[Vector2(x,y)] = wall_tiles.has(Vector2(x,y)) || (t in TILES_UNPASSABLE)
			if !blocked[Vector2(x,y)]:
				astar.add_point(x+y*width,Vector3(x,y,0))
	
	for pos in ground_tiles.keys():
		var ti = load("res://scenes/tiles/"+ground_tiles[pos]+".tscn")
		if ti==null || !ti.can_instance():
			continue
		
		var bi = tile_button.instance()
		var oi = Sprite.new()
		ti = ti.instance()
		ti.set_name("Tile_"+str(pos.x)+"_"+str(pos.y))
		ti.set_z_index(-1)
		oi.set_name("Outlines")
		oi.set_texture(preload("res://images/ui/tile_outlines.png"))
		oi.hide()
		ti.add_child(oi)
		ti.add_child(bi)
		bi.connect("pressed",Main,"_tile_selected",[pos])
		bi.connect("mouse_entered",Main,"_tile_hover",[pos])
		bi.connect("mouse_exited",Main,"_tile_clear",[pos])
		bi.connect("gui_input",bi,"input",[pos])
		Main.tiles.add_child(ti)
		for p in DIRECTIONS:
			var pos2 = pos+p
			pos2.x = fposmod(pos2.x,width)
			pos2.y = fposmod(pos2.y,height)
			if !blocked[pos] && !blocked[pos2]:
				astar.connect_points(pos.x+pos.y*width,pos2.x+pos2.y*width,true)
	create_regions()
	for pos in wall_tiles.keys():
		if !Main.tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)):
			continue
		var ti = load("res://scenes/tiles/"+wall_tiles[pos]+".tscn")
		if ti==null || !ti.can_instance():
			continue
		
		ti = ti.instance()
		ti.set_z_index(1)
		ti.position = Vector2(rand_range(0.0,8.0),0.0).rotated(2.0*PI*randf())
		ti.position = Vector2(round(ti.get_position().x),round(ti.get_position().y))
		Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)).add_child(ti)
	
	if has_method("environment_"+type):
		call("environment_"+type)

func add_item(item):
	var pos
	for _i in range(100):
		pos = Vector2(randi()%width,randi()%height)
		if is_walkable(pos) && Main.is_empty(pos):
			break
	
	Main._drop_item(pos,item)
	return pos

func add_control_points():
	for pos in control_points:
		var ii = Sprite.new()
		ii.set_texture(preload("res://images/ui/control_point.png"))
		ii.set_name("ControlPoint")
		Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)).add_child(ii)


func is_in_any_region(pos):
	for r in regions:
		if pos in r:
			return true
	return false

func get_region(pos):
	for r in regions:
		if pos in r:
			return r

func create_regions():
	# Create an array containing disjoint sets of connected tile's coordinates (exclude blocked/unpassable tiles).
	regions.clear()
	main_region.clear()
	for x in range(width):
		for y in range(height):
			var pos = Vector2(x,y)
			var added = false
			if blocked[pos] || is_in_any_region(pos):
				continue
			
			for p in DIRECTIONS:
				var pos2 = pos+p
				pos2.x = fposmod(pos2.x,width)
				pos2.y = fposmod(pos2.y,height)
				if !blocked[pos2]:
					var r = get_region(pos2)
					if r!=null:
						r.push_back(pos)
						added = true
						break
			if !added:
				regions.push_back([pos])
	
	# Find the largest region. This is were the player, enemies, items, etc. are placed.
	for r in regions:
		if r.size()>main_region.size():
			main_region = r

func generate_default(level_type):
	astar = AStar.new()
	ground_tiles.clear()
	wall_tiles.clear()
	blocked.clear()
	shops.clear()
	items.clear()
	regions.clear()
	main_region.clear()
	for x in range(width):
		for y in range(height):
			var num_zones = ZONE_TILES[level_type].size()-1
			var zone = 1.0*y/height
			var w = 2.0*abs(1.0*x/width-0.5)
			var t
			zone = zone*(1.0-w)+0.5*w
			zone = floor(clamp(zone+rand_range(-0.05,0.05),0.0,1.0)*num_zones)
			t = ZONE_TILES[level_type][zone]
			
			ground_tiles[Vector2(x,y)] = t
			if OBSTACLES.has(t) && OBSTACLE_CHANCE.has(t) && randf()<OBSTACLE_CHANCE[t]:
				wall_tiles[Vector2(x,y)] = OBSTACLES[t][randi()%OBSTACLES[t].size()]
				blocked[Vector2(x,y)] = true
			else:
				blocked[Vector2(x,y)] = t in TILES_UNPASSABLE
			if !blocked[Vector2(x,y)]:
				astar.add_point(x+y*width,Vector3(x,y,0))
	drops = []+COMMON_DROPS[level_type]
	gold = GOLD[level_type]
	Main.safe = false
	Main.cleared = false
	
	for pos in ground_tiles.keys():
		var ti = load("res://scenes/tiles/"+ground_tiles[pos]+".tscn")
		if ti==null || !ti.can_instance():
			continue
		
		var bi = tile_button.instance()
		var oi = Sprite.new()
		ti = ti.instance()
		ti.set_name("Tile_"+str(pos.x)+"_"+str(pos.y))
		ti.set_z_index(-1)
		oi.set_name("Outlines")
		oi.set_texture(preload("res://images/ui/tile_outlines.png"))
		oi.set_self_modulate(Color(1.0,1.0,1.0,0.5))
		oi.hide()
		ti.add_child(oi)
		ti.add_child(bi)
		bi.connect("pressed",Main,"_tile_selected",[pos])
		bi.connect("mouse_entered",Main,"_tile_hover",[pos])
		bi.connect("mouse_exited",Main,"_tile_clear",[pos])
		bi.connect("gui_input",bi,"input",[pos])
		Main.tiles.add_child(ti)
		for p in DIRECTIONS:
			var pos2 = pos+p
			pos2.x = fposmod(pos2.x,width)
			pos2.y = fposmod(pos2.y,height)
			if !blocked[pos] && !blocked[pos2]:
				astar.connect_points(pos.x+pos.y*width,pos2.x+pos2.y*width,true)
	create_regions()
	if level_type=="city":
		for _i in range(10):
			var shop_items = []
			var pos = get_random_position()
			shop_items.resize(randi()%10+10)
			for i in range(shop_items.size()):
				var t = randi()%drops.size()
				shop_items[i] = {"type":drops[t],"level":round(max(level*rand_range(0.8,1.25)+rand_range(-1.0,2.0),1)),"amount":1}
				if Parts.items[drops[t]].has("hp"):
					shop_items[i]["hp"] = Parts.items[drops[t]]["hp"]
			shops[pos] = shop_items
			if wall_tiles.has(pos):
				wall_tiles.erase(pos)
			blocked[pos] = false
	for pos in wall_tiles.keys():
		if !Main.tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)):
			continue
		var ti = load("res://scenes/tiles/"+wall_tiles[pos]+".tscn")
		if ti==null || !ti.can_instance():
			continue
		
		ti = ti.instance()
		ti.set_z_index(1)
		ti.position = Vector2(rand_range(0.0,8.0),0.0).rotated(2.0*PI*randf())
		ti.position = Vector2(round(ti.get_position().x),round(ti.get_position().y))
		Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)).add_child(ti)
	
	if level_type!="city":
		for _i in range(int(rand_range(5,10))):
			var pos = get_random_position()
			var t = randi()%drops.size()
			var item = {"type":drops[t],"level":round(max(level*rand_range(0.8,1.25)+rand_range(-1.0,2.0),1)),"amount":1}
			if Parts.items[drops[t]].has("hp"):
				item["hp"] = Parts.items[drops[t]]["hp"]
			for _j in range(100):
				if is_walkable(pos) && Main.is_empty(pos):
					break
				else:
					pos = get_random_position()
			Main._drop_item(pos,item,item["amount"])


func get_random_position():
	# Return a random position from the main region or a completely random position if the main region is empty.
	var pos
	if main_region.size()>0:
		pos = main_region[randi()%main_region.size()]
	else:
		pos = Vector2(randi()%width,randi()%height)
	return pos

func spawn_enemy(enemy_type,enemy_level,args=[]):
	var pos = get_random_position()
	for _i in range(100):
		if is_walkable(pos) && Main.is_empty(pos):
			break
		else:
			pos = get_random_position()
	
	if Enemy.has_method("create_"+enemy_type):
		return Enemy.callv("create_"+enemy_type,[enemy_level,pos]+args)


### path finding

func create_astar(f,t,ignore_obstacles=false):
	# Create an AStar instance with the center located between positions f and t.
	var offset
	var ast = AStar.new()
	var from = Vector2()
	var to = Vector2()
	var center = (f+t)/2
	center.x = int(center.x)
	center.y = int(center.y)
	if abs(f.x-t.x)>int(abs(f.x-t.x+width)+width)%width || abs(f.x-t.x)>int(abs(t.x-f.x+width)+width)%width:
		center.x = int((f.x+t.x+width)/2)%width
	if abs(f.y-t.y)>int(abs(f.y-t.y+height)+height)%height || abs(f.y-t.y)>int(abs(t.y-f.y+height)+height)%height:
		center.y = int((f.y+t.y+height)/2)%height
	offset = center-Vector2(width/2,height/2)
	from.x = int(f.x-offset.x+width)%width
	from.y = int(f.y-offset.y+height)%height
	to.x = int(t.x-offset.x+width)%width
	to.y = int(t.y-offset.y+height)%height
	
	for x0 in range(width):
		var x = int(x0+width-from.x)%width
		for y0 in range(height):
			var y = int(y0+height-from.y)%height
			if !blocked[Vector2(x,y)]:
				ast.add_point(x+y*width,Vector3(x,y,0))
	for pos0 in ground_tiles.keys():
		var pos = pos0-from
		pos.x = int(pos.x+width)%width
		pos.y = int(pos.y+height)%height
		for p in DIRECTIONS:
			var pos2 = pos+p
			pos2.x = int(pos2.x+width)%width
			pos2.y = int(pos2.y+height)%height
			if !blocked[pos] && !blocked[pos2] && (ignore_obstacles || (Main.is_empty(pos) && Main.is_empty(pos2))):
				ast.connect_points(pos.x+pos.y*width,pos2.x+pos2.y*width,true)
	
	return [ast,offset]

func _get_path(character,t,ignore_obstacles=false):
	# Find the shortest path between to points. Returns an array of tile positions.
	var path
	var p0
	var p1
	var offset
	var f = character.position
	var from = Vector2()
	var to = Vector2()
	var center = (f+t)/2
	center.x = int(center.x)
	center.y = int(center.y)
	if abs(f.x-t.x)>int(abs(f.x-t.x+width)+width)%width || abs(f.x-t.x)>int(abs(t.x-f.x+width)+width)%width:
		center.x = int((f.x+t.x+width)/2)%width
	if abs(f.y-t.y)>int(abs(f.y-t.y+height)+height)%height || abs(f.y-t.y)>int(abs(t.y-f.y+height)+height)%height:
		center.y = int((f.y+t.y+height)/2)%height
	offset = center-Vector2(width/2,height/2)
	if character.astar==null || offset.distance_squared_to(character.astar_offset)>min(width*width,height*height)/4:
		# create a new astar instance
		var data = create_astar(f,t,ignore_obstacles)
		character.astar = data[0]
		character.astar_offset = data[1]
	else:
		# use the existing astar instance
		offset = character.astar_offset
	from.x = int(f.x-offset.x+width)%width
	from.y = int(f.y-offset.y+height)%height
	to.x = int(t.x-offset.x+width)%width
	to.y = int(t.y-offset.y+height)%height
	p0 = from.x+from.y*width
	p1 = to.x+to.y*width
	
	path = Array(character.astar.get_id_path(p0,p1))
	for i in range(path.size()):
		var v = Vector2(path[i]%width,int(path[i]/width))
		v.x = int(v.x+offset.x)%width
		v.y = int(v.y+offset.y)%height
		path[i] = v
	if path==null || path.size()==0:
		path = Array(astar.get_id_path(f.x+f.y*width,t.x+t.y*width))
		for i in range(path.size()):
			var v = Vector2(path[i]%width,int(path[i]/width))
			path[i] = v
	if path==null || path.size()==0:
		path = get_direct_path(f,t)
	
	return path

func get_direct_path(from,to):
	var path = []
	var length = int(Main.get_distance(from,to))
	path.resize(length+1)
	path[0] = from
	path[length] = to
	if abs(from.x-to.x-Generate.width)<abs(from.x-to.x):
		to.x += Generate.width
	elif abs(from.x-to.x+Generate.width)<abs(from.x-to.x):
		to.x -= Generate.width
	if abs(from.y-to.y-Generate.height)<abs(from.y-to.y):
		to.y += Generate.height
	elif abs(from.y-to.y+Generate.height)<abs(from.y-to.y):
		to.y -= Generate.height
	
	for i in range(1,length):
		var delta = (from+(to-from)*i/length-path[i-1]).normalized()
		delta.x = round(delta.x)
		delta.y = round(delta.y)
		if delta.x==-delta.y:
			if (path[i-1]+Vector2(delta.x,0)).distance_squared_to(from+(to-from)*i/length)<(path[i-1]+Vector2(0,delta.y)).distance_squared_to(from+(to-from)*i/length):
				delta.y = 0
			else:
				delta.x = 0
		path[i] = path[i-1]+delta
		if path[i].x<0:
			path[i].x += width
		elif path[i].x>=width:
			path[i].x -= width
		if path[i].y<0:
			path[i].y += height
		elif path[i].y>=height:
			path[i].y -= height
	return path

###

func generate_terran(lvl):
	level = lvl
	type = "terran"
	width = 40
	height = 40
	generate_default("terran")
	environment_terran()
	update_tiles()
	for _i in range(5):
		spawn_enemy("rat",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(3):
		spawn_enemy("bat",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(3):
		spawn_enemy("spider",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(3):
		spawn_enemy("slime",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))

func environment_terran():
	Main.get_node("Atmosphere").set_modulate(Color(0.2,0.6,1.0,1.0))
	Main.get_node("Atmosphere").material.set_shader_param("light",Color(0.8,0.8,0.6,0.5))
	Main.get_node("Atmosphere").material.set_shader_param("shadow",Color(0.0,0.3,0.5,0.5))
	Main.get_node("TextureRect").material.set_shader_param("light",Color(0.8,0.8,0.6,0.5))
	Main.get_node("TextureRect").material.set_shader_param("shadow",Color(0.0,0.3,0.5,0.5))
	Music.change_music("exploration/09_green_forest.ogg")
	Main.safe = false

func generate_warm(lvl):
	level = lvl
	type = "warm"
	width = 40
	height = 40
	generate_default("warm")
	environment_terran()
	update_tiles()
	for _i in range(4):
		spawn_enemy("rat",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(3):
		spawn_enemy("bat",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(4):
		spawn_enemy("spider",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(3):
		spawn_enemy("bug",max(randi()%int(4+0.2*lvl)+0.8*lvl-3,0))

func environment_warm():
	Main.get_node("Atmosphere").set_modulate(Color(0.2,0.7,0.9,1.0))
	Main.get_node("Atmosphere").material.set_shader_param("light",Color(0.8,0.8,0.6,0.6))
	Main.get_node("Atmosphere").material.set_shader_param("shadow",Color(0.0,0.3,0.5,0.5))
	Main.get_node("TextureRect").material.set_shader_param("light",Color(0.8,0.8,0.6,0.6))
	Main.get_node("TextureRect").material.set_shader_param("shadow",Color(0.0,0.3,0.5,0.5))
	Music.change_music("exploration/07_high_in_the_mountains.ogg")
	Main.safe = false

func generate_desert(lvl):
	level = lvl
	type = "desert"
	width = 40
	height = 40
	generate_default("desert")
	environment_desert()
	update_tiles()
	for _i in range(5):
		spawn_enemy("bat",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(4):
		spawn_enemy("bug",max(randi()%int(4+0.2*lvl)+0.8*lvl-3,0))
	for _i in range(2):
		spawn_enemy("dragon",max(randi()%int(4+0.2*lvl)+0.8*lvl-5,0))

func environment_desert():
	Main.get_node("Atmosphere").set_modulate(Color(0.9,0.8,0.5,1.0))
	Main.get_node("Atmosphere").material.set_shader_param("light",Color(0.8,0.8,0.4,0.75))
	Main.get_node("Atmosphere").material.set_shader_param("shadow",Color(0.4,0.3,0.0,0.5))
	Main.get_node("TextureRect").material.set_shader_param("light",Color(0.8,0.8,0.4,0.75))
	Main.get_node("TextureRect").material.set_shader_param("shadow",Color(0.4,0.3,0.0,0.5))
	Music.change_music("exploration/08_endless_sands.ogg")
	Main.safe = false

func generate_ice_ball(lvl):
	level = lvl
	type = "ice_ball"
	width = 40
	height = 40
	generate_default("ice_ball")
	environment_terran()
	update_tiles()
	for _i in range(4):
		spawn_enemy("rat",max(randi()%int(4+0.2*lvl)+0.8*lvl-1,0))
	for _i in range(4):
		spawn_enemy("spider",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(2):
		spawn_enemy("dragon",max(randi()%int(4+0.2*lvl)+0.8*lvl-5,0))

func environment_ice_ball():
	Main.get_node("Atmosphere").set_modulate(Color(0.4,0.7,0.9,1.0))
	Main.get_node("Atmosphere").material.set_shader_param("light",Color(0.8,0.8,0.6,0.25))
	Main.get_node("Atmosphere").material.set_shader_param("shadow",Color(0.0,0.3,0.5,0.5))
	Main.get_node("TextureRect").material.set_shader_param("light",Color(0.8,0.8,0.6,0.25))
	Main.get_node("TextureRect").material.set_shader_param("shadow",Color(0.0,0.3,0.5,0.5))
	Music.change_music("exploration/05_gaseous_tethanus.ogg")
	Main.safe = false

func generate_lava(lvl):
	level = lvl
	type = "lava"
	width = 40
	height = 40
	generate_default("lava")
	environment_lava()
	update_tiles()
	for _i in range(4):
		spawn_enemy("rat",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(4):
		spawn_enemy("bug",max(randi()%int(4+0.2*lvl)+0.8*lvl-3,0))
	for _i in range(4):
		spawn_enemy("dragon",max(randi()%int(4+0.2*lvl)+0.8*lvl-4,0))

func environment_lava():
	Main.get_node("Atmosphere").set_modulate(Color(1.0,0.4,0.2,1.0))
	Main.get_node("Atmosphere").material.set_shader_param("light",Color(0.8,0.6,0.5,0.6))
	Main.get_node("Atmosphere").material.set_shader_param("shadow",Color(0.5,0.1,0.0,0.7))
	Main.get_node("TextureRect").material.set_shader_param("light",Color(0.8,0.6,0.5,0.6))
	Main.get_node("TextureRect").material.set_shader_param("shadow",Color(0.5,0.1,0.0,0.7))
	Music.change_music("exploration/02_lava_city.ogg")
	Main.safe = false

func generate_swamp(lvl):
	level = lvl
	type = "swamp"
	width = 40
	height = 40
	generate_default("swamp")
	environment_swamp()
	update_tiles()
	for _i in range(4):
		spawn_enemy("bat",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(2):
		spawn_enemy("bug",max(randi()%int(4+0.2*lvl)+0.8*lvl-3,0))
	for _i in range(4):
		spawn_enemy("tentacle",max(randi()%int(4+0.2*lvl)+0.8*lvl-6,0))

func environment_swamp():
#	var sun = Light2D.new()
	Main.get_node("Atmosphere").set_modulate(Color(0.3,0.9,0.7,1.0))
	Main.get_node("Atmosphere").material.set_shader_param("light",Color(0.7,0.8,0.6,0.4))
	Main.get_node("Atmosphere").material.set_shader_param("shadow",Color(0.3,0.5,0.0,0.4))
	Main.get_node("TextureRect").material.set_shader_param("light",Color(0.7,0.8,0.6,0.4))
	Main.get_node("TextureRect").material.set_shader_param("shadow",Color(0.3,0.5,0.0,0.4))
	Music.change_music("exploration/04_underground_cavern.ogg")
	Main.safe = false

func generate_toxic(lvl):
	level = lvl
	type = "toxic"
	width = 40
	height = 40
	generate_default("toxic")
	environment_toxic()
	update_tiles()
	for _i in range(6):
		spawn_enemy("tentacle",max(randi()%int(4+0.2*lvl)+0.8*lvl-2,0))
	for _i in range(6):
		spawn_enemy("bat",max(randi()%int(4+0.2*lvl)+0.8*lvl+2,0))

func environment_toxic():
	Main.get_node("Atmosphere").set_modulate(Color(0.5,1.0,0.3,1.0))
	Main.get_node("Atmosphere").material.set_shader_param("light",Color(0.6,0.8,0.5,0.5))
	Main.get_node("Atmosphere").material.set_shader_param("shadow",Color(0.0,0.5,0.2,0.6))
	Main.get_node("TextureRect").material.set_shader_param("light",Color(0.6,0.8,0.5,0.5))
	Main.get_node("TextureRect").material.set_shader_param("shadow",Color(0.0,0.5,0.2,0.6))
	Music.change_music("exploration/10_running_wild.ogg")
	Main.safe = false

func generate_city(lvl):
	level = lvl
	type = "city"
	width = 30
	height = 30
	generate_default("city")
	environment_city()
	update_tiles()

func environment_city():
	Main.get_node("Atmosphere").set_modulate(Color(0.8,0.8,0.6,1.0))
	Main.get_node("Atmosphere").material.set_shader_param("light",Color(0.8,0.8,0.6,0.5))
	Main.get_node("Atmosphere").material.set_shader_param("shadow",Color(0.0,0.3,0.5,0.5))
	Main.get_node("TextureRect").material.set_shader_param("light",Color(0.8,0.8,0.6,0.5))
	Main.get_node("TextureRect").material.set_shader_param("shadow",Color(0.0,0.3,0.5,0.5))
	Music.change_music("exploration/03_ship_interior.ogg")
	Main.safe = true
	Main.cleared = true
	if SHOP_TILES.has("city"):
		for pos in shops.keys():
			if !Main.tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)):
				continue
			var ti = load("res://scenes/tiles/"+SHOP_TILES["city"][randi()%SHOP_TILES["city"].size()]+".tscn")
			if ti==null || !ti.can_instance():
				continue
			ti = ti.instance()
			ti.set_z_index(1)
			ti.position = Vector2(rand_range(0.0,4.0),0.0).rotated(2.0*PI*randf())
			ti.position = Vector2(round(ti.get_position().x),round(ti.get_position().y))
			Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)).add_child(ti)

