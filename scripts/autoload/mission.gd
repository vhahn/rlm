extends Node

const MISSIONS = {
	"terran":["recycling","ratter","rat_boss","explore"],
	"warm":["contaminated_plants","ratter","rat_boss","explore"],
	"desert":["terraforming","pest_control","collect_probes","search_water"],
	"ice_ball":["terraforming","ratter","pest_control","collect_probes","explore"],
	"swamp":["contaminated_plants","terraforming","bounty","search_and_rescue"],
	"lava":["dragon_hunter","collect_probes","search_and_rescue"],
	"toxic":["recycling","bounty","search_and_rescue","explore"],
	"city":[]
}


# tutorial

class Tutorial:
	var stage := 0
	var enemy
	var timer : Timer
	var indicator
	var move_to : Vector2
	
	func _init(dict=null):
		timer = Timer.new()
		timer.one_shot = true
		Main.add_child(timer)
		timer.wait_time = 2.0
		HUD.get_node("TouchMenus/Launch").set_disabled(false)
		Main.cleared = true
		update_gui()
		if dict!=null:
			var array = dict.move_to.split(",")
			stage = dict.stage
			move_to = Vector2(int(array[0]),int(array[1]))
			if stage==1:
				Generate.control_points = [move_to]
				Generate.add_control_points()
				indicator = preload("res://scenes/gui/indicator.tscn").instance()
				indicator.tile = Main.tiles.get_node("Tile_"+str(move_to.x)+"_"+str(move_to.y))
				Main.add_child(indicator)
			update_gui()
			return
		
		move_to = Mission.get_pos_close_to(Main.player.position,2)
		Main.text.push_color(Main.COLOR_DIALOG)
		Main.text.add_text("\n"+tr("TUTORIAL_LOG_01")+"\n\n")
		timer.start()
		yield(timer,"timeout")
		for i in range(2,4):
			Main.text.push_color(Main.COLOR_DEFAULT)
			Main.text.add_text(tr("TUTORIAL_LOG_0"+str(i))+"\n")
			timer.start()
			yield(timer,"timeout")
		Main.text.push_color(Main.COLOR_DIALOG)
		Main.text.add_text("\n"+tr("TUTORIAL_LOG_04")+"\n")
		timer.start()
		yield(timer,"timeout")
		Main.text.push_color(Main.COLOR_DEFAULT)
		Main.text.add_text("\n"+tr("TUTORIAL_LOG_05")+"\n")
		stage += 1
		Generate.control_points = [move_to]
		Generate.add_control_points()
		update_gui()
		indicator = preload("res://scenes/gui/indicator.tscn").instance()
		indicator.tile = Main.tiles.get_node("Tile_"+str(move_to.x)+"_"+str(move_to.y))
		Main.add_child(indicator)
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("TUTORIAL_OBJECTIVE_0"+str(stage+1)))
		HUD.get_node("Mission").show()
	
	func on_control_point_entered(pos):
		if stage!=1:
			return false
		var position : Vector2
		stage += 1
		if Main.tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint"):
			Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint").queue_free()
		Generate.control_points.erase(pos)
		indicator.queue_free()
		indicator = null
		update_gui()
		Main.text.add_text("\n")
		for i in range(6,10):
			Main.text.push_color(Main.COLOR_DIALOG)
			Main.text.add_text(tr("TUTORIAL_LOG_0"+str(i))+"\n")
			timer.start()
			yield(timer,"timeout")
		for i in range(11,13):
			Main.text.push_color(Main.COLOR_DIALOG)
			Main.text.add_text(tr("TUTORIAL_LOG_"+str(i))+"\n")
			timer.start()
			yield(timer,"timeout")
		position = Mission.get_pos_close_to(Main.player.position,2)
		enemy = Enemy.create_dummy(1,position).name
		indicator = preload("res://scenes/gui/indicator.tscn").instance()
		indicator.tile = Main.tiles.get_node("Tile_"+str(position.x)+"_"+str(position.y))
		Main.add_child(indicator)
		
		stage += 1
		update_gui()
		Main.text.add_text("\n")
		for i in range(14,18):
			Main.text.push_color(Main.COLOR_DEFAULT)
			Main.text.add_text(tr("TUTORIAL_LOG_"+str(i))+"\n")
			timer.start()
			yield(timer,"timeout")
		Main.text.add_text("\n")
		return true
	
	func on_died(_enemy):
		var path : String
		if !Main.characters.has("player") || stage!=3:
			return false
		stage += 1
		update_gui()
		if indicator!=null:
			indicator.queue_free()
		indicator = null
		yield(Main.get_tree(),"idle_frame")
		Main.text.add_text("\n")
		for i in range(18,22):
			Main.text.push_color(Main.COLOR_DIALOG)
			Main.text.add_text(tr("TUTORIAL_LOG_"+str(i))+"\n")
			timer.start()
			yield(timer,"timeout")
		Main.add_item(Main.player,"light_armor",1,1,1)
		path = Main.find_path("/",Main.player.equipment,"external")
		if path=="":
			path = Main.find_used_path("/",Main.player.equipment,"external")
			Main.unequip(Main.player,path)
		stage += 1
		update_gui()
		Main.text.push_color(Main.COLOR_DEFAULT)
		Main.text.add_text("\n"+tr("TUTORIAL_LOG_22").format({"inventory_key":OS.get_scancode_string(Main.get_scancode("inventory")),"equipment_key":OS.get_scancode_string(Main.get_scancode("equipment"))})+"\n")
		timer.start()
		yield(timer,"timeout")
		Main.text.push_color(Main.COLOR_DIALOG)
		Main.text.add_text("\n"+tr("TUTORIAL_LOG_23")+"\n")
		timer.start()
		yield(timer,"timeout")
		Main.text.push_color(Main.COLOR_DEFAULT)
		Main.text.add_text("\n"+tr("TUTORIAL_LOG_24")+"\n")
		return true
	
	func on_equip_item(item):
		if stage!=5 || Main.player.inventory[item].type!="light_armor":
			return false
		stage += 1
		update_gui()
		Main.text.add_text("\n")
		Main.text.push_color(Main.COLOR_DIALOG)
		Main.text.add_text("\n"+tr("TUTORIAL_LOG_25")+"\n")
		timer.start()
		yield(timer,"timeout")
		Main.text.push_color(Main.COLOR_DEFAULT)
		Main.text.add_text("\n"+tr("TUTORIAL_LOG_26")+"\n")
		timer.start()
		yield(timer,"timeout")
		Main.text.add_text("\n")
		for i in range(27,30):
			Main.text.push_color(Main.COLOR_DIALOG)
			Main.text.add_text(tr("TUTORIAL_LOG_"+str(i))+"\n")
			timer.start()
			yield(timer,"timeout")
		Main.add_item(Main.player,"metal_alloys",1,1)
		stage += 1
		update_gui()
		Main.text.push_color(Main.COLOR_DEFAULT)
		Main.text.add_text("\n\n"+tr("TUTORIAL_LOG_30").format({"inventory_key":OS.get_scancode_string(Main.get_scancode("inventory"))})+"\n")
		return true
	
	func on_item_used(item):
		if stage!=7 || Main.player.inventory[item].type!="metal_alloys":
			return false
		stage += 1
		update_gui()
		HUD.get_node("Status/Help").show()
		Main.text.add_text("\n")
		for i in range(31,37):
			Main.text.push_color(Main.COLOR_DIALOG)
			Main.text.add_text(tr("TUTORIAL_LOG_"+str(i))+"\n")
			timer.start()
			yield(timer,"timeout")
		Mission.mission_accomplished()
		return true
	
	func on_launch():
		if indicator!=null:
			indicator.queue_free()
	
	func to_dict():
		return {"type":"tutorial","vars":{"stage":stage,"enemy":enemy,"move_to":move_to}}

func new_tutorial(vars=null):
	return Tutorial.new(vars)


# receycling

class Recycling:
	const ITEMS = ["waste","toxic_waste","radioactive_waste"]
	var waste
	
	func _init(dict=null):
		if dict!=null:
			var has = 0
			for item in Main.characters["player"].inventory:
				if item["type"] in ITEMS:
					has += item["amount"]
			waste = dict["waste"]
			update_gui(has)
			on_pick_up()
			return
		
		var amount = randi()%5+6
		waste = round(rand_range(0.8,0.9)*amount)
		for _i in range(amount):
			var item
			var type = "waste"
			if randf()<0.3:
				type = "toxic_waste"
			elif randf()<0.2:
				type = "radioactive_waste"
			item = {"type":type,"level":0,"amount":randi()%3+1}
			Generate.add_item(item)
		update_gui(0)
	
	func update_gui(amount):
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("RECYCLING")+"\n"+tr("COLLECT_WASTE").format({"waste":amount,"total":waste}))
		HUD.get_node("Mission").show()
	
	func on_pick_up(_pos=null,_it=null):
		if !Main.characters.has("player"):
			return false
		var has = 0
		for item in Main.characters["player"].inventory:
			if item["type"] in ITEMS:
				has += item["amount"]
		update_gui(has)
		if has>=waste:
			Mission.mission_accomplished()
		return true
	
	func on_drop(_pos=null,_it=null):
		if !Main.characters.has("player"):
			return false
		var has = 0
		for item in Main.characters["player"].inventory:
			if item["type"] in ITEMS:
				has += item["amount"]
		update_gui(has)
		if Main.cleared && has<waste:
			Mission.mission_abborded()
		return true
	
	func to_dict():
		return {"type":"recycling","vars":{"waste":waste}}

func new_recycling(vars=null):
	return Recycling.new(vars)


# contaminated plants

class ContaminatedPlants:
	const ITEMS = ["contaminated_tree","contaminated_bush","contaminated_grass"]
	var plants : int
	
	func _init(dict=null):
		if dict!=null:
			var has = 0
			for item in Main.characters["player"].inventory:
				if item["type"] in ITEMS:
					has += item["amount"]
			plants = dict["plants"]
			update_gui(has)
			on_pick_up()
			return
		
		var amount = randi()%4+8
		plants = int(rand_range(0.8,0.9)*amount)
		for _i in range(int(1.5*amount+1)):
			var item
			var type = ITEMS[randi()%3]
			item = {"type":type,"level":0,"amount":1}
			Generate.add_item(item)
		update_gui(0)
	
	func update_gui(amount):
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("CONTAMINATED_PLANTS")+"\n"+tr("COLLECT_PLANTS").format({"plants":amount,"total":plants}))
		HUD.get_node("Mission").show()
	
	func on_pick_up(_pos=null,_it=null):
		if !Main.characters.has("player"):
			return false
		var has = 0
		for item in Main.characters["player"].inventory:
			if item["type"] in ITEMS:
				has += item["amount"]
		update_gui(has)
		if has>=plants:
			Mission.mission_accomplished()
		return true
	
	func on_drop(_pos=null,_it=null):
		if !Main.characters.has("player"):
			return false
		var has = 0
		for item in Main.characters["player"].inventory:
			if item["type"] in ITEMS:
				has += item["amount"]
		update_gui(has)
		if Main.cleared && has<plants:
			Mission.mission_abborded()
		return true
	
	func to_dict():
		return {"type":"contaminated_plants","vars":{"plants":plants}}

func new_contaminated_plants(vars=null):
	return ContaminatedPlants.new(vars)



# ratter

class Ratter:
	var rats : int
	var left : int
	
	func _init(dict=null):
		if dict!=null:
			rats = dict["rats"]
			left = dict["left"]
			update_gui()
			if left<1:
				Mission.mission_accomplished()
			return
		
		var amount = 0
		for c in Main.characters.values():
			amount += int(c.species=="RAT")
		for _i in range(4-amount+randi()%3):
			Generate.spawn_enemy("rat",randi()%int(3+0.1*Generate.level)+Generate.level+1)
			amount += 1
		rats = int(clamp(round(rand_range(0.7,0.9)*amount),1,amount))
		left = rats
		update_gui()
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("RATTER")+"\n"+tr("KILLED_RATS").format({"rats":int(rats),"left":int(left)}))
		HUD.get_node("Mission").show()
	
	func on_died(enemy):
		if !Main.characters.has("player"):
			return false
		left -= int(enemy.species=="RAT")
		update_gui()
		if left<1:
			Mission.mission_accomplished()
		return true
	
	func to_dict():
		return {"type":"ratter","vars":{"rats":rats,"left":left}}

func new_ratter(vars=null):
	return Ratter.new(vars)


# dragon hunter

class DragonHunter:
	var dragons : int
	var left : int
	
	func _init(dict=null):
		if dict!=null:
			dragons = dict["dragons"]
			left = dict["left"]
			update_gui()
			if left<1:
				Mission.mission_accomplished()
			return
		
		var amount = 0
		for c in Main.characters.values():
			amount += int(c.species=="DRAGON")
		for _i in range(3-amount+randi()%2):
			Generate.spawn_enemy("dragon",randi()%int(3+0.1*Generate.level)+Generate.level+1)
			amount += 1
		dragons = int(clamp(round(rand_range(0.7,0.9)*amount),1,amount))
		left = dragons
		update_gui()
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("DRAGON_HUNTER")+"\n"+tr("KILLED_DRAGONS").format({"dragons":int(dragons),"left":int(left)}))
		HUD.get_node("Mission").show()
	
	func on_died(enemy):
		if !Main.characters.has("player"):
			return false
		left -= int(enemy.species=="DRAGON")
		update_gui()
		if left<1:
			Mission.mission_accomplished()
		return true
	
	func to_dict():
		return {"type":"dargon_hunter","vars":{"dragons":dragons,"left":left}}

func new_dragon_hunter(vars=null):
	return DragonHunter.new(vars)


# pest control

class PestControl:
	var amount : int
	var left : int
	
	func _init(dict=null):
		if dict!=null:
			amount = dict["amount"]
			left = dict["left"]
			update_gui()
			if left<1:
				Mission.mission_accomplished()
			return
		
		amount = 0
		for c in Main.characters.values():
			amount += int(c.faction!=Main.characters["player"].faction)
		for _i in range(8-amount+randi()%4):
			Generate.spawn_enemy("rat",randi()%int(3+0.1*Generate.level)+Generate.level+1)
			amount += 1
		amount = int(clamp(round(rand_range(0.6,0.8)*amount),1,amount))
		left = amount
		update_gui()
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("PEST_CONTROL")+"\n"+tr("KILLED_VERMIN").format({"amount":int(amount),"left":int(left)}))
		HUD.get_node("Mission").show()
	
	func on_died(enemy):
		if !Main.characters.has("player") || enemy.faction==Main.characters["player"].faction:
			return false
		left -= 1
		update_gui()
		if left<1:
			Mission.mission_accomplished()
		return true
	
	func to_dict():
		return {"type":"pest_control","vars":{"amount":amount,"left":left}}

func new_pest_control(vars=null):
	return PestControl.new(vars)


# rat boss

class RatBoss:
	var ID
	
	func _init(dict=null):
		if dict!=null:
			ID = dict["ID"]
			update_gui()
			if !Main.characters.has(ID):
				Mission.mission_accomplished()
			return
		
		var enemy = Generate.spawn_enemy("cybernetic_rat",randi()%int(2+0.1*Generate.level)+int(1.2*Generate.level)+2)
		for n in Main.characters.keys():
			if Main.characters[n]==enemy:
				ID = n
				break
		update_gui()
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("RAT_BOSS_DESC"))
		HUD.get_node("Mission").show()
	
	func on_died(enemy):
		if !Main.characters.has("player"):
			return false
		if !Main.characters.has(ID) || Main.characters[ID]==enemy:
			Mission.mission_accomplished()
		return true
	
	func to_dict():
		return {"type":"rat_boss","vars":{"ID":ID}}

func new_rat_boss(vars=null):
	return RatBoss.new(vars)


# bounty

const BOUNTY_MECH = [
	{"type":"heavy_mech","level":1,"amount":1},
	{"type":"light_head","level":1,"amount":1},
	{"type":"light_arm","level":1,"amount":2},
	{"type":"light_leg","level":1,"amount":2},
	{"type":"radar","level":1,"amount":1},
	{"type":"foot","level":1,"amount":2},
	{"type":"heavy_armor","level":1,"amount":3},
	{"type":"metal_alloys","level":1,"amount":2}
]

class Bounty:
	var ID
	
	func _init(dict=null):
		if dict!=null:
			ID = dict["ID"]
			update_gui()
			if !Main.characters.has(ID):
				Mission.mission_accomplished()
			return
		
		var enemy = Generate.spawn_enemy("mech",randi()%int(2+0.1*Generate.level)+int(1*Generate.level)+2,[3000+rand_range(375,425)*(Generate.level+2),"pirates","criminal",BOUNTY_MECH])
		for n in Main.characters.keys():
			if Main.characters[n]==enemy:
				ID = n
				break
		update_gui()
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("BOUNTY_DESC"))
		HUD.get_node("Mission").show()
	
	func on_died(enemy):
		if !Main.characters.has("player"):
			return false
		if !Main.characters.has(ID) || Main.characters[ID]==enemy:
			Mission.mission_accomplished()
		return true
	
	func to_dict():
		return {"type":"bounty","vars":{"ID":ID}}

func new_bounty(vars=null):
	return Bounty.new(vars)



# terraforming

class Terraforming:
	var left : int
	var points : Array
	var last_time
	var timer : float
	var delay = 20.0
	
	func _init(dict=null):
		if dict!=null:
			left = dict["left"]
			points = dict["points"]
			for s in dict["points"]:					# convert strings to Vector2
				var c = s.split(",",true,1)
				var p = Vector2(int(c[0]),int(c[1]))
				Generate.control_points.push_back(p)
			Generate.add_control_points()
			update_gui()
			if left<1:
				Mission.mission_accomplished()
			return
		
		left = 3
		points = []
		points.resize(left+2)
		for i in range(points.size()):
			var pos = Vector2(randi()%Generate.width,randi()%Generate.height)
			while !Generate.is_walkable(pos) || !Main.is_empty(pos):
				pos = Vector2(randi()%Generate.width,randi()%Generate.height)
			points[i] = pos
		Generate.control_points = points
		Generate.add_control_points()
		update_gui()
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("TERRAFORMING")+"\n"+tr("INSTALL_TERRAFORMING_EQUIPMENT").format({"amount":int(left)}))
		if last_time!=null:
			HUD.get_node("Mission/Text").add_text("\n"+tr("HOLD_CONTROL_POINT_FOR").format({"left":ceil(timer)}))
		HUD.get_node("Mission").show()
	
	func on_control_point_entered(_pos):
		timer = delay
		last_time = Main.time
		update_gui()
		Main.text.push_color(Main.COLOR_DEFAULT)
		Main.text.add_text(tr("HOLD_CONTROL_POINT_FOR").format({"left":ceil(delay)})+"\n")
	
	func on_control_point_left(_pos):
		timer = delay
		last_time = null
		update_gui()
	
	func on_control_point_hold(pos):
		if last_time!=null:
			timer -= Main.time-last_time
			update_gui()
		last_time = Main.time
		if timer<=0.0:
			timer = delay
			last_time = null
			left -= 1
			if left<0:
				left = 0
			if Main.tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint"):
				Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint").queue_free()
			Generate.control_points.erase(pos)
			update_gui()
			Main.text.push_color(Main.COLOR_DEFAULT)
			Main.text.add_text(tr("DEVICE_INSTALLED")+"\n")
			if left<1:
				Mission.mission_accomplished()
	
	func to_dict():
		return {"type":"terraforming","vars":{"left":left,"points":Generate.control_points}}

func new_terraforming(vars=null):
	return Terraforming.new(vars)


# collect probes

class CollectProbes:
	var left : int
	var points : Array
	
	func _init(dict=null):
		if dict!=null:
			left = dict["left"]
			points = dict["points"]
			for s in dict["points"]:					# convert strings to Vector2
				var c = s.split(",",true,1)
				var p = Vector2(int(c[0]),int(c[1]))
				Generate.control_points.push_back(p)
			Generate.add_control_points()
			update_gui()
			if left<1:
				Mission.mission_accomplished()
			return
		
		left = 5+randi()%3
		points = []
		points.resize(left+1)
		for i in range(points.size()):
			var pos = Vector2(randi()%Generate.width,randi()%Generate.height)
			while !Generate.is_walkable(pos) || !Main.is_empty(pos):
				pos = Vector2(randi()%Generate.width,randi()%Generate.height)
			points[i] = pos
		Generate.control_points = points
		Generate.add_control_points()
		update_gui()
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("PICK_UP_PROBES").format({"left":int(left)}))
		HUD.get_node("Mission").show()
	
	func on_control_point_entered(pos):
		left -= 1
		if left<0:
			left = 0
		if Main.tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint"):
			Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint").queue_free()
		Generate.control_points.erase(pos)
		update_gui()
		Main.text.push_color(Main.COLOR_DEFAULT)
		Main.text.add_text(tr("PICKED_UP_PROBE")+"\n")
		if left<1:
			Mission.mission_accomplished()
	
	func to_dict():
		return {"type":"collect_probes","vars":{"left":left,"points":Generate.control_points}}

func new_collect_probes(vars=null):
	return CollectProbes.new(vars)


# search for water

class SearchWater:
	var left : int
	var points : Array
	
	func _init(dict=null):
		var has_drill = false
		for item in Main.characters["player"].inventory:
			if item["type"]=="drill":
				has_drill = true
				break
		if !has_drill:
			Main.add_item(Main.characters["player"],"drill",0,1)
		if dict!=null:
			left = dict["left"]
			points = dict["points"]
			for s in dict["points"]:					# convert strings to Vector2
				var c = s.split(",",true,1)
				var p = Vector2(int(c[0]),int(c[1]))
				Generate.control_points.push_back(p)
			Generate.add_control_points()
			for pos in Generate.control_points:
				if Main.tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint"):
					Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint").hide()
			update_gui()
			if left<1:
				Mission.mission_accomplished()
			return
		
		left = 2+randi()%2
		points = []
		points.resize(3*left+10)
		for i in range(points.size()):
			var pos = Vector2(randi()%Generate.width,randi()%Generate.height)
			while !Generate.is_walkable(pos) || !Main.is_empty(pos) || !("desert" in Generate.ground_tiles[pos]):
				pos = Vector2(randi()%Generate.width,randi()%Generate.height)
			points[i] = pos
		Generate.control_points = points
		Generate.add_control_points()
		for pos in Generate.control_points:
			if Main.tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint"):
				Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint").hide()
		update_gui()
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
		HUD.get_node("Mission/Text").add_text(tr("SEARCH_FOR_WATER_LEFT").format({"left":int(left)}))
		HUD.get_node("Mission").show()
	
	func on_dig(pos):
		if pos in Generate.control_points:
			left -= 1
			if left<0:
				left = 0
			if Main.tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint"):
				Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/ControlPoint").queue_free()
			Generate.control_points.erase(pos)
			update_gui()
			Main.text.push_color(Main.COLOR_DEFAULT)
			Main.text.add_text(tr("FOUND_WATER")+"\n")
			if left<1:
				Mission.mission_accomplished()
		else:
			var closest
			var dist = 10
			for p in Generate.control_points:
				var d = pos.distance_to(p)
				if d<dist:
					dist = d
					closest = p
					if dist<3:
						break
			Main.text.push_color(Main.COLOR_MISSED)
			Main.text.add_text(tr("DIGING_FAILED")+"\n")
			Main.text.push_color(Main.COLOR_DEFAULT)
			if dist>=10:
				Main.text.add_text(tr("NO_WATER_HERE")+"\n")
			elif dist>=3:
				var ang = pos.angle_to_point(closest)
				var dir = Mission.get_direction(ang)
				Main.text.add_text(tr("WATER_DIRECTION").format({"direction":tr(dir)})+"\n")
			else:
				Main.text.add_text(tr("WATER_CLOSE")+"\n")
			
	
	func to_dict():
		return {"type":"search_water","vars":{"left":left,"points":Generate.control_points}}

func new_search_water(vars=null):
	return SearchWater.new(vars)


# Search & Rescue

const SURVIVOR_MECH = [
	{"type":"scout_mech","level":1,"amount":1},
	{"type":"light_head","level":1,"amount":1},
	{"type":"light_arm","level":1,"amount":1},
	{"type":"light_leg","level":1,"amount":2},
	{"type":"radar","level":1,"amount":1},
	{"type":"ecm","level":1,"amount":1},
	{"type":"foot","level":1,"amount":2},
	{"type":"light_stealth_armor","level":1,"amount":3},
	{"type":"metal_alloys","level":1,"amount":2}
]

class SearchRescue:
	var timer : float
	var point : Vector2
	var found : bool
	var ID
	var failed : bool
	
	func _init(dict=null):
		if dict!=null:
			var s = dict["point"].split(",",true,1)			# convert strings to Vector2
			timer = dict["timer"]
			found = dict["found"]
			failed = dict["failed"]
			ID = dict["ID"]
			point = Vector2(int(s[0]),int(s[1]))
			Generate.control_points = [point]
			Generate.add_control_points()
			Main.tiles.get_node("Tile_"+str(point.x)+"_"+str(point.y)).add_child(preload("res://scenes/tiles/capsule.tscn").instance())
			update_gui()
			if timer<=0.0:
				Mission.mission_accomplished()
			return
		
		found = false
		timer = 60.0
		point = Vector2(randi()%Generate.width,randi()%Generate.height)
		while !Generate.is_walkable(point) || !Main.is_empty(point):
			point = Vector2(randi()%Generate.width,randi()%Generate.height)
		Generate.control_points = [point]
		Generate.add_control_points()
		Main.tiles.get_node("Tile_"+str(point.x)+"_"+str(point.y)).add_child(preload("res://scenes/tiles/capsule.tscn").instance())
		update_gui()
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
		if found:
			HUD.get_node("Mission/Text").add_text(tr("SEARCH_AND_RESCUE_PROTECT").format({"left":str(timer).pad_decimals(1)}))
		elif failed:
			HUD.get_node("Mission/Text").add_text(tr("MISISON_FAILED"))
		else:
			HUD.get_node("Mission/Text").add_text(tr("SEARCH_AND_RESCUE_SEARCH"))
		HUD.get_node("Mission").show()
	
	func on_control_point_entered(_pos):
		if found:
			return
		found = true
		ID = Enemy.create_mech(Main.player.level,Mission.get_pos_close_to(Main.player.position,2),2000+rand_range(250,350)*(Generate.level+1),"player",tr("SURVIVOR"),Mission.SURVIVOR_MECH).name
		update_gui()
		Main.text.push_color(Main.COLOR_DEFAULT)
		Main.text.add_text(tr("FOUND_SURVIVOR")+"\n"+tr("PROTECT_SURVIVOR_FOR").format({"left":str(timer).pad_decimals(1)})+"\n")
	
	func on_died(character):
		if character.name!=ID:
			return
		Mission.mission_failed()
	
	func on_process(delta):
		if !found || failed || timer<=0.0:
			return
		timer -= delta
		if timer<=0.0:
			Mission.mission_accomplished()
			Main.text.push_color(Main.COLOR_DEFAULT)
			Main.text.add_text(tr("LEAVE_WITH_SURVIVOR")+"\n")
		else:
			update_gui()
	
	func to_dict():
		return {"type":"search_and_rescue","vars":{"point":point,"found":found,"timer":timer,"ID":ID,"failed":failed}}

func new_search_and_rescue(vars=null):
	return SearchRescue.new(vars)


# explore

class Explore:
	var visited : Array
	var amount : int
	
	func _init(dict=null):
		if dict!=null:
			amount = dict["amount"]
			visited = []
			visited.resize(dict["visited"].size())
			for i in range(dict["visited"].size()):				# convert strings to Vector2
				var c = dict["visited"][i].split(",",true,1)
				visited[i] = Vector2(int(c[0]),int(c[1]))
			update_gui()
			return
		
		amount = int(Generate.get_region(Main.player.position).size()*rand_range(0.25,0.33))
		update_gui()
	
	func update_gui():
		HUD.get_node("Mission/Text").clear()
# warning-ignore:integer_division
		HUD.get_node("Mission/Text").add_text(tr("EXPLORE_PROGRESS").format({"percentage":str(int(min(100*visited.size()/amount,100)))}))
		HUD.get_node("Mission").show()
	
	func on_move(pos):
		if pos in visited || Main.cleared:
			return
		visited.push_back(pos)
		if visited.size()>=amount:
			Mission.mission_accomplished()
		else:
			update_gui()
	
	func to_dict():
		return {"type":"explore","vars":{"visited":visited,"amount":amount}}

func new_explore(vars=null):
	return Explore.new(vars)



func mission_accomplished():
	Main.text.push_color(Main.COLOR_DEFAULT)
	Main.text.add_text(tr("MISSION_ACCOMPLISHED")+" "+tr("PRESS_KEY_TO_LEAVE").format({"key":OS.get_scancode_string(Main.get_scancode("launch"))})+"\n")
	HUD.get_node("TouchMenus/Launch").set_disabled(false)
	Main.cleared = true
	HUD.get_node("Mission/Text").set_text(tr("MISSION_ACCOMPLISHED"))
	HUD.get_node("Mission").show()

func mission_abborded():
	Main.text.push_color(Main.COLOR_DEFAULT)
	Main.text.add_text(tr("MISSION_ABBORDED"))
	HUD.get_node("TouchMenus/Launch").set_disabled(true)
	Main.cleared = false

func mission_failed():
	Main.text.push_color(Main.COLOR_PLAYER_KILLED)
	Main.text.add_text(tr("MISSION_FAILED"))
	HUD.get_node("TouchMenus/Launch").set_disabled(true)
	Main.cleared = false

func callback(type,args):
	if Main.mission==null:
		return false
	
	if Main.mission.has_method(type):
		return Main.mission.callv(type,args)
	return false


func on_died(ID):
	callback("on_died",[ID])

func on_process(delta):
	callback("on_process",[delta])

func on_move(pos):
	callback("on_move",[pos])

func on_pick_up(pos,item):
	callback("on_pick_up",[pos,item])

func on_drop(pos,item):
	callback("on_drop",[pos,item])

func on_control_point_entered(pos):
	callback("on_control_point_entered",[pos])

func on_control_point_left(pos):
	callback("on_control_point_left",[pos])

func on_control_point_hold(pos):
	callback("on_control_point_hold",[pos])

func on_dig(pos):
	callback("on_dig",[pos])

func on_equip_item(item):
	callback("on_equip_item",[item])

func on_item_used(item):
	callback("on_item_used",[item])

func on_launch():
	callback("on_launch",[])

# helper functions

func get_direction(ang):
	# Returns the geographic direction given the angle between to points.
	var directions := ["NORTH_WEST","NORTH","NORTH_EAST","SOUTH_EAST","SOUTH","SOUTH_WEST"]
	var dir = directions[directions.size()-1]
	if ang<0:
		ang += 2*PI
	for i in range(6):
		if ang<PI/3.0*(i+0.5):
			dir = directions[i]
			break
	return dir

func get_pos_close_to(pos,dist):
	var position = pos+dist*Generate.DIRECTIONS[randi()%Generate.DIRECTIONS.size()]
	if position.x<0:
		position.x += Generate.width
	elif position.x>=Generate.width:
		position.x -= Generate.width
	if position.y<0:
		position.y += Generate.height
	elif position.y>=Generate.height:
		position.y -= Generate.height
	return position
