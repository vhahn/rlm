extends Node

# Define effects used by items.
const STATUS_CORROSION = {
	"name":"corrosion","type":"acid","malus":true,
	"chance":0.25,"time":8.0,
	"stats":{"armor":-4},
	"msg_applied":"CORROSION_APPLIED","msg_expired":"CORROSION_EXPIRED",
	"icon":"res://images/icons/status/def_down.png"
}
const STATUS_BURN = {
	"name":"burning","type":"fire","malus":true,
	"chance":0.5,"time":5.0,
	"stats":{"hp_reg":-1},
	"msg_applied":"BURN_APPLIED","msg_expired":"BURN_EXPIRED",
	"icon":"res://images/icons/status/burn.png"
}
const STATUS_FROZEN = {
	"name":"frozen","type":"ice","malus":true,
	"chance":0.5,"time":4.0,
	"stats":{"speed":-75,"evasion":-5},
	"msg_applied":"FREEZE_APPLIED","msg_expired":"FREEZE_EXPIRED",
	"icon":"res://images/icons/status/frozen.png"
}
const STATUS_WEB = {
	"name":"entangled","type":"physical","malus":true,
	"chance":0.75,"time":6.0,
	"stats":{"speed":-25,"evasion":-2},
	"msg_applied":"WEBS_APPLIED","msg_expired":"WEBS_EXPIRED",
	"icon":"res://images/icons/status/evasion_down.png"
}
const STATUS_SHOCK = {
	"name":"shock","type":"electricity","malus":true,
	"chance":0.25,"time":0.5,
	"stats":{"speed":-25,"sp_reg":-5},
	"msg_applied":"SHOCK_APPLIED","msg_expired":"SHOCK_EXPIRED",
	"icon":"res://images/icons/status/shock.png"
}
const STATUS_DISCHARGE = {
	"name":"discharge","type":"electricity","malus":true,
	"chance":0.25,"time":3.0,
	"stats":{"sp_reg":-4,"ep_reg":-4},
	"msg_applied":"SHOCK_APPLIED","msg_expired":"SHOCK_EXPIRED",
	"icon":"res://images/icons/status/shock.png"
}
const SUMMON_EQUIPMENT = {
	"portable_turret":[
		{"type":"light_turret","amount":1},
		{"type":"chain_gun","amount":1},
		{"type":"light_armor","amount":1},
		{"type":"radar","amount":1}],
	"large_portable_turret":[
		{"type":"medium_turret","amount":1},
		{"type":"rifle","amount":1},
		{"type":"missile_launcher","amount":1},
		{"type":"light_armor","amount":3},
		{"type":"radar","amount":1}],
	"attack_drone":[
		{"type":"light_drone","amount":1},
		{"type":"pulse_laser","amount":1},
		{"type":"radar","amount":1},
		{"type":"ecm","amount":1}]
}

var items = {}



func get_total_price(item):
	# Increase the item price by the item's level and amount within the stack.
	if !items[item["type"]].has("price"):
		return 0
	var price = int(items[item["type"]]["price"]*(0.75+0.25*item["level"]))
	if item.has("amount"):
		price *= item["amount"]
	return price


func repair(target,amount):
	# Repair the target mech by a certain amount.
	var remaining = target.repair(amount)
	if get_node("/root/Menu/Equipment").is_visible():
		get_node("/root/Menu").update_equipment()
	get_node("/root/Menu").update_gui()
	Main.text.push_color(Main.COLOR_BUFF)
	Main.text.add_text(tr("WAS_REPAIRED").format({"user":Main.player.name,"amount":str(amount-remaining).pad_decimals(1)})+"\n")
	return true

func repair1(target,level,skills):
	# Repair the target mech. Increase the effect by the engineering skill
	var amount := 50.0
	if skills.has("engineering"):
		amount *= 0.9+0.1*level+0.1*skills["engineering"]
	else:
		amount *= 0.9+0.1*level
	return repair(target,amount)

func smoke_bomb(part,caster,_target=null):
	# Reduce the caster's noise.
	caster.noise -= part.stats_l["cloak_strength"]
	return true

func shield(part,caster,_target=null):
	# Increases caster's shield points.
	caster.sp = min(caster.sp+part.stats_l["sp"],caster.stats["sp"])
	return true

func deploy_turret(part,caster,_target=null):
	# Spawn a turret next to the caster.
	var pos
	# Find empty position.
	var offset = randi()%Generate.DIRECTIONS.size()
	for i in range(Generate.DIRECTIONS.size()):
		pos = caster.position+Generate.DIRECTIONS[(i+offset)%Generate.DIRECTIONS.size()]
		pos = Vector2(fposmod(pos.x,Generate.width),fposmod(pos.y,Generate.height))
		if Generate.is_walkable(pos) && Main.is_empty(pos):
			break
	if !Generate.is_walkable(pos) || !Main.is_empty(pos):
		return false
	# Create the turret.
	var eq = SUMMON_EQUIPMENT[part.type].duplicate(true)
	for i in range(eq.size()):
		if caster.skills.has("construction"):
			eq[i]["level"] = max(floor(0.5*part.level+0.5*caster.skills["construction"]+rand_range(-2.0,0.0)),1)
		else:
			eq[i]["level"] = max(floor(0.5*part.level+rand_range(-2.0,0.0)),1)
	var stats := 4.0+float(part.level)/4.0
	var add_level := 0
	if caster.skills.has("construction"):
		stats += caster.skills["construction"]/4.0
		add_level = caster.skills["construction"]/2
	Enemy.create_summon(floor(part.level/2+add_level),0,pos,tr(part.type.to_upper()),"player","robot",eq,max(floor(stats+rand_range(-1.0,1.0)),1),max(floor(stats/2.0+rand_range(-2.0,0.0)),1),max(floor(stats/2.0+rand_range(-1.0,1.0)),1),10)
	caster.noise += part.stats_l["noise"]
	return true

func quest_dig(user,_level,_skills):
	# Used for certain missions.
	Mission.on_dig(user.position)
	return true

# global_: applied to all parts
# parent_: applied only to parent part
# local_: applied only to this part and all sub-parts
# by default applied only to this part
func _ready():
	items["gold"] = {
		"type":"misc",
		"mass":0.0,"price":1,
		"icon":"oceansdream_money"
	}
	
	# repair
	items["metal_alloys"] = {
		"type":"useable","skill":"engineering",
		"mass":0.1,"price":40,
		"use":funcref(self,"repair1"),"target":"user_equipment_all",
		"repair":50,
		"icon":"metal_alloys"
	}
	
	# melee weapons
	items["dagger"] = {
		"type":"weapon","slot":"hand","skill":"backstab",
		"hp":15,"mass":0.4,"price":50,
		"accuracy":7,"damage":10,"attack_delay":0.75,"rays":1,"range":1,"noise":0.0,
		"shield_penetrate":1.0,"armor_penetrate":0.5,
		"slots":["handle"],
		"icon":"dagger","sprite":"dagger",
		"animation":"dagger","msg_dmg":"STABED"
	}
	items["short_sword"] = {
		"type":"weapon","slot":"hand","skill":"backstab",
		"hp":20,"mass":0.6,"price":60,
		"accuracy":5,"damage":12,"attack_delay":0.9,"rays":1,"range":1,"noise":0.2,
		"shield_penetrate":1.0,"armor_penetrate":0.25,
		"slots":["handle"],
		"icon":"short_sword","sprite":"short_sword",
		"animation":"dagger","msg_dmg":"STABED"
	}
	items["rapier"] = {
		"type":"weapon","slot":"hand","skill":"backstab",
		"hp":30,"mass":0.8,"price":175,
		"accuracy":5,"damage":15,"attack_delay":0.75,"rays":1,"range":1,"noise":0.4,
		"shield_penetrate":1.0,"armor_penetrate":0.25,
		"slots":["handle","internal"],
		"icon":"rapier","sprite":"short_sword",
		"animation":"dagger","msg_dmg":"STABED"
	}
	items["long_sword"] = {
		"type":"weapon","slot":"hand","skill":"melee",
		"hp":20,"mass":1.0,"price":70,
		"accuracy":3,"damage":15,"attack_delay":1.0,"rays":1,"range":1,"noise":0.5,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"slots":["handle"],
		"icon":"blade","sprite":"long_sword",
		"animation":"sword","msg_dmg":"SLICED"
	}
	items["axe"] = {
		"type":"weapon","slot":"hand","skill":"melee",
		"hp":35,"mass":1.25,"price":180,
		"accuracy":3,"damage":20,"attack_delay":1.0,"rays":1,"range":1,"noise":0.7,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"slots":["handle","external"],
		"icon":"axe","sprite":"axe",
		"animation":"sword","msg_dmg":"SLICED"
	}
	items["great_sword"] = {
		"type":"weapon","slot":"hand_2","skill":"melee",
		"hp":35,"mass":4.0,"price":120,
		"accuracy":-1,"damage":40,"attack_delay":2.0,"rays":1,"range":1,"noise":1.0,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"slots":["handle","handle"],
		"icon":"great_sword","sprite":"great_sword",
		"animation":"sword","msg_dmg":"SLICED"
	}
	items["spear"] = {
		"type":"weapon","slot":"hand_2","skill":"melee",
		"hp":40,"mass":3.0,"armor":1,"price":125,
		"accuracy":1,"damage":30,"attack_delay":1.8,"rays":1,"range":1,"noise":0.9,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"slots":["handle","internal"],
		"icon":"spear","sprite":"spear",
		"animation":"spear","msg_dmg":"IMPALED"
	}
	items["scythe"] = {
		"type":"weapon","slot":"hand_2","skill":"melee",
		"hp":50,"mass":4.5,"armor":2,"price":250,
		"accuracy":1,"damage":35,"attack_delay":1.8,"rays":1,"range":1,"noise":1.2,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"slots":["handle","weapon"],
		"icon":"scythe","sprite":"scythe",
		"animation":"sword","msg_dmg":"IMPALED"
	}
	
	# melee mod
	items["rocket_handle"] = {
		"type":"mod","slot":"handle","skill":"melee",
		"hp":5,"mass":0.5,"price":50,
		"parent_range":1,
		"icon":"rocket_handle"
	}
	items["tungsten_spikes"] = {
		"type":"mod","slot":"handle","skill":"melee",
		"hp":5,"mass":0.25,"price":30,
		"parent_damage":1,
		"icon":"spikes"
	}
	items["blade_armor"] = {
		"type":"mod","slot":"handle","skill":"melee",
		"hp":10,"armor":1,"mass":0.6,"price":80,
		"parent_armor":2,"parent_armor_penetrate":0.05,
		"icon":"blade_mod"
	}
	items["shock_discharger"] = {
		"type":"mod","slot":"handle","skill":"melee",
		"hp":7,"mass":0.5,"price":100,
		"parent_status":[STATUS_DISCHARGE],
		"icon":"shock_discharger"
	}
	
	# ranged weapons
	items["chain_gun"] = {
		"type":"weapon","slot":"hand","skill":"ranged",
		"hp":15,"mass":1.0,"price":75,
		"accuracy":5,"damage":10,"attack_delay":0.75,"rays":1,"range":4,
		"ep_cost":2.0,"noise":1.0,
		"shield_penetrate":0.0,"armor_penetrate":0.5,
		"slots":["barrel"],
		"icon":"pistol1","sprite":"pistol01",
		"animation":"pistol","msg_dmg":"SHOT"
	}
	items["rifle"] = {
		"type":"weapon","slot":"hand_2","skill":"ranged",
		"hp":30,"mass":2.0,"price":150,
		"accuracy":3,"damage":20,"attack_delay":1.5,"rays":1,"range":6,
		"ep_cost":4.0,"noise":2.5,
		"shield_penetrate":0.0,"armor_penetrate":0.0,
		"slots":["barrel"],
		"icon":"rifle1","sprite":"rifle01",
		"animation":"rifle","msg_dmg":"SHOT"
	}
	items["sniper_rifle"] = {
		"type":"weapon","slot":"hand_2","skill":"sniper",
		"hp":35,"mass":4.0,"price":225,
		"accuracy":6,"damage":30,"attack_delay":2.5,"rays":1,"range":8,
		"ep_cost":8.0,"noise":3.5,
		"shield_penetrate":0.0,"armor_penetrate":0.0,
		"slots":["barrel","barrel"],
		"icon":"rifle3","sprite":"rifle01",
		"animation":"rifle","msg_dmg":"SHOT"
	}
	items["laser_rifle"] = {
		"type":"weapon","slot":"hand_2","skill":"sniper",
		"hp":35,"mass":3.0,"price":150,
		"accuracy":7,"damage":15,"attack_delay":2.0,"rays":1,"range":9,
		"ep_cost":6.0,"noise":1.0,
		"shield_penetrate":0.0,
		"armor_penetrate":0.0,
		"slots":["barrel","internal"],
		"icon":"rifle6","sprite":"rifle01",
		"animation":"laser","msg_dmg":"SHOT"
	}
	items["flamer"] = {
		"type":"weapon","slot":"hand_2","skill":"ranged",
		"hp":30,"mass":2.5,"price":180,
		"accuracy":0,"damage":20,"attack_delay":1.2,"rays":1,"range":2,
		"ep_cost":6.0,"noise":3.5,
		"shield_penetrate":0.0,"armor_penetrate":0.0,
		"status":[STATUS_BURN],
		"slots":["barrel"],
		"icon":"flamer","sprite":"flamer",
		"animation":"flamer","msg_dmg":"BURNED"
	}
	items["freezer"] = {
		"type":"weapon","slot":"hand_2","skill":"ranged",
		"hp":35,"mass":2.5,"price":180,
		"accuracy":0,"damage":16,"attack_delay":1.0,"rays":1,"range":2,
		"ep_cost":5.0,"noise":3.0,
		"shield_penetrate":0.0,"armor_penetrate":0.0,
		"status":[STATUS_FROZEN],
		"slots":["barrel"],
		"icon":"freezer","sprite":"freezer",
		"animation":"freeze","msg_dmg":"FROZEN"
	}
	items["acid_blaster"] = {
		"type":"weapon","slot":"hand","skill":"ranged",
		"hp":20,"mass":1.0,"price":80,
		"accuracy":3,"damage":10,"attack_delay":0.8,"rays":1,"range":4,
		"ep_cost":4.0,"noise":1.0,
		"shield_penetrate":0.5,"armor_penetrate":0.25,
		"status":[STATUS_CORROSION],
		"slots":["barrel"],
		"icon":"pistol_acid","sprite":"pistol01",
		"animation":"acid_blast","msg_dmg":"SHOT"
	}
	items["shotgun"] = {
		"type":"weapon","slot":"hand_2","skill":"ranged",
		"hp":40,"mass":2.5,"price":160,
		"accuracy":1,"damage":8,"attack_delay":2.5,"rays":5,"range":3,
		"ep_cost":6.0,"noise":4.0,"shield_penetrate":0.0,"armor_penetrate":0.0,
		"slots":["barrel","external"],
		"icon":"oceansdream_shotgun","sprite":"rifle01",
		"animation":"shotgun","msg_dmg":"SHOT"
	}
	items["pulse_laser"] = {
		"type":"weapon","slot":"hand","skill":"ranged",
		"hp":20,"mass":2.0,"price":140,
		"accuracy":7,"damage":5,"attack_delay":0.75,"rays":3,"range":5,
		"ep_cost":4.0,"noise":1.5,
		"shield_penetrate":0.0,"armor_penetrate":0.5,
		"slots":["barrel","barrel"],
		"icon":"pistol3","sprite":"pistol03",
		"animation":"pulse_laser","msg_dmg":"SHOT"
	}
#	items["zapper"] = {
#		"type":"weapon","slot":"hand","skill":"ranged",
#		"hp":15,"mass":1.0,"price":100,
#		"accuracy":4,"damage":8,"attack_delay":0.7,"rays":1,"range":4,
#		"ep_cost":2.0,"noise":1.5,
#		"shield_penetrate":0.0,"armor_penetrate":0.5,
#		"status":[STATUS_SHOCK],
#		"slots":["barrel"],
#		"icon":"zapper","sprite":"freezer",
#		"animation":"zapper","msg_dmg":"ELECTROCUTED"
#	}
	
	# ranged mod
	items["barrel_extension"] = {
		"type":"mod","slot":"barrel","skill":"sniper",
		"hp":5,"mass":0.25,"price":40,
		"parent_range":1,
		"icon":"barrel_extension"
	}
	items["silencer"] = {
		"type":"mod","slot":"barrel","skill":"stealth",
		"hp":5,"mass":0.3,"price":100,
		"parent_noise":-0.5,
		"icon":"silencer"
	}
	items["laser_sight"] = {
		"type":"mod","slot":"barrel","skill":"sniper",
		"hp":5,"mass":0.2,"price":30,
		"parent_accuracy":1,
		"icon":"laser_sight"
	}
	items["incendiary_ammo"] = {
		"type":"mod","slot":"barrel","skill":"ranged",
		"hp":5,"mass":0.2,"price":100,
		"parent_status":[STATUS_BURN],
		"icon":"ammo_fire"
	}
	items["piercing_ammo"] = {
		"type":"mod","slot":"barrel","skill":"ranged",
		"hp":5,"mass":0.2,"price":120,
		"parent_armor_penetrate":0.1,
		"icon":"ammo_piercing"
	}
	items["bayonet"] = {
		"type":"weapon","slot":"barrel","skill":"melee",
		"hp":10,"mass":0.3,"price":40,
		"accuracy":2,"damage":8,"attack_delay":0.75,"rays":1,"range":1,"noise":0.0,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"icon":"bayonet",
		"animation":"dagger","msg_dmg":"STABED"
	}
	items["heavy_bayonet"] = {
		"type":"weapon","slot":"barrel","skill":"melee",
		"hp":15,"mass":0.8,"armor":2,"price":120,
		"accuracy":3,"damage":9,"attack_delay":0.75,"rays":1,"range":1,"noise":0.0,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"slots":["handle"],
		"icon":"bayonet",
		"animation":"dagger","msg_dmg":"STABED"
	}
	
	# heavy weapons (mostly missiles)
	items["missile_launcher"] = {
		"type":"weapon","slot":"weapon","skill":"ranged",
		"hp":25,"mass":3.5,"price":200,
		"accuracy":5,"damage":30,"attack_delay":1.0,"rays":1,"range":4,
		"cool_down":12.0,"ep_cost":5.0,"noise":5.0,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"slots":["hardpoint"],
		"icon":"missiles1",
		"animation":"missile","msg_dmg":"BLOWN_UP"
	}
	items["swarm_missile_launcher"] = {
		"type":"weapon","slot":"weapon","skill":"ranged",
		"hp":20,"mass":4.0,"price":180,
		"accuracy":3,"damage":6,"attack_delay":1.0,"rays":5,"range":4,
		"cool_down":15.0,"ep_cost":4.0,"noise":6.0,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"icon":"missiles2",
		"animation":"swarm_missiles","msg_dmg":"BLOWN_UP"
	}
	items["emp_missile_launcher"] = {
		"type":"weapon","slot":"weapon","skill":"ranged",
		"hp":20,"mass":4.0,"price":220,
		"accuracy":6,"damage":35,"attack_delay":1.0,"rays":1,"range":4,
		"cool_down":12.0,"ep_cost":6.0,"noise":4.0,
		"shield_penetrate":0.0,"armor_penetrate":0.0,
		"icon":"missiles5",
		"animation":"missile","msg_dmg":"BLOWN_UP"
	}
	items["incendiary_missile_launcher"] = {
		"type":"weapon","slot":"weapon","skill":"ranged",
		"hp":25,"mass":4.5,"price":275,
		"accuracy":4,"damage":30,"attack_delay":1.0,"rays":1,"range":5,
		"cool_down":14.0,"ep_cost":6.0,"noise":5.5,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"status":[STATUS_BURN],
		"icon":"missiles3",
		"animation":"missile","msg_dmg":"BLOWN_UP"
	}
	items["heavy_missile_launcher"] = {
		"type":"weapon","slot":"weapon","skill":"ranged",
		"hp":35,"mass":6.0,"armor":1,"price":400,
		"accuracy":5,"damage":60,"attack_delay":1.5,"rays":1,"range":5,
		"cool_down":20.0,"ep_cost":8.0,"noise":6.0,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"slots":["hardpoint","hardpoint"],
		"icon":"missiles4",
		"animation":"missile","msg_dmg":"BLOWN_UP"
	}
	items["beam_cannon"] = {
		"type":"weapon","slot":"weapon","skill":"ranged",
		"hp":40,"mass":8.0,"armor":2,"price":500,
		"accuracy":-1,"damage":80,"attack_delay":1.25,"rays":1,"range":3,
		"cool_down":27.5,"ep_cost":16.0,"noise":8.0,
		"shield_penetrate":0.0,"armor_penetrate":0.0,
		"slots":["hardpoint","internal"],
		"icon":"beam_cannon",
		"animation":"beam","msg_dmg":"ANNIHILATED"
	}
	
	# heavy weapon mods
	items["reloading_mechanism"] = {
		"type":"mod","slot":"hardpoint","skill":"engineering",
		"hp":10,"armor":1,"mass":1.0,"price":150,
		"parent_cool_down":-2.0,
		"icon":"reloading_mechanism"
	}
	items["shock_absorber"] = {
		"type":"mod","slot":"hardpoint","skill":"ranged",
		"hp":5,"mass":1.0,"price":60,
		"parent_accuracy":1,
		"icon":"shock_absorber"
	}
	items["mass_optimization"] = {
		"type":"mod","slot":"hardpoint","skill":"armor",
		"hp":15,"mass":-2.0,"price":100,
		"icon":"mass_reduction"
	}
	
	# weapon mounts (used to carry missiles with hands)
	items["weapon_mount"] = {
		"type":"weapon_mount","slot":"hand_2","skill":"armor",
		"hp":20,"armor":2,"mass":1.0,"price":40,
		"slots":["weapon"],
		"icon":"heavy_weapon","sprite":"weapon01"
	}
	items["heavy_weapon_mount"] = {
		"type":"weapon_mount","slot":"hand_2","skill":"armor",
		"hp":30,"armor":4,"mass":2.0,"price":250,
		"slots":["external","weapon"],
		"icon":"heavy_weapon","sprite":"weapon02"
	}
	items["large_weapon_mount"] = {
		"type":"weapon_mount","slot":"hand_2","skill":"armor",
		"hp":30,"armor":3,"mass":4.0,"price":300,
		"slots":["weapon","weapon"],
		"icon":"heavy_weapon","sprite":"weapon03"
	}
	
	# shields
	items["small_shield"] = {
		"type":"shield","slot":"hand","skill":"shield",
		"hp":20,"armor":1,"mass":0.5,"price":40,
		"global_accuracy":0,"global_block":4,
		"icon":"shield1","sprite":"shield1"
	}
	items["medium_shield"] = {
		"type":"shield","slot":"hand","skill":"shield",
		"hp":50,"armor":3,"mass":2.0,"price":140,
		"global_accuracy":-2,"global_block":6,"parent_armor":2,
		"slots":["external"],
		"icon":"shield2","sprite":"shield2"
	}
	items["large_shield"] = {
		"type":"shield","slot":"hand","skill":"shield",
		"hp":250,"armor":6,"mass":5.0,"price":250,
		"global_accuracy":-5,"global_block":10,"parent_armor":4,
		"slots":["external"],
		"icon":"shield3","sprite":"shield3"
	}
	
	# base
	items["light_mech"] = {
		"type":"body","slot":"main","skill":"armor",
		"armor":3,"hp":100,"mass":5.0,"price":250,
		"global_ep":100,"global_sp":25,"global_sensor_strength":2,
		"slots":["head","arm","arm","leg","leg","weapon","internal","external","external"],
		"icon":"mech1","sprite":"base02"
	}
	items["heavy_mech"] = {
		"type":"body","slot":"main","skill":"armor",
		"armor":5,"hp":120,"mass":7.0,"price":450,
		"global_ep":100,"global_sp":40,"global_sensor_strength":2,
		"slots":["head","arm","arm","leg","leg","weapon","weapon","internal","internal","external","external"],
		"icon":"mech2","sprite":"base01"
	}
	items["scout_mech"] = {
		"type":"body","slot":"main","skill":"armor",
		"armor":4,"hp":80,"mass":4.0,"price":350,
		"global_ep":120,"global_sp":30,"global_sensor_strength":3,
		"slots":["head","arm","leg","leg","weapon","weapon","internal","internal","external","external"],
		"icon":"mech3","sprite":"base03"
	}
	items["tripple_mech"] = {
		"type":"body","slot":"main","skill":"armor",
		"armor":5,"hp":140,"mass":8.0,"price":600,
		"global_ep":120,"global_sp":40,"global_sensor_strength":2,
		"slots":["head","arm","arm","arm","leg","leg","internal","external","external","external"],
		"icon":"mech4","sprite":"base04"
	}
	items["light_tank"] = {
		"type":"body","slot":"main","skill":"armor",
		"armor":6,"hp":150,"mass":5.0,"price":300,
		"global_ep":80,"global_sp":15,"global_sensor_strength":2,
		"slots":["head","hand","hand","propulsion","propulsion","weapon","weapon","internal","internal","external","external"],
		"icon":"tank1","sprite":"tank01"
	}
	
	# arms
	items["light_arm"] = {
		"type":"arm","slot":"arm","skill":"armor",
		"hp":20,"armor":2,"mass":2.0,"price":100,
		"slots":["external","hand"],
		"icon":"arm01","sprite":"arm02"
	}
	items["medium_arm"] = {
		"type":"arm","slot":"arm","skill":"armor",
		"hp":50,"armor":3,"mass":3.5,"price":250,
		"slots":["internal","external","hand"],
		"icon":"arm01","sprite":"arm01"
	}
	items["heavy_arm"] = {
		"type":"arm","slot":"arm","skill":"armor",
		"hp":50,"armor":4,"mass":4.0,"price":500,
		"slots":["external","weapon","hand"],
		"icon":"arm01","sprite":"arm03"
	}
	items["large_arm"] = {
		"type":"arm","slot":"arm","skill":"armor",
		"hp":125,"armor":6,"mass":8.0,"price":1000,
		"accuracy":-4,
		"slots":["external","hand","hand"],
		"icon":"arm01","sprite":"arm04"
	}
	
	items["shoulder"] = {
		"type":"arm","slot":"arm","skill":"armor",
		"hp":15,"armor":2,"mass":1.0,"price":100,
		"slots":["weapon","arm"],
		"icon":"shoulder1","sprite":"shoulder01"
	}
	
	# legs
	items["light_leg"] = {
		"type":"leg","slot":"leg","skill":"armor",
		"hp":20,"armor":2,"mass":2.0,"price":100,
		"slots":["external","propulsion"],
		"icon":"leg1","sprite":"leg02"
	}
	items["heavy_leg"] = {
		"type":"leg","slot":"leg","skill":"armor",
		"hp":50,"armor":4,"mass":4.0,"price":500,
		"slots":["external","weapon","propulsion"],
		"icon":"leg1","sprite":"leg01"
	}
	
	# heads
	items["light_head"] = {
		"type":"head","slot":"head","skill":"armor",
		"hp":15,"armor":2,"mass":1.5,"price":75,
		"slots":["internal","external"],
		"icon":"oceansdream_helmet","sprite":"head02"
	}
	items["armored_head"] = {
		"type":"head","slot":"head","skill":"armor",
		"hp":25,"armor":4,"mass":2.5,"price":150,
		"global_sp":20,
		"slots":["internal","external"],
		"icon":"oceansdream_helmet","sprite":"head01"
	}
	items["turret_head"] = {
		"type":"head","slot":"head","skill":"armor",
		"hp":15,"armor":3,"mass":2.0,"price":250,
		"slots":["external","weapon"],
		"icon":"oceansdream_helmet","sprite":"head03"
	}
	
	# armor
	items["light_armor"] = {
		"type":"armor","slot":"external","skill":"armor",
		"hp":5,"armor":1,"mass":0.5,"price":20,
		"parent_armor":1,
		"icon":"armor_plating"
	}
	items["heavy_armor"] = {
		"type":"armor","slot":"external","skill":"armor",
		"hp":20,"armor":4,"mass":2.0,"price":150,
		"parent_armor":4,
		"icon":"armor_plating"
	}
	items["light_stealth_armor"] = {
		"type":"armor","slot":"external","skill":"stealth",
		"hp":5,"armor":1,"mass":0.5,"price":40,
		"global_cloak_strength":1,
		"icon":"stealth_armor_plating"
	}
	items["combat_stealth_armor"] = {
		"type":"armor","slot":"external","skill":"stealth",
		"hp":12,"armor":2,"mass":1.0,"price":200,
		"global_cloak_strength":2,"global_evasion":1,
		"icon":"stealth_armor_plating"
	}
	items["reinforcement"] = {
		"type":"armor","slot":"external","skill":"armor",
		"hp":10,"armor":1,"mass":1.0,"price":50,
		"parent_hp":10,
		"icon":"armor_plating"
	}
	
	# propulsion
	items["foot"] = {
		"type":"propulsion","slot":"propulsion","skill":"propulsion",
		"hp":15,"armor":2,"mass":2.0,"price":50,
		"global_speed":50,"global_mass_limit":8,
		"icon":"foot1","sprite":"foot02"
	}
	items["heavy_foot"] = {
		"type":"propulsion","slot":"propulsion","skill":"propulsion",
		"hp":50,"armor":4,"mass":5.0,"price":500,
		"global_speed":50,"global_mass_limit":22,
		"slots":["external"],
		"icon":"foot1","sprite":"foot01"
	}
	items["wheels"] = {
		"type":"propulsion","slot":"propulsion","skill":"propulsion",
		"hp":20,"armor":1,"mass":3.0,"price":50,
		"global_speed":55,"global_mass_limit":6.5,
		"icon":"wheels","sprite":"wheels01"
	}
	items["hover_jets"] = {
		"type":"propulsion","slot":"propulsion","skill":"propulsion",
		"hp":15,"armor":1,"mass":3.0,"price":75,
		"global_speed":70,"global_mass_limit":5.5,
		"icon":"hover","sprite":"jet01"
	}
	
	# sensors
	items["radar"] = {
		"type":"sensors","slot":"internal","skill":"sensors",
		"hp":15,"mass":1.0,"price":50,
		"global_sensor_strength":5,
		"icon":"radar"
	}
	
	# stealth
	items["ecm"] = {
		"type":"stealth","slot":"internal","skill":"stealth",
		"hp":10,"mass":2.0,"price":150,
		"global_cloak_strength":3,
		"icon":"ecm"
	}
	
	# misc
	items["shielding_device"] = {
		"type":"component","slot":"internal","skill":"engineering",
		"hp":20,"mass":2.0,"price":175,
		"global_sp":20,"sp_reg":1.0,
		"icon":"shielding_device"
	}
	items["targeting_computer"] = {
		"type":"component","slot":"internal","skill":"sniper",
		"hp":10,"mass":1.0,"price":200,
		"global_accuracy":2,
		"icon":"computer1"
	}
	items["battery"] = {
		"type":"component","slot":"internal","skill":"engineering",
		"hp":10,"mass":1.5,"price":100,
		"global_ep":20,
		"icon":"battery"
	}
	items["large_battery"] = {
		"type":"component","slot":"internal","skill":"engineering",
		"hp":20,"mass":3.5,"price":175,
		"global_ep":50,
		"icon":"battery_large"
	}
	items["support_thrusters"] = {
		"type":"component","slot":"external","skill":"propulsion",
		"hp":15,"mass":2.0,"armor":1,"price":200,
		"global_speed":10,"global_mass_limit":1,
		"icon":"thruster"
	}
	items["combat_thrusters"] = {
		"type":"component","slot":"external","skill":"propulsion",
		"hp":15,"mass":2.0,"armor":1,"price":250,
		"global_evasion":2,
		"icon":"thruster"
	}
	
	# activatable
	items["smoke_bomb"] = {
		"type":"component","slot":"external","skill":"stealth",
		"attack_delay":0.75,"local_cloak_strength":8.0,
		"hp":10,"mass":1.0,"price":75,
		"cool_down":16.0,
		"actions":[{"name":"smoke_bomb","type":"action","target":"self",
				"use":funcref(self,"smoke_bomb"),
				"msg_use":"SMOKE_BOMB_USE",
				"animation":"smoke_bomb","icon":"smoke_bomb"}],
		"icon":"smoke_bomb"
	}
	items["emergency_shield"] = {
		"type":"component","slot":"internal","skill":"engineering",
		"attack_delay":0.75,"local_sp":7.5,
		"hp":15,"mass":2.0,"price":100,
		"cool_down":16.0,"ep_cost":20.0,
		"actions":[{"name":"emergency_shield","type":"action","target":"self",
				"use":funcref(self,"shield"),
				"msg_use":"EMERGENCY_SHIELD_USE",
				"animation":"emergency_shield","icon":"emergency_shield"}],
		"icon":"emergency_shield"
	}
	items["portable_turret"] = {
		"type":"component","slot":"external","skill":"engineering",
		"attack_delay":1.5,
		"armor":2,"hp":20,"mass":4.0,"price":200,
		"cool_down":16.0,"local_noise":3.0,
		"actions":[{"name":"deploy_turret","type":"action","target":"self",
				"use":funcref(self,"deploy_turret"),
				"msg_use":"DEPLOY_TURRET_USE",
				"animation":"deploy_turret","icon":"portable_turret1"}],
		"icon":"portable_turret1"
	}
	items["large_portable_turret"] = {
		"type":"component","slot":"external","skill":"engineering",
		"attack_delay":2.0,
		"armor":4,"hp":40,"mass":6.0,"price":400,
		"cool_down":20.0,"local_noise":4.0,
		"actions":[{"name":"deploy_turret","type":"action","target":"self",
				"use":funcref(self,"deploy_turret"),
				"msg_use":"DEPLOY_TURRET_USE",
				"animation":"deploy_turret","icon":"portable_turret1"}],
		"icon":"portable_turret1"
	}
	items["attack_drone"] = {
		"type":"component","slot":"external","skill":"engineering",
		"attack_delay":1.5,
		"armor":2,"hp":25,"mass":5.0,"price":300,
		"cool_down":18.0,"local_noise":5.0,
		"actions":[{"name":"deploy_turret","type":"action","target":"self",
				"use":funcref(self,"deploy_turret"),
				"msg_use":"DEPLOY_DRONE_USE",
				"animation":"deploy_drone","icon":"attack_drone"}],
		"icon":"attack_drone"
	}
	
	# special
	items["light_turret"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":30,"armor":3,"mass":4.0,
		"global_ep":10,"global_sensor_strength":2,"global_mass_limit":6,
		"slots":["hand","weapon","internal","external"],
		"icon":"portable_turret1","sprite":"turret01"
	}
	items["medium_turret"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":60,"armor":5,"mass":7.0,
		"global_ep":20,"global_sensor_strength":2,"global_mass_limit":8,
		"slots":["hand","hand","weapon","internal","external","external"],
		"icon":"portable_turret1","sprite":"turret01"
	}
	items["light_drone"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":40,"armor":3,"mass":3.0,
		"global_speed":125,"global_mass_limit":6,
		"global_ep":15,"global_sensor_strength":3,
		"slots":["hand","internal","internal"],
		"icon":"drone1","sprite":"drone01"
	}
	
	# quest items
	items["waste"] = {
		"type":"quest",
		"mass":1.0,"price":-100,
		"icon":"waste"
	}
	items["toxic_waste"] = {
		"type":"quest",
		"mass":1.0,"price":-500,
		"icon":"waste_toxic"
	}
	items["radioactive_waste"] = {
		"type":"quest",
		"mass":1.0,"price":-1000,
		"icon":"stick_green"
	}
	items["contaminated_tree"] = {
		"type":"quest",
		"mass":1.0,"price":0,
		"icon":"contaminated_tree"
	}
	items["contaminated_bush"] = {
		"type":"quest",
		"mass":1.0,"price":0,
		"icon":"contaminated_bush"
	}
	items["contaminated_grass"] = {
		"type":"quest",
		"mass":1.0,"price":0,
		"icon":"contaminated_grass"
	}
	items["drill"] = {
		"type":"useable","skill":"engineering",
		"mass":2.0,"price":250,
		"use":funcref(self,"quest_dig"),"target":"user","consumed":false,
		"icon":"drill"
	}
	
	# monsters
	items["rat_body"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":20,"armor":2,"mass":5.0,
		"global_sp":15,"global_ep":40,"global_sensor_strength":2,"global_mass_limit":6,
		"slots":["internal","external","weapon","head","hand","hand","propulsion"],
		"icon":"rat_body","sprite":"rat"
	}
	items["cybernetic_rat_body"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":40,"armor":4,"mass":6.0,
		"global_sp":20,"global_ep":50,"global_sensor_strength":3,"global_mass_limit":10,
		"slots":["internal","external","weapon","weapon","head","hand","hand","propulsion"],
		"icon":"rat_body","sprite":"cybernetic_rat"
	}
	items["rat_head"] = {
		"type":"head","slot":"head","skill":"armor",
		"hp":10,"armor":1,"mass":1.5,
		"global_sensor_strength":4,
		"slots":["internal","external"],
		"icon":"rat_head"
	}
	items["rat_claws"] = {
		"type":"weapon","slot":"hand","skill":"melee",
		"hp":5,"mass":0.4,
		"accuracy":6,"damage":6,"attack_delay":1.0,"rays":1,"range":1,"noise":0.25,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"icon":"rat_claw",
		"animation":"claws","msg_dmg":"BITES"
	}
	items["rat_feet"] = {
		"type":"propulsion","slot":"propulsion","skill":"propulsion",
		"hp":5,"armor":2,"mass":2.0,
		"global_speed":120,"global_mass_limit":6,
		"icon":"rat_foot"
	}
	
	items["bat_body"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":15,"armor":2,"mass":4.0,
		"global_sp":20,"global_ep":30,"global_sensor_strength":4,"global_mass_limit":4,
		"slots":["internal","external","weapon","head","hand","propulsion","propulsion"],
		"icon":"bat_body","sprite":"bat"
	}
	items["bat_head"] = {
		"type":"head","slot":"head","skill":"armor",
		"hp":10,"armor":1,"mass":1,
		"global_sensor_strength":8,
		"slots":["internal"],
		"icon":"bat_head"
	}
	items["bat_teeth"] = {
		"type":"weapon","slot":"hand","skill":"melee",
		"hp":5,"mass":0.3,
		"accuracy":8,"damage":5,"attack_delay":0.8,"rays":1,"range":1,"noise":0.25,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"icon":"teeth",
		"animation":"bite","msg_dmg":"BITES"
	}
	items["bat_wings"] = {
		"type":"propulsion","slot":"propulsion","skill":"propulsion",
		"hp":4,"armor":1,"mass":1.0,
		"global_speed":85,"global_mass_limit":6,
		"icon":"bat_wing"
	}
	
	items["spider_body"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":15,"armor":1,"mass":4.0,
		"global_sp":25,"global_ep":50,"global_sensor_strength":1,"global_mass_limit":4,
		"slots":["head","hand","hand","propulsion","propulsion","propulsion"],
		"icon":"spider_body","sprite":"spider"
	}
	items["spider_head"] = {
		"type":"head","slot":"head","skill":"sensors",
		"hp":10,"armor":1,"mass":2.0,
		"global_sensor_strength":4,
		"slots":["internal","external"],
		"icon":"spider_head"
	}
	items["spider_teeth"] = {
		"type":"weapon","slot":"hand","skill":"melee",
		"hp":5,"mass":0.5,
		"accuracy":6,"damage":5,"attack_delay":1.0,"rays":1,"range":1,"noise":0.25,
		"shield_penetrate":1.0,"armor_penetrate":0.25,
		"icon":"teeth",
		"animation":"claws","msg_dmg":"BITES"
	}
	items["spider_acid_spray"] = {
		"type":"weapon","slot":"hand","skill":"ranged",
		"hp":5,"mass":0.5,
		"accuracy":4,"damage":4,"attack_delay":1.25,"rays":2,"range":3,
		"ep_cost":10.0,"noise":1.0,
		"shield_penetrate":0.0,"armor_penetrate":0.5,
		"status":[STATUS_CORROSION],
		"icon":"acid",
		"animation":"acid_spray","msg_dmg":"SPITS_ACID_AT"
	}
	items["spider_web"] = {
		"type":"weapon","slot":"hand","skill":"ranged",
		"hp":5,"mass":0.5,
		"accuracy":3,"damage":5,"attack_delay":2.0,"rays":1,"range":3,
		"ep_cost":10.0,"noise":2.0,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"status":[STATUS_WEB],
		"icon":"web",
		"animation":"webs","msg_dmg":"ENTANGLES"
	}
	items["spider_feet"] = {
		"type":"propulsion","slot":"propulsion","skill":"propulsion",
		"hp":5,"mass":1.5,
		"global_speed":60,"global_mass_limit":5,
		"icon":"rat_foot"
	}
	
	items["slime_body"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":25,"mass":5.0,
		"global_sp":30,"global_ep":40,"global_sensor_strength":5,"global_mass_limit":4,"global_cloak_strength":1,
		"slots":["internal","internal","external","weapon","hand","hand","propulsion"],
		"icon":"slime_body","sprite":"slime"
	}
	items["slime_touch"] = {
		"type":"weapon","slot":"hand","skill":"melee",
		"hp":10,"mass":0.5,
		"accuracy":6,"damage":5,"attack_delay":1.0,"rays":1,"range":1,"noise":0.25,
		"shield_penetrate":1.0,"armor_penetrate":0.25,
		"icon":"slime_body",
		"animation":"slime_touch","msg_dmg":"BITES"
	}
	items["slime_acid_spray"] = {
		"type":"weapon","slot":"hand","skill":"ranged",
		"hp":10,"mass":0.5,
		"accuracy":4,"damage":4,"attack_delay":1.25,"rays":2,"range":3,
		"ep_cost":10.0,"noise":1.0,
		"shield_penetrate":0.0,"armor_penetrate":0.5,
		"status":[STATUS_CORROSION],
		"icon":"acid",
		"animation":"slime_blast","msg_dmg":"SPITS_ACID_AT"
	}
	items["slime_propulsion"] = {
		"type":"propulsion","slot":"propulsion","skill":"propulsion",
		"hp":15,"mass":2.0,
		"global_speed":85,"global_mass_limit":6,
		"icon":"slime_body"
	}
	
	items["tentacle_body"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":40,"armor":2,"mass":4.0,
		"global_sp":50,"global_ep":50,"global_sensor_strength":6,
		"global_cloak_strength":3,
		"global_speed":25,"global_mass_limit":28,
		"slots":["internal","internal","internal","external","weapon","weapon","hand","hand","hand"],
		"icon":"tentacle_body","sprite":"tentacle"
	}
	items["tentacle_spores"] = {
		"type":"weapon","slot":"weapon","skill":"ranged",
		"hp":20,"mass":2.0,
		"accuracy":5,"damage":10,"attack_delay":1.5,"rays":1,"range":5,
		"cool_down":16.0,"ep_cost":5.0,"noise":2.0,
		"shield_penetrate":0.0,"armor_penetrate":0.0,
		"status":[STATUS_CORROSION],
		"icon":"spores",
		"animation":"spores","msg_dmg":"POISENED"
	}
	
	items["bug_body"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":40,"armor":3,"mass":5.0,
		"global_sp":20,"global_ep":40,"global_sensor_strength":2,
		"slots":["internal","external","external","external","external","head","propulsion","propulsion","propulsion"],
		"icon":"bug_body","sprite":"bug"
	}
	items["bug_head"] = {
		"type":"head","slot":"head","skill":"armor",
		"hp":20,"armor":2,"mass":1.5,
		"global_sensor_strength":4,
		"slots":["external","hand","hand"],
		"icon":"bug_head"
	}
	items["bug_teeth"] = {
		"type":"weapon","slot":"hand","skill":"melee",
		"hp":15,"armor":1,"mass":0.4,
		"accuracy":6,"damage":10,"attack_delay":1.2,"rays":1,"range":1,"noise":0.25,
		"shield_penetrate":1.0,"armor_penetrate":0.0,
		"icon":"teeth",
		"animation":"claws","msg_dmg":"BITES"
	}
	items["bug_feet"] = {
		"type":"propulsion","slot":"propulsion","skill":"propulsion",
		"hp":10,"armor":1,"mass":2.0,
		"global_speed":40,"global_mass_limit":8,
		"icon":"rat_foot"
	}
	
	items["dragon_body"] = {
		"type":"body","slot":"main","skill":"armor",
		"hp":40,"armor":4,"mass":5.0,
		"global_sp":40,"global_ep":100,"global_sensor_strength":4,"global_mass_limit":8,
		"slots":["internal","external","head","hand","hand","propulsion","propulsion"],
		"icon":"dragon_body","sprite":"dragon"
	}
	items["dragon_head"] = {
		"type":"head","slot":"head","skill":"sensors",
		"hp":25,"armor":3,"mass":2.5,
		"global_sensor_strength":6,
		"slots":["internal","external","weapon","hand"],
		"icon":"dragon_head"
	}
	items["dragon_claws"] = {
		"type":"weapon","slot":"hand","skill":"melee",
		"hp":15,"armor":1,"mass":1.0,
		"accuracy":4,"damage":10,"attack_delay":1.0,"rays":1,"range":1,"noise":0.25,
		"shield_penetrate":1.0,"armor_penetrate":0.25,
		"icon":"rat_claw",
		"animation":"claws","msg_dmg":"BITES"
	}
	items["dargon_fire_breath"] = {
		"type":"weapon","slot":"hand","skill":"ranged",
		"hp":15,"armor":1,"mass":1.0,
		"accuracy":8,"damage":6,"attack_delay":1.25,"rays":4,"range":1,
		"ep_cost":15.0,"noise":2.0,
		"shield_penetrate":0.25,"armor_penetrate":0.25,
		"status":[STATUS_BURN],
		"icon":"fire_blast",
		"animation":"fire_breath","msg_dmg":"BREATHES_FIRE_AT"
	}
	items["dargon_fire_blast"] = {
		"type":"weapon","slot":"hand","skill":"ranged",
		"hp":15,"armor":1,"mass":2.0,
		"accuracy":2,"damage":14,"attack_delay":1.25,"rays":1,"range":4,
		"ep_cost":20.0,"noise":3.0,
		"shield_penetrate":0.0,"armor_penetrate":0.5,
		"status":[STATUS_BURN],
		"icon":"fire_blast",
		"animation":"fire_blast","msg_dmg":"SPITS_FIRE_AT"
	}
	items["dragon_wings"] = {
		"type":"propulsion","slot":"propulsion","skill":"propulsion",
		"hp":20,"mass":2.0,
		"global_speed":75,"global_mass_limit":6,
		"icon":"dragon_wing"
	}
	
