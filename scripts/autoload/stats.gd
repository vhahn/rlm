extends Node

const STATS_BASE = ["str","int","dex"]
const STATS_ALL = ["hp","hp_reg","sp","sp_reg","ep","ep_reg","repair","damage","accuracy","range","rays","attack_delay","cool_down","evasion","armor","block","mass","mass_limit","speed","sensor_strength","cloak_strength","shield_penetrate","armor_penetrate","noise"]
const STATS_INC = ["hp","hp_reg","ep","ep_reg","sp","sp_reg","repair","damage","accuracy","evasion","armor","block","mass_limit","sensor_strength","cloak_strength"]
const SKILLS = ["melee","backstab","ranged","sniper","shield","armor","evasion","propulsion","sensors","stealth","engineering","construction","emergency_shuttle"]
const BASE = {
"hp":0,"hp_reg":0,"ep":0,"ep_reg":1.0,"sp":0,"sp_reg":1.0,
"damage":0,"accuracy":10,"evasion":10,"armor":0,"block":0,
"speed":0,"mass_limit":15,"mass":0,
"sensor_strength":0,"cloak_strength":0,
"range":0,"rays":0,"shield_penetrate":0,"armor_penetrate":0,
"attack_delay":0,"cool_down":0,"noise":0,
"exp_multiplier":1.0,"repair":0}


func get_sub_stats(stats,part,skills):
	if part.type=="empty" || part.copy:
		return
	
	var it = Parts.items[part.type]
	var st = {}
	for s in part.stats_g.keys():
		st[s] = part.stats_g[s]
	for s in part.stats_g.keys():
		if stats.has(s):
			stats[s] += st[s]
		else:
			stats[s] = st[s]
	if part.stats.has("mass"):
		if stats.has("mass"):
			stats["mass"] += part.stats["mass"]
		else:
			stats["mass"] = part.stats["mass"]
	if it["type"]=="weapon":
		stats["weapons"].push_back(it.duplicate())
	for p in part.slots.values():
		get_sub_stats(stats,p,skills)

func get_inventory_mass(inventory):
	var mass = 0.0
	for item in inventory:
		if !item.has("amount"):
			item["amount"] = 1
		mass += Parts.items[item["type"]]["mass"]*item["amount"]
	return mass

func max_exp(lvl,mult):
	return 50*(1+lvl*lvl)*mult

func calc_stats(stats_base,equipment,skills,traits,level,inventory):
	var stats = {}
	stats["weapons"] = []
	for s in BASE.keys():
		stats[s] = BASE[s]
	stats["mass_limit"] *= 0.1*stats_base["str"]
	stats["accuracy"] *= 0.1*stats_base["dex"]
	stats["evasion"] *= 0.1*stats_base["dex"]
	stats["sp_reg"] *= 0.1*stats_base["int"]
	stats["ep_reg"] *= 0.1*stats_base["int"]
	for s in STATS_INC:
		stats[s] *= 0.9+0.1*level
	
	get_sub_stats(stats,equipment,skills)
	stats["mass"] += get_inventory_mass(inventory)
	var ec = 10.0/(10.0+max(stats["mass"]-stats["mass_limit"],0))
	stats["accuracy"] *= ec
	if skills.has("evasion"):
		stats["evasion"] *= ec*(0.9+0.1*skills["evasion"])
	else:
		stats["evasion"] *= ec
	stats["speed"] *= ec
	for s in traits.keys():
		if stats.has(s):
			stats[s] += traits[s]
		else:
			stats[s] = traits[s]
	
	return stats

func apply_melee(stats,level):
	if stats.has("damage"):
		stats["damage"] *= 1.0+0.1*level
	if stats.has("hp"):
		stats["hp"] *= 1.0+0.05*level

func apply_backstab(stats,level):
	if stats.has("damage"):
		stats["damage"] *= 1.0+0.05*level
	if stats.has("accuracy"):
		stats["accuracy"] *= 1.0+0.05*level

func apply_ranged(stats,level):
	if stats.has("damage"):
		stats["damage"] *= 1.0+0.1*level
	if stats.has("attack_delay"):
		stats["attack_delay"] /= 1.0+0.05*level

func apply_sniper(stats,level):
	if stats.has("damage"):
		stats["damage"] *= 1.0+0.05*level
	if stats.has("accuracy"):
		stats["accuracy"] *= 1.0+0.05*level
	if stats.has("range"):
		stats["range"] *= 1.0+0.05*level

func apply_shield(stats,level):
	if stats.has("hp"):
		stats["hp"] *= 1.0+0.05*level
	if stats.has("block"):
		stats["block"] *= 1.0+0.1*level

func apply_armor(stats,level):
	if stats.has("hp"):
		stats["hp"] *= 1.0+0.05*level
	if stats.has("armor"):
		stats["armor"] *= 1.0+0.05*level

func apply_evasion(stats,level):
	if stats.has("evasion"):
		stats["evasion"] *= 1.0+0.05*level

func apply_propulsion(stats,level):
	if stats.has("speed"):
		stats["speed"] *= 1.0+0.05*level

func apply_sensors(stats,level):
	if stats.has("sensor_strength"):
		stats["sensor_strength"] *= 1.0+0.05*level

func apply_stealth(stats,level):
	if stats.has("cloak_strength"):
		stats["cloak_strength"] *= (1.0+0.05*level)
	if stats.has("noise"):
		stats["noise"] = stats["noise"]*max(1.0-0.05*level*sign(stats["noise"]),0.0)

func apply_engineering(stats,level):
	if stats.has("hp_reg"):
		stats["hp_reg"] *= 1.0+0.1*level
	if stats.has("repair"):
		stats["repair"] *= 1.0+0.1*level
	if stats.has("cool_down"):
		if stats["cool_down"]<0.0:
			stats["cool_down"] *= 1.0+0.05*level
		else:
			stats["cool_down"] *= pow(0.99, level)
