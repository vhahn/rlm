extends TextureRect

# warning-ignore:unused_class_variable
onready var center := get_size()/2
# warning-ignore:unused_class_variable
var inner := 44
# warning-ignore:unused_class_variable
var outer := 54
var parts := []
# warning-ignore:unused_class_variable
var special_parts := {"W":[],"E":[],"D":[]}


func _draw():
	var N := parts.size()
	var pi := deg2rad(5)
	var pe := deg2rad(345)
	for i in range(N):
		var colors : Array
		var a1 := pi+(pe-pi)*2*i/N/2
		var a2 := pi+(pe-pi)*(2*i+1.5)/N/2
		if parts[i].hp<=0:
			colors = [Color(0.7,0.0,0.0),Color(0.7,0.0,0.0),Color(0.7,0.0,0.0),Color(0.7,0.0,0.0)]
		elif parts[i].stats.has("hp") && parts[i].hp<parts[i].stats["hp"]/2:
			colors = [Color(0.9,0.4,0.0),Color(0.9,0.4,0.0),Color(0.9,0.4,0.0),Color(0.9,0.4,0.0)]
		else:
			colors = [Color(0.0,1.0,0.0),Color(0.0,1.0,0.0),Color(0.0,1.0,0.0),Color(0.0,1.0,0.0)]
		draw_polygon([center+inner*Vector2(sin(a1),-cos(a1)),center+inner*Vector2(sin(a2),-cos(a2)),center+outer*Vector2(sin(a2),-cos(a2)),center+outer*Vector2(sin(a1),-cos(a1))],colors)
		

func _ready():
	get_node("Help/ButtonClose").connect("pressed",get_node("Help"),"hide")
	for c in get_node("Help").get_children():
		c.connect("mouse_entered",HUD.get_node("Tooltip"),"hide")
