extends Node2D

var tile : Node2D


func _process(_delta):
	if tile==null:
		queue_free()
		return
	
	# scale to range -1.0..1.0
	var scaled_pos = (tile.position-Main.camera.get_camera_screen_center())/OS.window_size*2.0
	var center := Vector2(0.0,-0.5)
	var radius := 1.2
	var r
	var normal
	# apply transform of the viewport texture
	scaled_pos *= Vector2(1.5,1.2)
	scaled_pos += center
	r = sqrt(scaled_pos.x*scaled_pos.x+scaled_pos.y*scaled_pos.y)/radius
	if r<1.0:
		normal = scaled_pos*cos(PI/2.0*r/2.0)*sqrt(2.0)
		position = (normal-center)*Vector2(1.1,1.5)*OS.window_size/2.0+OS.window_size/2.0
		show()
	else:
		hide()
