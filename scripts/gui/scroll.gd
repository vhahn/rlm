extends RichTextLabel

func _input(event):
	if event is InputEventScreenDrag:
		var position = get_global_position()
		var limit = position+get_size()
		if event.position.x>position.x && event.position.x<limit.x && event.position.y>position.y && event.position.y<limit.y:
			var vs = get_v_scroll()
			vs.set_value(vs.get_value()-0.1*event.speed.y)
