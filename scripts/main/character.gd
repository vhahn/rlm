extends Node2D

func destroy():
	var timer = Timer.new()
	hide()
	timer.set_wait_time(2.0)
	timer.set_one_shot(true)
	timer.connect("timeout",self,"queue_free")
	add_child(timer)
	timer.start()
