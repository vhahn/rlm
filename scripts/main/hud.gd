extends CanvasLayer

const BUTTON_VECTOR = Vector2(58,0)
const BUTTON_OFFSET = Vector2(0,-34)

var icon_status = preload("res://scenes/gui/status.tscn")
var character


func update_skill_bar(chara):
	character = chara
	for c in get_node("Actions").get_children():
		c.hide()
	
	for i in range(character.actions.size()):
		var button
		var act = character.actions[i]
		if !has_node("Actions/Button"+str(i)):
			button = get_node("Actions/Button").duplicate()
			button.set_name("Button"+str(i))
			button.connect("pressed",Main,"_select_action",[i])
			button.connect("mouse_entered",get_node("Tooltip"),"hide")
			if i<=10:
				button.get_node("Label").set_text(str((i+1)%10))
			get_node("Actions").add_child(button)
		else:
			button = get_node("Actions/Button"+str(i))
		button.get_node("Icon").set_texture(load("res://images/icons/items/"+act["icon"]+".png"))
		if act["type"]=="attack":
			button.set_tooltip(tr(act["name"].to_upper())+"\n"+tr("RANGE")+": "+str(act["range"])+"\n"+tr("ATTACK_DELAY")+": "+str(act["attack_delay"])+"\n"+tr("HITS")+": "+str(act["rays"])+"\n"+tr("DAMAGE")+": "+str(act["damage"])+"\n"+tr("ACCURACY")+": "+str(act["accuracy"])+"\n"+tr("SHIELD_PENETRATE")+": "+str(act["shield_penetrate"])+"\n"+tr("ARMOR_PENETRATE")+": "+str(act["armor_penetrate"]))
		elif act["type"]=="outfit":
			button.set_tooltip(tr(act["name"].to_upper())+"\n"+tr("TARGET")+": "+tr(act["target"].to_upper())+"\n"+tr("DELAY")+": "+str(act["attack_delay"])+"\n"+tr(act["name"].to_upper()+"_DESC"))
		button.set_position(i*BUTTON_VECTOR+(i%2)*BUTTON_OFFSET)
		if act.has("cool_down") && act["cool_down"]>0.0:
			button.get_node("Cooldown").set_max(act["cool_down"])
			button.get_node("Cooldown").show()
		else:
			button.get_node("Cooldown").hide()
		button.show()

func update_status(chr):
	var status = Main.characters[chr].status
	for c in get_node("Effects").get_children():
		if int(c.get_name())>=status.size() && c.get_node("Animation").get_current_animation()!="fade_out":
			c.get_node("Animation").queue("fade_out")
	for i in range(status.size()):
		var ii
		var text = tr(status[i]["name"].to_upper())+"\n"
		if !has_node("Effects/Icon"+str(i)):
			ii = icon_status.instance()
			ii.set_name("Icon"+str(i))
			get_node("Effects").add_child(ii)
		else:
			ii = get_node("Effects/Icon"+str(i))
		ii.get_node("Icon").set_texture(load(status[i]["icon"]))
		for s in status[i]["stats"].keys():
			text += tr(s.to_upper())+": "+str(status[i]["stats"][s])+"\n"
		ii.tooltip = text
		ii.get_node("Duration").set_text(str(status[i]["time"]).pad_decimals(1))
		ii.set_position(i*BUTTON_VECTOR-(i%2)*BUTTON_OFFSET)
		ii.show()

func remove_status(chr,status):
	var ID = Main.characters[chr].status.find(status)
	if ID>=0 && has_node("Effects/Icon"+str(ID)):
		get_node("Effects/Icon"+str(ID)+"/Animation").queue("fade_out")
		get_node("Effects/Icon"+str(ID)).set_name("deleted")

func update_status_duration(chr):
	var status = Main.characters[chr].status
	for i in range(status.size()):
		if has_node("Effects/Icon"+str(i)):
			get_node("Effects/Icon"+str(i)).get_node("Duration").set_text(str(status[i]["time"]).pad_decimals(1))

func hide():
	for c in get_children():
		if c.has_method("hide"):
			c.hide()

func show():
	for c in get_children():
		if c.has_method("show"):
			c.show()
	if !OS.has_feature("mobile"):
		get_node("TouchMove").hide()
	get_node("Tooltip").hide()
	if Main.mission==null:
		get_node("Mission").hide()

func _show_skills():
	get_node("/root/Menu").update_stats()
	get_node("/root/Menu").update_skills()


func _process(_delta):
	if character!=null:
		for i in range(character.actions.size()):
			var act = character.actions[i]
			if character.cooldown.has(act["ID"]):
				get_node("Actions/Button"+str(i)+"/Cooldown").set_value(character.cooldown[act["ID"]])

func _resized():
	get_node("TouchMove").set_position(Vector2(128,OS.get_window_size().y/2))
	get_node("TouchMenus").set_position(Vector2(OS.get_window_size().x-4,4))

func _ready():
	get_tree().connect("screen_resized",self,"_resized")
	_resized()
	hide()
	
	# connect buttons
	get_node("TouchMove/Wait").connect("pressed",Main,"wait",["player"])
	get_node("TouchMove/MoveN").connect("pressed",Main,"move_player",["player",{"move_n":true}])
	get_node("TouchMove/MoveNW").connect("pressed",Main,"move_player",["player",{"move_nw":true}])
	get_node("TouchMove/MoveNE").connect("pressed",Main,"move_player",["player",{"move_ne":true}])
	get_node("TouchMove/MoveS").connect("pressed",Main,"move_player",["player",{"move_s":true}])
	get_node("TouchMove/MoveSW").connect("pressed",Main,"move_player",["player",{"move_sw":true}])
	get_node("TouchMove/MoveSE").connect("pressed",Main,"move_player",["player",{"move_se":true}])
	get_node("TouchMenus/Menu").connect("pressed",get_node("/root/Menu"),"toggle_menu")
	get_node("TouchMenus/Inventory").connect("pressed",get_node("/root/Menu"),"update_inventory")
	get_node("TouchMenus/Equipment").connect("pressed",get_node("/root/Menu"),"update_equipment")
	get_node("TouchMenus/Skills").connect("pressed",self,"_show_skills")
	get_node("TouchMenus/Launch").connect("pressed",Main,"launch")
	
	for c in get_node("Actions").get_children()+[get_node("Mission/Text"),get_node("Log/Text")]:
		c.connect("mouse_entered",get_node("Tooltip"),"hide")
	
