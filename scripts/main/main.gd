extends Node

const VERSION = "0.3.1-alpha"
const MAX_VIEW_DIST = 15
const MAX_NOISE = 10
const ANIMATION_CHARGE = ["dagger","sword","spear","claws","slime_touch"]
const DIRECTED_SPRITES = ["arm","leg","propulsion","weapon_mount"]
const COLOR_DEFAULT = Color(1.0,1.0,1.0)
const COLOR_MISSED = Color(0.9,0.9,0.5)
const COLOR_DEBUFF = Color(1.0,0.75,0.5)
const COLOR_HIT = Color(0.6,1.0,0.8)
const COLOR_DAMAGED = Color(0.5,1.0,0.6)
const COLOR_KILLED = Color(0.2,1.0,0.3)
const COLOR_BUFF = Color(0.5,0.75,1.0)
const COLOR_REWARD = Color(0.5,1.0,0.5)
const COLOR_PLAYER_MISSED = Color(0.5,0.9,0.9)
const COLOR_PLAYER_HIT = Color(1.0,0.5,0.5)
const COLOR_PLAYER_DAMAGED = Color(1.0,0.15,0.1)
const COLOR_PLAYER_KILLED = Color(0.5,0.0,0.0)
const COLOR_HP = Color(1.0,0.75,0.25)
const COLOR_SP = Color(0.25,0.75,1.0)
const COLOR_ALLY = [Color(1.0,0.05,0.0),Color(0.0,1.0,0.05)]
const COLOR_OBJECT = Color(0.1,0.2,0.9)
const COLOR_HOVER = Color(0.1,0.4,0.8)
const COLOR_DIALOG = Color(0.3,1.0,0.2)

var characters := {}
var player
var mission
var inventory_size := 10.0
var time := 0.0
var time_scale := 10.0
var position := Vector2(0.0,0.0)
var last_position := Vector2(0.0,0.0)
var last_mouse_pos := Vector2(0,0)
var mouse_tile := Vector2(0,0)
var camera_position := Vector2(0,0)
var camera_last_position := Vector2(0,0)
var camera_speed := 10.0
var pause := false
var action_selected
var safe := false
var cleared := false
var shuttle_timer := 0.0
var mission_penalty := 0.0
var view_character

# warning-ignore:unused_class_variable
var base := preload("res://scenes/main/character.tscn")

onready var tiles := get_node("Viewport/Tiles")
onready var text := HUD.get_node("Log/Text")
onready var camera := get_node("Viewport/Camera")


class Character:
	var name
	var level
	var experience
	var inventory
	var equipment
	var status
	var skills
	var traits
	var base_stats
	var stats
	var faction
	var species
	var stat_points
	var skill_points
	var movement_time
	var actions
	var ai_type
	var noise
	var cooldown
	var drop_rate
	var summoned
	var duration
	
	var hp
	var hp_max
	var sp
	var ep
	var dead
	var action
	var time
	var position
# warning-ignore:unused_class_variable
	var path
	var node
	var tween
# warning-ignore:unused_class_variable
	var astar
# warning-ignore:unused_class_variable
	var astar_offset
	
	func _init(nm,l,ex,b,s,t,i,e,f,spc,pos,ai,stp=0,skp=0,st=[],a=null,ti=0.0,n=0.0,dr=0.0,cd=null,sum=false,dur=0):
		name = nm
		level = l
		experience = ex
		stat_points = stp
		skill_points = skp
		faction = f
		base_stats = b
		for s in Stats.STATS_BASE:
			if !base_stats.has(s):
				base_stats[s] = 0
		skills = s
		traits = t
		inventory = i
		equipment = e
		species = spc
		position = pos
		status = st
		action = a
		time = ti
		noise = n
		ai_type = ai
		stats = {}
		summoned = sum
		duration = dur
		if summoned:
			drop_rate = 0.0
		else:
			drop_rate = dr
		dead = false
		if cd!=null:
			cooldown = cd
		else:
			cooldown = {}
		update()
	
	func get_max_exp():
		var mult = 1.0
		if stats.has("exp_multiplier"):
			mult = stats["exp_multiplier"]
		return Stats.max_exp(level,mult)
	
	func update():
		# (Re)calculate stats and update the sprite displayed on the world map.
		var mult = 1.0
		var act_num = {}
		equipment.parent = self
		if stats.has("exp_multiplier"):
			mult = 1.0+stats["exp_multiplier"]
		while experience>=Stats.max_exp(level,mult):
			experience -= Stats.max_exp(level,mult)
			level += 1
		stats = Stats.calc_stats(base_stats,equipment,skills,traits,level,inventory)
		for s in status:
			for st in s["stats"].keys():
				stats[st] += s["stats"][st]
		movement_time = 100.0/max(stats["speed"],1)
		update_hp()
		if sp==null || sp>stats["sp"]:
			sp = stats["sp"]
		if ep==null || ep>stats["ep"]:
			ep = stats["ep"]
		
		actions = equipment.get_actions()
		for act in actions:
			var act_ID = act["name"]
			if act_num.has(act_ID):
				act_num[act_ID] += 1
				act_ID += str(act_num[act_ID])
			else:
				act_num[act_ID] = 1
				act_ID += "1"
			if !cooldown.has(act_ID):
				cooldown[act_ID] = 0.0
			act["ID"] = act_ID
		if ai_type=="player":
			HUD.update_skill_bar(self)
		update_sprite()
	
	func update_sprite():
		# Update the sprite displayed on the world map.
		var ci = preload("res://scenes/main/character.tscn").instance()
		var si = equipment.get_sprite_instance()
		if node!=null:
			node.destroy()
#			node.queue_free()
		
		ci.get_node("Base").add_child(si)
		Main.tiles.add_child(ci)
		node = ci
		tween = Tween.new()
		tween.set_name("Tween")
		node.add_child(tween)
		if Main.tiles.has_node("Tile_"+str(position.x)+"_"+str(position.y)):
			var ID
			node.set_position(Main.tiles.get_node("Tile_"+str(position.x)+"_"+str(position.y)).get_position())
			for k in Main.characters.keys():
				if Main.characters[k]==self:
					ID = k
					break
			if ID!=null:
				Main.tiles.get_node("Tile_"+str(position.x)+"_"+str(position.y)+"/Outlines").set_modulate(Main.COLOR_ALLY[int(faction==Main.characters["player"].faction)])
				Main.tiles.get_node("Tile_"+str(position.x)+"_"+str(position.y)+"/Outlines").set_visible(Main.is_visible("player",ID))
	
	func update_hp():
		hp = equipment.get_total_hp()
		hp_max = equipment.get_max_hp()
	
	func damaged():
		# The mech was damaged. Update the health statistics die if main part health dropped to 0 or below.
		update_hp()
		if equipment.disabled && equipment.hp<=0:
			for c in node.get_node("Base").get_children():
				if c.has_node("Animation") && c.get_node("Animation").has_animation("die"):
					c.get_node("Animation").play("die")
			if Main.tiles.has_node("Tile_"+str(position.x)+"_"+str(position.y)):
				Main.tiles.get_node("Tile_"+str(position.x)+"_"+str(position.y)+"/Outlines").hide()
			for i in range(Main.characters.values().size()):
				if Main.characters.values()[i]==self:
					action = null
					if !summoned && randf()<drop_rate:
						if randf()<0.5:
							Main.drop_gold(Main.characters.keys()[i],int(Generate.gold*(1.0-Main.mission_penalty)*rand_range(0.8,1.2)))
						else:
							var type = randi()%Generate.drops.size()
							inventory = [{"type":Generate.drops[type],"level":round(Generate.level*rand_range((2.0-Main.mission_penalty)/2.0,1.0)*rand_range(0.8,1.2)),"amount":1}]
							if Parts.items[Generate.drops[type]].has("hp"):
								inventory[0]["hp"] = Parts.items[Generate.drops[type]]["hp"]
							Main.drop_item(Main.characters.keys()[i],0)
					dead = true
					action = "die"
					node.get_node("Animation").play("die")
					Mission.on_died(Main.characters.values()[i])
					Main.characters.erase(Main.characters.keys()[i])
					Main.character_died()
					return
		update()
	
	func auto_repair(scale):
		# Repair all components, scale the hp regeneration by a certain value.
		if stats.has("hp_reg"):
			equipment.repair(scale*stats["hp_reg"])
	
	func restore():
		# Resore all components to full hp.
		equipment.restore()
		update()
	
	func add_exp(_exp):
		# Add expierience and increase level if necessery.
		var lvl_up = false
		var mult = 1.0
		experience += _exp
		if stats.has("exp_multiplier"):
			mult = stats["exp_multiplier"]
		while experience>=Stats.max_exp(level,mult):
			experience -= Stats.max_exp(level,mult)
			level += 1
			stat_points += 1
			skill_points += 2
			lvl_up = true
		update()
		return lvl_up
	
	func face_left():
		if node==null:
			return
		
		node.get_node("Base").set_scale(Vector2(-1,1))
	
	func face_right():
		if node==null:
			return
		
		node.get_node("Base").set_scale(Vector2(1,1))
	
	func to_dict():
		var dict = {}
		dict["name"] = name
		dict["level"] = level
		dict["experience"] = experience
		dict["inventory"] = inventory
		dict["equipment"] = equipment.to_dict()
		dict["status"] = status
		dict["skills"] = skills
		dict["traits"] = traits
		dict["base_stats"] = base_stats
		dict["faction"] = faction
		dict["species"] = species
		dict["stat_points"] = stat_points
		dict["skill_points"] = skill_points
		dict["action"] = action
		dict["hp"] = hp
		dict["sp"] = sp
		dict["ep"] = ep
		dict["time"] = time
		dict["noise"] = noise
		dict["position"] = position
		dict["drop_rate"] = drop_rate
		dict["ai_type"] = ai_type
		dict["cooldown"] = cooldown
		dict["title"] = Main.get_title(self)
		dict["summoned"] = summoned
		dict["duration"] = duration
		return dict

class Part:
	var type
	var level
	var copy
	var parent
	var slots
	var path
	var stats = {}
	var stats_l = {}
	var stats_g = {}
	var stats_p = {}
	var skills = {}
	var eq
	
	var hp
	var hp_max
	var disabled
	var icon
	
	func _init(t,l,pth,eqp,sk={},c=false,p=null,h=null):
		type = t
		level = l
		copy = c
		parent = p
		path = pth
		skills = sk
		
		reset(eqp)
		if h!=null:
			hp = h
	
	func reset(equipment=null,sk=null):
		# Set everything to default values.
		# Reset and recalculate stats.
		slots = {}
		stats.clear()
		stats_l.clear()
		stats_g.clear()
		stats_p.clear()
		slots.clear()
		if sk!=null:
			skills = sk
		if type=="empty" || copy || equipment==null:
			disabled = true
			return
		
		var part = Parts.items[type]
		var sl = []
		eq = equipment
		disabled = false
		for s in Stats.STATS_ALL:
			if part.has(s):
				stats[s] = part[s]
			if part.has("local_"+s):
				stats_l[s] = part["local_"+s]
			if part.has("global_"+s):
				stats_g[s] = part["global_"+s]
			if part.has("parent_"+s):
				stats_p[s] = part["parent_"+s]
		for s in Stats.STATS_INC:
			if stats.has(s):
				stats[s] *= 0.9+0.1*level
			if stats_l.has(s):
				stats_l[s] *= 0.9+0.1*level
			if stats_g.has(s):
				stats_g[s] *= 0.9+0.1*level
			if stats_p.has(s):
				stats_p[s] *= 0.9+0.1*level
		if Stats.has_method("apply_"+part["skill"]) && skills.has(part["skill"]):
			Stats.call("apply_"+part["skill"],stats,skills[part["skill"]])
			Stats.call("apply_"+part["skill"],stats_l,skills[part["skill"]])
			Stats.call("apply_"+part["skill"],stats_g,skills[part["skill"]])
			Stats.call("apply_"+part["skill"],stats_p,skills[part["skill"]])
		if Parts.items[type].has("slots"):
			sl = Parts.items[type]["slots"]
			for i in range(sl.size()):
				slots[sl[i]+str(i+1)] = Main.Part.new("empty",0,path+"/"+sl[i]+str(i+1),equipment,skills)
		
		for pt in slots.values():
			for s in pt.stats_p.keys():
				if stats.has(s):
					stats[s] += pt.stats_p[s]
				else:
					stats[s] = pt.stats_p[s]
		var prt = Main._get_parent(path,equipment)
		var last = self
		while prt!=last && prt!=null:
			if prt.disabled:
				break
			for s in prt.stats_l.keys():
				if stats_l.has(s):
					stats_l[s] += prt.stats_l[s]
				else:
					stats_l[s] = prt.stats_l[s]
			last = prt
			prt = Main._get_parent(prt.path,equipment)
		
		for s in stats_l.keys():
			if stats.has(s):
				stats[s] += stats_l[s]
			else:
				stats[s] = stats_l[s]
		var st = {}
		Main.get_global_stats(st,"/",equipment)
		for s in st.keys():
			if stats.has(s):
				stats[s] += st[s]
			else:
				stats[s] = st[s]
		
		if stats.has("hp"):
			if hp==null:
				hp = stats["hp"]
			else:
				hp = min(stats["hp"],hp)
			if hp==null || hp<=0:
				disabled = true
			hp_max = stats["hp"]
		else:
			hp = 1
			hp_max = 1
	
	func update(equipment):
		# Update stats.
		stats.clear()
		stats_l.clear()
		stats_g.clear()
		stats_p.clear()
		for p in slots.values():
			p.update(equipment)
		if disabled || type=="empty" || copy:
			disabled = true
			damaged()
			return
		
		var part = Parts.items[type]
		eq = equipment
		for s in Stats.STATS_ALL:
			if part.has(s):
				stats[s] = part[s]
			if part.has("local_"+s):
				stats_l[s] = part["local_"+s]
			if part.has("global_"+s):
				stats_g[s] = part["global_"+s]
			if part.has("parent_"+s):
				stats_p[s] = part["parent_"+s]
		for s in Stats.STATS_INC:
			if stats.has(s):
				stats[s] *= 0.9+0.1*level
			if stats_l.has(s):
				stats_l[s] *= 0.9+0.1*level
			if stats_g.has(s):
				stats_g[s] *= 0.9+0.1*level
			if stats_p.has(s):
				stats_p[s] *= 0.9+0.1*level
		if Stats.has_method("apply_"+part["skill"]) && skills.has(part["skill"]):
			Stats.call("apply_"+part["skill"],stats,skills[part["skill"]])
			Stats.call("apply_"+part["skill"],stats_l,skills[part["skill"]])
			Stats.call("apply_"+part["skill"],stats_g,skills[part["skill"]])
			Stats.call("apply_"+part["skill"],stats_p,skills[part["skill"]])
		
		for pt in slots.values():
			for s in pt.stats_p.keys():
				if stats.has(s):
					stats[s] += pt.stats_p[s]
				else:
					stats[s] = pt.stats_p[s]
		var prt = Main._get_parent(path,equipment)
		var last = self
		while prt!=last && prt!=null:
			if prt.disabled:
				disabled = true
				break
			for s in prt.stats_l.keys():
				if stats_l.has(s):
					stats_l[s] += prt.stats_l[s]
				else:
					stats_l[s] = prt.stats_l[s]
			last = prt
			prt = Main._get_parent(prt.path,equipment)
		if disabled:
			stats.clear()
			stats_l.clear()
			stats_g.clear()
			stats_p.clear()
			damaged()
			return
		
		for s in stats_l.keys():
			if stats.has(s):
				stats[s] += stats_l[s]
			else:
				stats[s] = stats_l[s]
		var st = {}
		Main.get_global_stats(st,"/",equipment)
		for s in st.keys():
			if stats.has(s):
				stats[s] += st[s]
			else:
				stats[s] = st[s]
		
		if stats.has("hp"):
			# Reduce hp if the max. hp was lowered.
			hp = min(stats["hp"],hp)
			hp_max = stats["hp"]
	
	func damaged():
		# The component was damaged. Disable it if hp is below 0.
		if type=="empty" || copy:
			return
		if hp<0:
			hp = 0
		if hp==null || hp<=0:
			disabled = true
		if icon!=null:
			var damaged = 0.0
			if stats.has("hp"):
				damaged = clamp(1.0-hp/stats["hp"],0.0,1.0)
			if disabled:
				damaged = 1.0
				icon.get_node("Disabled").show()
			else:
				icon.get_node("Disabled").hide()
			icon.get_node("Icon").set_self_modulate(Color(1.0,1.0-damaged,max(1.0-1.25*damaged,0.0),0.5))
			if stats.has("hp"):
				icon.get_node("Structure").set_max(stats["hp"])
				icon.get_node("Structure").set_value(hp)
			HUD.get_node("Status").update()
	
	func repair(amount,reactivate=false):
		# Increase hp by a certain amount. Restore if it was disabled.
		# The amount is reduced by the actual healing done and sub components are repaired by the reduced amount.
		if amount==0 || copy || type=="empty" || (disabled && !reactivate):
			return amount
		if hp!=null && stats.has("hp"):
			var hp_old = hp
			hp += amount
			amount -= min(amount,stats["hp"]-hp_old)
			if hp>=stats["hp"]:
				hp = stats["hp"]
			elif hp<=0.0:
				damaged()
		if disabled:
			disabled = false
			update(eq)
		for equip in slots.values():
			amount = equip.repair(amount)
		return amount
	
	func restore():
		# Reset a component's hp.
		if copy || type=="empty":
			return
		
		disabled = false
		update(eq)
		if stats.has("hp"):
			hp = stats["hp"]
		else:
			hp = 1
		damaged()
		for equip in slots.values():
			equip.restore()
	
	func get_actions():
		# Return an array with possible actions that this component can do, e.g. attacking.
		if copy || type=="empty" || stats.size()==0:
			return []
		
		var actions = []
		var part = Parts.items[type]
		var st = []
		if part.has("status"):
			st = part["status"]
		for p in slots.values():
			if p.copy || p.type=="empty":
				continue
			var sp = Parts.items[p.type]
			if sp.has("parent_status"):
				st += sp["parent_status"]
		if part["type"]=="weapon":
			var ep_cost := 0.0
			var cooldown := 0.0
			if part.has("ep_cost"):
				ep_cost = part["ep_cost"]
			if stats.has("cool_down"):
				cooldown = stats["cool_down"]
			actions.push_back({"name":type,"type":"attack","range":stats["range"],"rays":stats["rays"],
				"attack_delay":stats["attack_delay"],"status":st,
				"noise":stats["noise"],"ep_cost":ep_cost,"cool_down":cooldown,
				"damage":stats["damage"],"accuracy":stats["accuracy"],
				"shield_penetrate":stats["shield_penetrate"],
				"armor_penetrate":stats["armor_penetrate"],
				"msg_dmg":part["msg_dmg"],"animation":part["animation"],
				"icon":part["icon"]})
		if part.has("actions"):
			var ep_cost := 0.0
			var cooldown := 0.0
			var noise := 0.0
			if part.has("ep_cost"):
				ep_cost = part["ep_cost"]
			if stats.has("cool_down"):
				cooldown = stats["cool_down"]
			if stats.has("noise"):
				noise = stats["noise"]
			for i in range(part["actions"].size()):
				var act = part["actions"][i].duplicate()
				act["type"] = "outfit"
				act["part"] = self
				if !act.has("attack_delay"):
					act["attack_delay"] = stats["attack_delay"]
				if !act.has("noise"):
					act["noise"] = noise
				if !act.has("ep_cost"):
					act["ep_cost"] = ep_cost
				if !act.has("cool_down"):
					act["cool_down"] = cooldown
				actions.push_back(act)
		
		for equip in slots.values():
			actions += equip.get_actions()
		return actions
	
	func get_sprite_instance(left=true):
		# Instance the component's sprite and add sub component's sprites as children.
		if type=="empty" || copy || !Parts.items[type].has("sprite"):
			return
		
		var si
		if Parts.items[type]["type"] in Main.DIRECTED_SPRITES:
			var d
			if left:
				d = "_left"
			else:
				d = "_right"
			si =  load("res://scenes/sprites/"+Parts.items[type]["sprite"]+d+".tscn").instance()
		else:
			si =  load("res://scenes/sprites/"+Parts.items[type]["sprite"]+".tscn").instance()
		for slot in slots.keys():
			var pt = slot[0].to_upper().split("_")[0]+slot.substr(1,slot.length()-1)
			if si.has_node(pt):
				var n = si.get_node(pt)
				var d = n.get_position().x<=0
				var si2 = slots[slot].get_sprite_instance(d)
				if si2!=null:
					n.add_child(si2)
		return si
	
	func get_total_hp():
		if type=="empty" || copy:
			return 0
		
		var _hp = max(hp,0)
		for equip in slots.values():
			_hp += equip.get_total_hp()
		return _hp
	
	func get_max_hp():
		if type=="empty" || copy:
			return 0
		
		var _hp = hp_max
		for equip in slots.values():
			_hp += equip.get_max_hp()
		return _hp
	
	func clear():
		# Set to empty slot.
		type = "empty"
		level = 0
		copy = false
		parent = null
		stats.clear()
		reset()
	
	func to_dict():
		var dict = {}
		var sl = {}
		if copy:
			dict["type"] = "empty"
			dict["copy"] = true
			dict["parent"] = parent
			return dict
		dict["type"] = type
		dict["level"] = level
		dict["hp"] = hp
		for s in slots.keys():
			if slots[s]!=null:
				sl[s] = slots[s].to_dict()
			else:
				sl[s] = null
		dict["slots"] = sl
		return dict


###

func add_item(target,type,level,amount,hp=null):
	# Add an item to the inventory.
	if hp==null && Parts.items[type].has("hp"):
		hp = Parts.items[type]["hp"]
	for i in range(target.inventory.size()):
		if target.inventory[i]["type"]==type && target.inventory[i]["level"]==level && (hp==null || (target.inventory[i].has("hp") && target.inventory[i]["hp"]==hp)):
			target.inventory[i]["amount"] += amount
			return false
	target.inventory.push_back({"type":type,"level":level,"amount":amount,"hp":hp})
	return true

func remove_item(target,ID,amount):
	# Remove the item with index ID from the inventory.
	if target.inventory[ID]["amount"]<amount:
		return false
	elif target.inventory[ID]["amount"]==amount:
		target.inventory.remove(ID)
		return true
	target.inventory[ID]["amount"] -= amount
	return true

func rm_item(target,type,level,amount):
	# Remove an item from the inventory.
	var ID = find_item(target,type,level)
	if ID==null:
		return false
	
	return remove_item(target,ID,amount)

func find_item(target,type,level,hp=null):
	# Search for a certain item in the inventory. Return the index.
	for i in range(target.inventory.size()):
		if target.inventory[i]["type"]==type && target.inventory[i]["level"]==level && (hp==null || (target.inventory[i].has("hp") && target.inventory[i]["hp"]==hp)):
			return i

func get_global_stats(stats,path,equipment):
	# Writes the global stats intp the stats dictionary.
	var part = get_part(path,equipment)
	if part.disabled:
		return
	
	for s in part.stats_g.keys():
		if stats.has(s):
			stats[s] += part.stats_g[s]
		else:
			stats[s] = part.stats_g[s]
	for p in part.slots.keys():
		# Add recursively global stat contributions from sub components to the stats dictionary.
		get_global_stats(stats,path+"/"+p,equipment)

func find_path(path,part,slot,exceptions=[]):
	# Search an empty slot of type slot and return the path to that slot.
	if slot=="main":
		return "/"
	for sl in part.slots.keys():
		var s = sl
		for i in range(10):
			s = s.replace(str(i),"")
		if s==slot && part.slots[sl].type=="empty" && !(path+"/"+sl in exceptions):
			return path+"/"+sl
		var pth = ""+find_path(path+"/"+sl,part.slots[sl],slot,exceptions)
		if pth!="" && !(pth in exceptions):
			return pth
	return ""

func find_used_path(path,part,slot,exceptions=[]):
	# Search an empty slot of type slot and return the path to that slot.
	if slot=="main":
		return "/"
	for sl in part.slots.keys():
		var s = sl
		for i in range(10):
			s = s.replace(str(i),"")
		if s==slot && part.slots[sl].type!="empty" && part.slots[sl].type!="copy":
			return path+"/"+sl
		var pth = ""+find_path(path+"/"+sl,part.slots[sl],slot,exceptions)
		if pth!="" && !(pth in exceptions):
			return pth
	return ""

func find_paths(path,part,slot,paths,exceptions=[]):
	# Search all empty slots of type slot and return an array of the paths to these slots.
	if slot=="main":
		return ["/"]
	for sl in part.slots.keys():
		var s = sl
		for i in range(10):
			s = s.replace(str(i),"")
		if s==slot && part.slots[sl].type=="empty":
			paths.push_back(path+"/"+sl)
		var pths = find_paths(path+"/"+sl,part.slots[sl],slot,paths,exceptions)
		for pth in pths:
			if pth!="" && !(pth in exceptions) && !(pth in paths):
				paths.push_back(pth)
	return paths

func find_used_paths(path,part,slot,paths,exceptions=[]):
	# Search all non-empty slots of type slot and return an array of the paths to these slots.
	if slot=="main":
		return ["/"]
	for sl in part.slots.keys():
		var s = sl
		for i in range(10):
			s = s.replace(str(i),"")
		if s==slot && part.slots[sl].type!="empty":
			paths.push_back(path+"/"+sl)
		var pths = find_used_paths(path+"/"+sl,part.slots[sl],slot,paths,exceptions)
		for pth in pths:
			if pth!="" && !(pth in exceptions) && !(pth in paths):
				paths.push_back(pth)
	return paths

func get_part(path,part):
	# Returns the component at a certain path.
	var pt = part
	for p in path.split("/",false):
		if pt.slots.has(p):
			pt = pt.slots[p]
		else:
			return null
	return pt

func _get_parent(path,equipment):
	# Returns the parent component of the component at a certain path.
	return get_part(path.substr(0,path.rfind(path,"/")),equipment)

func equip(target,item,path=null):
	# Equip a certain item either at a given path or take the next empty slot if path is null.
	var type = target.inventory[item]["type"]
	if !Parts.items[type].has("slot"):
		return false
	var item_slot = Parts.items[type]["slot"]
	var blocked = []
	var num_slots = 1
	item_slot = item_slot.split("_")
	if item_slot.size()>1:
		num_slots = int(item_slot[1])
	item_slot = item_slot[0]
	if path==null:
		# Find an empty slot.
		path = ""
		path = find_path(path,target.equipment,item_slot)
		if path=="":
			return false
	else:
		# Check whether the slot is of the right type.
		var slot_type = path.split("/")
		slot_type = slot_type[slot_type.size()-1]
		for i in range(10):
			slot_type = slot_type.replace(str(i),"")
		if slot_type!=item_slot && !(slot_type=="" && item_slot=="main"):
			print("Invalid slot! Is "+slot_type+", required "+item_slot+".")
			return false
	if num_slots>1:
		# The item requires multiple slots. Check whether there are enough empty slots available.
		for _i in range(num_slots-1):
			var path2 = ""
			path2 = find_path(path2,target.equipment,item_slot,[path]+blocked)
			if path=="":
				printt("Not enough free slots to equip",target.inventory[item])
				return false
			else:
				blocked.push_back(path2)
		for p in blocked:
			var ptb = get_part(p,target.equipment)
			if ptb==null || ptb.type!="empty":
				printt("Not enough free slots to equip",target.inventory[item])
				return false
			else:
				ptb.type = type
				ptb.level = target.inventory[item]["level"]
				ptb.copy = true
				ptb.parent = path
				ptb.reset(target.equipment,target.skills)
	
	var pt = get_part(path,target.equipment)
	if pt==null:
		return false
	elif pt.type!="empty":
		# The slot is not empty. Unequip the component first.
		unequip(target,path)
	
	# Change to the component that should be equiped and remove it from inventory.
	pt.type = type
	pt.level = target.inventory[item]["level"]
	if target.inventory[item].has("hp"):
		pt.hp = target.inventory[item]["hp"]
	pt.reset(target.equipment,target.skills)
	remove_item(target,item,1)
	target.restore()
	if target==player:
		Mission.on_equip_item(item)
	return true

func unequip(target,path):
	# Unequip the component at a certain path.
	var pt = get_part(path,target.equipment)
	if pt==null || pt.type=="empty" || pt.copy:
		return false
	
	if !pt.copy:
		add_item(target,pt.type,pt.level,1,pt.hp)
	for slot in pt.slots.keys():
		unequip(target,path+"/"+slot)
	clear_copies(path,target.equipment)
	unequip_parents(target,pt)
	pt.clear()
	target.restore()

func clear_copies(path,part):
	# Clear slots blocked by 2h weapons.
	for p in part.slots.values():
		if p.copy && p.parent==path:
			p.clear()
		if p.type!="empty":
			clear_copies(path,p)

func unequip_parents(target,part):
	# Unequip 2h weapons that requires a sub-slot of the part at path.
	if part.copy:
		unequip(target,part.parent)
	else:
		for p in part.slots.values():
			unequip_parents(target,p)

func add_tag(tag,tags):
	if tags.has(tag):
		tags[tag] += 1
	else:
		tags[tag] = 1

func get_equipment_tags(equipment,tags):
	if !Parts.items.has(equipment.type):
		return tags
	var item = Parts.items[equipment.type]
	var type = item["type"]
	var slot = item["slot"]
	var skill = item["skill"]
	add_tag(skill,tags)
	if skill=="ranged" && type=="weapon":
		tags["ranged"] -= 1
		if tags.has("explosive"):
			tags["explosive"] += 1
		else:
			tags["explosive"] = 1
	if slot=="hand":
		add_tag("1h",tags)
	elif slot.find("hand_")>=0:
		add_tag("2h",tags)
	if skill=="melee":
		add_tag("melee",tags)
	elif skill=="ranged":
		add_tag("ranged",tags)
	
	for sl in equipment.slots.values():
		tags = get_equipment_tags(sl,tags)
	return tags


### Battle

func add_status(target,status):
	characters[target].status.push_back(status.duplicate())
	characters[target].update()
	
	yield(get_tree(),"idle_frame")
	if is_visible("player",target):
		if target=="player" && status["malus"]:
			text.push_color(COLOR_DEBUFF)
		else:
			text.push_color(COLOR_BUFF)
		text.add_text(tr(status["msg_applied"]).format({"target":characters[target].name})+"\n")
		text.push_color(COLOR_DEFAULT)
		HUD.update_status("player")

func remove_status(target,status):
	characters[target].status.erase(status)
	characters[target].update()
	if is_visible("player",target):
		if target=="player" && status["malus"]:
			text.push_color(COLOR_BUFF)
		else:
			text.push_color(COLOR_DEBUFF)
		text.add_text(tr(tr(status["msg_expired"]).format({"target":characters[target].name}))+"\n")
		text.push_color(COLOR_DEFAULT)
		HUD.update_status("player")

func damage(target,part,dmg,accuracy,shield_penetrate,armour_penetrate,status):
	dmg *= rand_range(0.9,1.1)
	var damage = round(dmg*(1-shield_penetrate))
	var dmg_p = dmg*shield_penetrate
	var r = {}
	# Shield damage.
	r["sp"] = min(damage,target.sp)
	target.sp -= damage
	if target.sp<0:
		damage = -target.sp
		target.sp = 0
	else:
		damage = 0
	
	if status.size()>0:
		# Apply status effects.
		var name
		for n in characters.keys():
			if characters[n]==target:
				name = n
				break
		if name!=null:
			for s in status:
				var chance = s["chance"]
				if randf()<chance:
					add_status(name,s)
	
	if randf()<0.667:
		# Target a sub component instead of the current one.
		for _i in range(10):
			if part.slots.size()==0:
				break
			var new = part.slots.values()[randi()%(part.slots.size())]
			if !new.disabled && !new.copy && new.type!="empty":
				part = new
			if randf()<0.333:
				break
	
	var hit_chance = exp(-target.stats["evasion"]/accuracy*log(2))
	var block
	r["part"] = part
	if randf()>hit_chance || part.disabled:
		r["missed"] = true
		return r
	r["missed"] = false
	
	if part.stats.has("block"):
		block = randf()>exp(-part.stats["block"]/accuracy)
	else:
		block = false
	damage += dmg_p
	if part.stats.has("armor"):
		damage = round(max(damage-part.stats["armor"],0)*(1.0-armour_penetrate)*(1-float(block))+armour_penetrate*damage)
	else:
		damage = round(damage*(1.0-armour_penetrate)*(1-float(block))+armour_penetrate*damage)
	r["hp"] = damage
	r["blocked"] = block
	if damage<=0:
		return r
	
	part.hp -= damage
	part.damaged()
	target.equipment.update(target.equipment)
	target.damaged()
	r["destroyed"] = part.disabled
	r["part"] = part
	return r

func attack(actor,target,weapon):
	# Attack target with a certain weapon.
	if actor==target:
		return false
	
	var c = characters[actor]
	var t = characters[target]
	var dist = get_distance(c.position,t.position)
	if c.action!=null || dist>weapon["range"] || c.ep<weapon["ep_cost"] || (c.cooldown.has(weapon["ID"]) && c.cooldown[weapon["ID"]]>0.0):
		return false
	
	var backstab = !is_visible(target,actor) && c.skills.has("backstab")
	var dmg_scale = 1.0
	var rprt = []
	var visible = is_visible("player",actor) && is_visible("player",target)
	c.action = "attack"
	c.time = weapon["attack_delay"]
	if backstab:
		# Damage bonus from the backstab skill.
		dmg_scale += 0.1*c.skills["backstab"]
	c.noise = min(c.noise+max(weapon["noise"],0),MAX_NOISE)
	c.ep -= weapon["ep_cost"]
	if weapon.has("cool_down"):
		c.cooldown[weapon["ID"]] = weapon["cool_down"]
	for _i in range(weapon["rays"]):
		var r = damage(t,get_part("/",t.equipment),weapon["damage"]*dmg_scale,weapon["accuracy"]+c.stats["accuracy"],clamp(weapon["shield_penetrate"],0.0,1.0),clamp(weapon["armor_penetrate"],0.0,1.0),weapon["status"])
		rprt.push_back(r)
	
	reset_time_scale()
	if t.path!=null && t.path.size()>0:
		t.path = [t.path[0]]
	if Enemy.attacked_from.has(target):
		Enemy.attacked_from[target].push_back(c.position)
	else:
		Enemy.attacked_from[target] = [c.position]
	for dmg in rprt:
		# Add log messages.
		if dmg["missed"]:
			if visible:
				if actor=="player":
					text.push_color(COLOR_PLAYER_MISSED)
				else:
					text.push_color(COLOR_MISSED)
				text.add_text(tr("MISSED").format({"attacker":c.name,"target":t.name})+"\n")
		elif dmg["blocked"]:
			if visible:
				if actor=="player":
					text.push_color(COLOR_PLAYER_MISSED)
				else:
					text.push_color(COLOR_MISSED)
				text.add_text(tr("BLOCKED").format({"attacker":c.name,"target":t.name})+"\n")
		elif t.dead:
			if target=="player":
				text.push_color(COLOR_PLAYER_KILLED)
			else:
				text.push_color(COLOR_KILLED)
			if target=="player":
#				get_node("/root/Menu").killed_by = tr("LVL")+" "+str(c.level)+" "+tr(c.name)
				get_node("/root/Menu").killed_by = c
			if visible:
				text.add_text(tr("KILLED").format({"attacker":c.name,"target":t.name})+"\n")
			if actor=="player":
				var lvl_up
				lvl_up = c.add_exp(t.experience)
				text.push_color(COLOR_REWARD)
				if lvl_up:
					text.add_text(tr("LEVEL_UP")+"\n"+tr("REACHED_LEVEL").replace("{lvl}",str(c.level))+"\n")
				else:
					text.add_text(tr("GAINED").replace("{exp}",str(t.experience)+" "+tr("EXP"))+"\n")
		elif visible:
			var color
			if target=="player":
				color = COLOR_PLAYER_HIT
			else:
				color = COLOR_HIT
			text.push_color(color)
			if backstab:
				text.add_text(tr(c.name)+" "+tr("BACKSTABED")+" "+tr(t.name)+" "+tr("FOR")+" ")
			else:
				text.add_text(tr(c.name)+" "+tr(weapon["msg_dmg"])+" "+tr(t.name)+" "+tr("FOR")+" ")
			if dmg["hp"]>0:
				text.push_color(COLOR_HP)
				text.add_text(str(floor(dmg["hp"])))
				text.push_color(color)
				if dmg["sp"]>0:
					text.add_text("+")
			if dmg["sp"]>0:
				text.push_color(COLOR_SP)
				text.add_text(str(floor(dmg["sp"])))
				text.push_color(color)
			if dmg["hp"]<=0 && dmg["sp"]<=0:
				text.add_text("0")
			text.add_text(" "+tr("DAMAGE")+".\n")
			if dmg.has("destroyed") && dmg["destroyed"]:
				if target=="player":
					text.push_color(COLOR_PLAYER_DAMAGED)
				else:
					text.push_color(COLOR_DAMAGED)
				text.add_text(tr("WAS_DESTROYED").format({"target":t.name,"component":tr(dmg["part"].type.to_upper())})+"\n")
		text.push_color(COLOR_DEFAULT)
	
	# Face in direction of target.
	if t.position.y>c.position.y || t.position.x<c.position.x:
		c.face_left()
	else:
		c.face_right()
	
	# Animations.
	c.tween.remove_all()
	if visible:
		if weapon["animation"] in ANIMATION_CHARGE:
			# Bump character into target.
			c.tween.interpolate_property(c.node.get_node("Base"),"position",c.node.get_node("Base").get_position(),(t.node.get_position()-c.node.get_position())/2.0,0.2,Tween.TRANS_BACK,Tween.EASE_IN)
			c.tween.interpolate_property(c.node.get_node("Base"),"position",c.node.get_node("Base").get_position(),Vector2(0,0),0.2,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT,0.2)
			c.tween.start()
		else:
			# Push character back.
			c.tween.interpolate_property(c.node.get_node("Base"),"position",c.node.get_node("Base").get_position(),(c.node.get_position()-t.node.get_position()).normalized()*8.0,0.125,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
			c.tween.interpolate_property(c.node.get_node("Base"),"position",c.node.get_node("Base").get_position(),Vector2(0,0),0.125,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT,0.125)
			c.tween.start()
		# Add particle effects.
		for dmg in rprt:
			var pi = load("res://scenes/projectiles/"+weapon["animation"]+".tscn")
			if pi!=null && pi.can_instance():
				pi = pi.instance()
				pi.position = c.node.get_global_position()
				pi.creator = c.node
				pi.target = t.node
				tiles.add_child(pi)
			
			pi = load("res://scenes/hit/"+weapon["animation"]+".tscn")
			if pi!=null && pi.can_instance():
				t.node.add_child(pi.instance())
			
			pi = load("res://scenes/particles/"+weapon["animation"]+".tscn")
			if pi!=null && pi.can_instance():
				pi = pi.instance()
				c.node.add_child(pi)
				pi.look_at(t.node.get_global_position())
			
			for ch in c.node.get_node("Base").get_children():
				if ch.has_node("Animation") && ch.get_node("Animation").has_animation("attack"):
					ch.get_node("Animation").play("attack")
			
			if dmg["missed"]:
				t.node.get_node("SoundEvade").play()
			elif dmg["sp"]>0:
				t.node.get_node("SoundShield").play()
	
	return true

func move(actor,dir):
	if !(dir in Generate.DIRECTIONS) || characters[actor].movement_time>=100.0:
		return false
	
	var c = characters[actor]
	if c.action!=null:
		return false
	
	var last_pos = c.position
	var pos = c.position+dir
	pos.x = fposmod(pos.x,Generate.width)
	pos.y = fposmod(pos.y,Generate.height)
	if !Generate.is_walkable(pos) || !is_empty(pos):
		return false
	
	var visible
	c.action = "move"
	if tiles.has_node("Tile_"+str(c.position.x)+"_"+str(c.position.y)):
		tiles.get_node("Tile_"+str(c.position.x)+"_"+str(c.position.y)+"/Outlines").hide()
	if tiles.has_node("Tile_"+str(position.x)+"_"+str(position.y)) && characters.has("player"):
		tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/Outlines").set_modulate(COLOR_ALLY[int(c.faction==characters["player"].faction)])
		tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/Outlines").set_visible(is_visible("player",actor))
	c.position = pos
	c.time = c.movement_time
	move_sprite(c.node,c.position)
	if dir.y>0 || dir.x<0:
		c.face_left()
	else:
		c.face_right()
	visible = is_visible("player",actor)
	if visible && ((!c.node.is_visible() && c.node.get_node("Animation").get_current_animation()!="fade_in") || c.node.get_node("Animation").get_current_animation()=="fade_out"):
		c.node.get_node("Animation").clear_queue()
		c.node.get_node("Animation").queue("fade_in")
	elif !visible && ((c.node.is_visible() && c.node.get_node("Animation").get_current_animation()!="fade_out") || c.node.get_node("Animation").get_current_animation()=="fade_in"):
		c.node.get_node("Animation").clear_queue()
		c.node.get_node("Animation").queue("fade_out")
	
	if actor=="player":
		move_view_to(characters["player"].position)
		if Generate.shops.has(pos):
			get_node("/root/Menu").shopping = true
			get_node("/root/Menu").show_shop(Generate.shops[pos])
		else:
			get_node("/root/Menu").shopping = false
			get_node("/root/Menu").hide_all()
		Mission.on_move(pos)
		for p in Generate.control_points:
			if last_pos==p:
				Mission.on_control_point_left(p)
			if pos==p:
				Mission.on_control_point_entered(p)
	if c.path!=null:
		for en in characters.keys():
			if en!=actor && is_visible(actor,en):
				c.path = null
	speed_up_time()
	return true

func wait(actor):
	if characters[actor].action!=null:
		return false
	if actor=="player" && pick_up_item(actor):
		return true
	
	characters[actor].time = 1.0
	characters[actor].action = "wait"
	
	if actor=="player":
		move_view_to(characters["player"].position)
		for p in Generate.control_points:
			if characters[actor].position==p:
				Mission.on_control_point_hold(p)
	speed_up_time()
	return true

func move_sprite(node,to_pos):
	if tiles.has_node("Tile_"+str(to_pos.x)+"_"+str(to_pos.y)):
		var last_pos = node.get_position()
		node.set_position(tiles.get_node("Tile_"+str(to_pos.x)+"_"+str(to_pos.y)).get_position())
		node.get_node("Tween").remove_all()
		node.get_node("Tween").interpolate_property(node.get_node("Base"),"position",last_pos-node.get_position(),Vector2(0,0),0.2,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
		node.get_node("Tween").start()
		for c in node.get_node("Base").get_children():
			if c.has_node("Animation") && c.get_node("Animation").has_animation("walk"):
				c.get_node("Animation").play("walk")

func pick_up_item(actor):
	if characters[actor].action!=null:
		return false
	
	var pos = characters[actor].position
	if !Generate.items.has(pos):
		return false
	
	for i in range(Generate.items[pos].size()-1,-1,-1):
		if i>=Generate.items[pos].size():
			break
		var hp
		var item = Generate.items[pos][i]
		var amount = 0
		if !Parts.items[item["type"]].has("mass") || Parts.items[item["type"]]["mass"]<=0.0:
			amount = item["amount"]
		else:
			amount = min(item["amount"],floor((get_inventory_size()-Stats.get_inventory_mass(characters[actor].inventory))/Parts.items[item["type"]]["mass"]))
		if amount==0:
			text.push_color(COLOR_DEBUFF)
			text.add_text(tr("ENCUMBERED")+"\n")
			return false
		if item.has("hp"):
			hp = item["hp"]
		add_item(characters[actor],item["type"],item["level"],amount,hp)
		if amount>=item["amount"] && tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/Item_"+item["type"]+"_"+str(item["level"])):
			tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/Item_"+item["type"]+"_"+str(item["level"])).queue_free()
		if actor=="player":
			text.push_color(COLOR_REWARD)
			if amount==1:
				text.add_text(tr("PICKED_UP_ITEM").replace("{item}",tr(item["type"].to_upper()))+"\n")
			else:
				text.add_text(tr("PICKED_UP_ITEM").replace("{item}",str(amount)+"x "+tr(item["type"].to_upper()))+"\n")
			Mission.on_pick_up(pos,item)
		if amount>=item["amount"]:
			Generate.items[pos].remove(i)
		else:
			Generate.items[pos][i]["amount"] -= amount
	if actor=="player":
		if get_node("/root/Menu/Inventory").is_visible():
			get_node("/root/Menu").update_inventory()
	characters[actor].time = 1.0
	characters[actor].action = "wait"
	get_node("/root/Menu").update_inventory()
	return true

func drop_item(actor,ID,amount=1):
	if characters[actor].action!=null || characters[actor].inventory.size()<=ID:
		return false
	
	var pos = characters[actor].position
	var item = characters[actor].inventory[ID]
	_drop_item(pos,item,amount)
	remove_item(characters[actor],ID,amount)
	characters[actor].time = 1.0
	characters[actor].action = "wait"
	if actor=="player":
		text.push_color(COLOR_REWARD)
		text.add_text(tr("DROPED_UP_ITEM").replace("{item}",tr(item["type"].to_upper()))+"\n")
		Mission.on_drop(pos,item)
	return true

func _drop_item(pos,item,amount=null):
	var i
	if amount==null && item.has("amount"):
		amount = item["amount"]
	if amount<=0:
		amount = 1
	if !Generate.items.has(pos):
		Generate.items[pos] = []
	else:
		for j in range(Generate.items[pos].size()):
			if Generate.items[pos][j]["type"]==item["type"] && Generate.items[pos][j]["level"]==item["level"] && item.has("hp") && Generate.items[pos][j]["hp"]==item["hp"]:
				i = j
				break
	
	if i==null:
		var it = {"type":item["type"],"level":item["level"],"amount":amount}
		if item.has("hp"):
			it["hp"] = item["hp"]
		Generate.items[pos].push_back(it)
	else:
		Generate.items[pos][i]["amount"] += amount
	if tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)):
		var ii = Sprite.new()
		var texture = load("res://images/icons/items/"+Parts.items[item["type"]]["icon"]+".png")
		if texture==null && Parts.items[item["type"]].has("slot"):
			var s = Parts.items[item["type"]]["slot"].replace("_","")
			for _i in range(10):
				s = s.replace(str(i),"")
			texture = load("res://images/icons/slots/"+s+".png")
		ii.set_texture(texture)
		ii.set_name("Item_"+item["type"]+"_"+str(item["level"]))
		tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)).add_child(ii)
	

func drop_gold(actor,amount):
	var pos = characters[actor].position
	if Generate.items.has(pos) || amount<=0:
		return false
	
	var i
	if !Generate.items.has(pos):
		Generate.items[pos] = []
	else:
		for j in range(Generate.items[pos].size()):
			if Generate.items[pos][j]["type"]=="gold" && Generate.items[pos][j]["level"]==0:
				i = j
				break
	
	if i==null:
		Generate.items[pos].push_back({"type":"gold","level":0,"amount":amount})
	else:
		Generate.items[pos][i]["amount"] += amount
	if tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)):
		var ii = Sprite.new()
		ii.set_texture(load("res://images/icons/items/"+Parts.items["gold"]["icon"]+".png"))
		ii.set_name("Item_gold_0")
		tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)).add_child(ii)
	return true


### helper functions

func is_empty(pos):
	# Returns true if the tile is empty, false otherwise.
	pos.x = fposmod(pos.x,Generate.width)
	pos.y = fposmod(pos.y,Generate.height)
	for c in characters.values():
		if c.position==pos:
			return false
	return true

func get_distance(pos1,pos2):
	# Returns the shortest distance between to positions.
	var dist
	if abs(pos1.x-pos2.x-Generate.width)<abs(pos1.x-pos2.x):
		pos2.x += Generate.width
	elif abs(pos1.x-pos2.x+Generate.width)<abs(pos1.x-pos2.x):
		pos2.x -= Generate.width
	if abs(pos1.y-pos2.y-Generate.height)<abs(pos1.y-pos2.y):
		pos2.y += Generate.height
	elif abs(pos1.y-pos2.y+Generate.height)<abs(pos1.y-pos2.y):
		pos2.y -= Generate.height
	dist = max(max(abs(pos1.x-pos2.x),abs(pos1.y-pos2.y)),abs(pos1.x-pos2.x-pos1.y+pos2.y))
	return dist

func is_visible(character,target):
	# Returns true if character can see target, false otherwise.
	if !characters.has(target):
		return false
	if character==target || !characters.has("player"):
		return true
	var dist = get_distance(characters[character].position,characters[target].position)
	return dist<min(characters[character].stats["sensor_strength"]-characters[target].stats["cloak_strength"]+characters[target].noise,MAX_VIEW_DIST)

func get_character_at_pos(pos):
	# Returns the character at the given position, null if no character exists there.
	pos.x = fposmod(pos.x,Generate.width)
	pos.y = fposmod(pos.y,Generate.height)
	for character in characters.keys():
		if characters[character].position==pos:
			return character
	return

func get_title(character):
	# Returns a fancy title based on the character's skills and equipment.
	if characters.has(character):
		character = characters[character]
	var tags = get_equipment_tags(character.equipment,{})
	var title = "ROOKIE"
	var score = 0
	for skill in character.skills.keys():
		if tags.has(skill):
			tags[skill] += 0.25*character.skills[skill]
		else:
			tags[skill] = 0.25*character.skills[skill]
	if tags.has("backstab"):
		var value = 2*tags["backstab"]
		if value>score:
			title = "ROGUE"
			score = value
		if tags.has("stealth"):
			value = 1.5*tags["backstab"]+0.5*tags["stealth"]-4
			if value>score:
				title = "ASSASSIN"
				score = value
	if tags.has("shield"):
		var value = 2*tags["shield"]
		if value>score:
			title = "SHIELDER"
			score = value
		if tags.has("melee") && tags.has("armor"):
			value = 0.75*tags["melee"]+tags["shield"]+0.25*tags["armor"]-3
			if value>score:
				title = "KNIGHT"
				score = value
	if tags.has("engineering"):
		var value = 2.5*tags["engineering"]-2
		if value>score:
			title = "ENGINEER"
			score = value
		if tags.has("construction"):
			value = 1.5*tags["engineering"]+1.5*tags["construction"]-4
			if value>score:
				title = "TECHNOMANCER"
				score = value
	if tags.has("stealth"):
		var value = tags["stealth"]-5
		if value>score:
			title = "SHADOW"
			score = value
		value = 0.75*tags["stealth"]-3
		if tags.has("evasion"):
			value += 0.5*tags["evasion"]
		if tags.has("sensors"):
			value += 0.2*tags["sensors"]
		if value>score:
			title = "PHANTOM"
			score = value
	if tags.has("sniper"):
		var value = 2*tags["sniper"]-2
		if value>score:
			title = "SNIPER"
			score = value
	if tags.has("melee"):
		if tags.has("2h"):
			var value = tags["2h"]+tags["melee"]
			if value>score:
				title = "WARRIOR"
				score = value
			value = tags["2h"]+tags["melee"]
			if tags.has("armor"):
				value -= 0.25*tags["armor"]
			if value>score:
				title = "BERSERKER"
				score = value
		elif tags.has("1h") && tags.has("evasion"):
			var value = tags["1h"]+0.5*tags["melee"]+0.25*tags["evasion"]
			if tags.has("armor"):
				value -= 0.25*tags["armor"]
			if value>score:
				title = "MARAUDER"
				score = value
	if tags.has("propulsion") && tags.has("2h"):
		if tags.has("1h"):
			var value = (tags["1h"]-0.5*tags["2h"]+2*tags["propulsion"])/2-2
			if tags.has("melee"):
				value += 0.3*tags["melee"]
			if tags.has("ranged"):
				value += 0.3*tags["ranged"]
			if value>score:
				title = "SKIRMISHER"
				score = value
		var value = 2*tags["propulsion"]-tags["2h"]-1
		if value>score:
			title = "RAIDER"
			score = value
	if tags.has("ranged"):
		var value = tags["ranged"]
		if value>score:
			title = "GUNNER"
			score = value
	if tags.has("explosive"):
		var value = tags["explosive"]-2
		if tags.has("ranged"):
			value += 0.2*tags["ranged"]
		if tags.has("armor"):
			value += 0.1*tags["armor"]
		if value>score:
			title = "PYROMANCER"
			value = score
	
	return title

func get_inventory_size():
	if !characters.has("player"):
		return inventory_size
	return inventory_size+characters["player"].base_stats["str"]


### save / load

func _save():
	var file := File.new()
	var error := file.open("user://saves/"+characters["player"].name+".sav",File.WRITE)
	
	if error!=OK:
		var dir := Directory.new()
		error = dir.make_dir_recursive("user://saves/")
		printt("Creating save dir directory...",error)
		error = file.open("user://saves/"+characters["player"].name+".sav",File.WRITE)
		if error!=OK:
			printt("Can't save file saves/"+characters["player"].name+".sav!",error)
			return
	
	var chars := {}
	for k in characters.keys():
		chars[k] = characters[k].to_dict()
	
	text.push_color(COLOR_DEFAULT)
	text.add_text(tr("SAVING")+"\n")
	file.store_line(JSON.print({"version":VERSION}))
	file.store_line(JSON.print(chars))
	file.store_line(JSON.print({"level":Generate.level,"type":Generate.type,"width":Generate.width,"height":Generate.height,"shuttle_timer":shuttle_timer,"gold":Generate.gold,"mission_penalty":mission_penalty,"drops":Generate.drops,"shops":Generate.shops}))
	file.store_line(JSON.print(Generate.ground_tiles))
	file.store_line(JSON.print(Generate.wall_tiles))
	file.store_line(JSON.print(Generate.items))
	if mission!=null:
		file.store_line(JSON.print(mission.to_dict()))
	file.close()

func _load(filename):
	var file := File.new()
	var error := file.open("user://saves/"+filename,File.READ)
	var ground_tiles
	var wall_tiles
	var items
	
	if error!=OK:
		print("Can't load file saves/"+filename+"!")
		return false
	
	var currentline = JSON.parse(file.get_line())
	if currentline.error!=OK || currentline.result["version"]>VERSION:
		print("Can't load file saves/"+filename+"!")
		return false
	
	reset()
	currentline = JSON.parse(file.get_line())
	if currentline.error!=OK:
		printt("Can't parse character data.")
		return false
	currentline = currentline.result
	for k in currentline.keys():
		var c = currentline[k]
		var equipment
		var a = c["position"].split(",",true,1)
		var pos = Vector2(int(a[0]),int(a[1]))
		if !c.has("cooldown"):
			c["cooldown"] = null
		equipment = Part.new(c["equipment"]["type"],c["equipment"]["level"],"/",null,false,null,c["equipment"]["hp"])
		equipment.reset(equipment,c["skills"])
		add_eq(equipment,c["equipment"]["slots"],equipment,c["skills"])
		characters[k] = Character.new(c["name"],c["level"],c["experience"],c["base_stats"],c["skills"],c["traits"],c["inventory"],equipment,c["faction"],c["species"],pos,c["ai_type"],c["skill_points"],c["stat_points"],c["status"],c["action"],c["time"],c["noise"],c["drop_rate"],c["cooldown"],c["summoned"],c["duration"])
	if characters.has("player"):
		player = characters["player"]
		position = characters["player"].position
		last_position = position
	
	currentline = JSON.parse(file.get_line())
	if currentline.error!=OK:
		printt("Can't parse level data.")
		return false
	currentline = currentline.result
	Generate.level = int(currentline["level"])
	Generate.type = currentline["type"]
	Generate.width = int(currentline["width"])
	Generate.height = int(currentline["height"])
	Generate.gold = float(currentline["gold"])
	Generate.drops = Array(currentline["drops"])
	shuttle_timer = currentline["shuttle_timer"]
	mission_penalty = currentline["mission_penalty"]
	for i in range(currentline["shops"].size()):	# convert strings to Vector2
		var c = currentline["shops"].keys()[i].split(",",true,1)
		Generate.shops[Vector2(int(c[0]),int(c[1]))] = currentline["shops"].values()[i]
	
	currentline = JSON.parse(file.get_line())
	if currentline.error!=OK:
		printt("Can't parse level data.")
		return false
	currentline = currentline.result
	ground_tiles = currentline
	for s in ground_tiles.keys():					# convert strings to Vector2
		var c = s.split(",",true,1)
		var p = Vector2(int(c[0]),int(c[1]))
		Generate.ground_tiles[p] = ground_tiles[s]
	
	currentline = JSON.parse(file.get_line())
	if currentline.error!=OK:
		printt("Can't parse level data.")
		return false
	currentline = currentline.result
	wall_tiles = currentline
	for s in wall_tiles.keys():						# convert strings to Vector2
		var c = s.split(",",true,1)
		var p = Vector2(int(c[0]),int(c[1]))
		Generate.wall_tiles[p] = wall_tiles[s]
	
	currentline = JSON.parse(file.get_line())
	if currentline.error!=OK:
		printt("Can't parse level data.")
		return false
	currentline = currentline.result
	items = currentline
	for s in items.keys():							# convert strings to Vector2
		var c = s.split(",",true,1)
		var p = Vector2(int(c[0]),int(c[1]))
		Generate.items[p] = items[s]
	
	Generate.update_map()
	
	currentline = JSON.parse(file.get_line())
	if currentline.error==OK:
		currentline = currentline.result
		mission = Mission.call("new_"+currentline["type"],currentline["vars"])
	
	HUD.get_node("TouchMenus/Launch").set_disabled(!can_launch())
	for s in items.keys():							# display dropped items
		if items[s].size()==0:
			continue
		var c = s.split(",",true,1)
		var pos = Vector2(int(c[0]),int(c[1]))
		var item = items[s][0]
		if tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)):
			var ii = Sprite.new()
			var texture = load("res://images/icons/items/"+Parts.items[item["type"]]["icon"]+".png")
			if texture==null && Parts.items[item["type"]].has("slot"):
				var st = Parts.items[item["type"]]["slot"].replace("_","")
				for i in range(10):
					st = st.replace(str(i),"")
				texture = load("res://images/icons/slots/"+st+".png")
			ii.set_texture(texture)
			ii.set_name("Item_"+item["type"]+"_"+str(item["level"]))
			tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)).add_child(ii)
	
	if characters.has("player") && (player.skill_points>0 || player.stat_points>0):
		text.push_color(COLOR_DEFAULT)
		text.add_text(tr("UNUSED_SKILL_POINTS")+"\n")
	
	for x in range(Generate.width):
		for y in range(Generate.height):
			_tile_clear(Vector2(x,y))
	Generate.update_tiles()
	position = characters["player"].position
	camera_position = tiles.get_node("Tile_"+str(position.x)+"_"+str(position.y)).position
	camera_last_position = camera_position
	camera.position = camera_position
	return true

func delete_char():
	# This is a rogue-like, so let's just delete the save file of the current character.
	var dir = Directory.new()
	dir.remove("user://saves/"+player.name+".sav")


func add_eq(equipment,dict,eq,skills):
	if equipment==null:
		return
	
	for k in dict.keys():
		if dict[k]!=null:
			var c = dict[k]
			equipment.slots[k].type = c["type"]
			if c.has("level"):
				equipment.slots[k].level = c["level"]
			if c.has("hp"):
				equipment.slots[k].hp = c["hp"]
			if c.has("copy") && c["copy"]:
				var part = get_part(c["parent"],eq)
				equipment.slots[k].copy = true
				equipment.slots[k].parent = c["parent"]
				equipment.slots[k].type = part.type
				equipment.slots[k].level = part.level
			equipment.slots[k].reset(eq,skills)
			if c.has("slots"):
				add_eq(equipment.slots[k],c["slots"],eq,skills)
	equipment.update(eq)

func reset():
	characters.clear()
	Generate.ground_tiles.clear()
	Generate.wall_tiles.clear()
	Generate.items.clear()
	Generate.drops.clear()
	Generate.shops.clear()
	Generate.control_points.clear()
	text.clear()
	last_position = Vector2()
	camera_position = Vector2()
	camera_last_position = Vector2()
	mission = null
	HUD.get_node("Mission").hide()
	for c in HUD.get_node("Effects").get_children():
		c.queue_free()
	for c in tiles.get_children():
		c.set_name("deleted")
		c.queue_free()
	get_node("TextureRect").set_modulate(Color(1.0,1.0,1.0,1.0))

func change_level(type,level,pos):
	# Try to change the level.
	if !Generate.has_method("generate_"+type):
		print("Can't create level "+type+"!")
		return
	
	player = characters["player"]
	reset()
	Generate.call("generate_"+type,level)
	if pos==null:
		pos = Generate.get_random_position()
	characters["player"] = player
	characters["player"].position = pos
	characters["player"].update_sprite()
	position = pos
	if safe && cleared:
		text.push_color(COLOR_DEFAULT)
		text.add_text(tr("SAFE_PLANET").format({"key":OS.get_scancode_string(get_scancode("launch"))})+"\n\n")
		shuttle_timer = 0.0
		HUD.get_node("TouchMenus/Launch/Progress").hide()
	else:
		if player.skills.has("emergency_shuttle"):
			shuttle_timer = 1200.0*3.0/(3.0+player.skills["emergency_shuttle"])
		else:
			shuttle_timer = 1200.0
		HUD.get_node("TouchMenus/Launch/Progress").max_value = shuttle_timer
		HUD.get_node("TouchMenus/Launch/Progress").value = shuttle_timer
		HUD.get_node("TouchMenus/Launch/Progress").show()
	if player.skill_points>0 || player.stat_points>0:
		text.push_color(COLOR_DEFAULT)
		text.add_text(tr("UNUSED_SKILL_POINTS")+"\n")
	HUD.get_node("TouchMenus/Launch").set_disabled(!can_launch())
	for x in range(Generate.width):
		for y in range(Generate.height):
			_tile_clear(Vector2(x,y))

func character_died():
	if !characters.has("player"):
		delete_char()
		yield(get_tree(),"idle_frame")
		get_node("/root/Menu").game_over()

func get_key(action):
	for event in InputMap.get_action_list(action):
		if event is InputEventKey:
			return event

func get_scancode(action):
	var scancode = 0
	var event = get_key(action)
	if event!=null:
		scancode = event.scancode
	return scancode

func reset_time_scale():
	# Set time scale to default value.
	time_scale = 10.0

func speed_up_time():
	# Set time scale to a larger value in order to speed up the delay due to slow actions.
	time_scale = 1000.0
	for c in characters.values():
		var scale = Engine.get_frames_per_second()/max(c.time,0.01)
		if scale<time_scale:
			time_scale = scale
		for s in c.status:
			scale = Engine.get_frames_per_second()/max(s["time"],0.01)
			if scale<time_scale:
				time_scale = scale
	if time_scale<10.0:
		time_scale = 10.0

func _process(delta):
	if !characters.has("player") || characters["player"].node==null:
		return
	
	var mouse_pos = get_node("TextureRect").get_local_mouse_position()
	
	# update HUD
	HUD.get_node("Status/HP").set_max(characters["player"].hp_max)
	HUD.get_node("Status/HP").set_value(characters["player"].hp)
	HUD.get_node("Status/SP").set_max(characters["player"].stats["sp"])
	HUD.get_node("Status/SP").set_value(characters["player"].sp)
	HUD.get_node("Status/EP").set_max(characters["player"].stats["ep"])
	HUD.get_node("Status/EP").set_value(characters["player"].ep)
	HUD.get_node("Status/ST").set_max(characters["player"].stats["cloak_strength"])
	HUD.get_node("Status/ST").set_value(characters["player"].stats["cloak_strength"]-characters["player"].noise)
	if characters["player"].equipment.stats.has("hp"):
		HUD.get_node("Status/MS").set_max(characters["player"].equipment.stats["hp"])
	else:
		HUD.get_node("Status/MS").set_max(characters["player"].equipment.hp)
	HUD.get_node("Status/MS").set_value(characters["player"].equipment.hp)
	for t in HUD.get_node("Status").special_parts.keys():
		HUD.get_node("Status/"+t+"S").set_max(0)
		HUD.get_node("Status/"+t+"S").set_value(0)
		for part in HUD.get_node("Status").special_parts[t]:
			if part.stats.has("hp"):
				HUD.get_node("Status/"+t+"S").max_value += part.stats["hp"]
			HUD.get_node("Status/"+t+"S").value += part.hp*(1.0-float(part.disabled))
		if HUD.get_node("Status/"+t+"S").max_value<=0:
			HUD.get_node("Status/"+t+"S").max_value = 1
	HUD.get_node("TouchMenus/Launch/Progress").value = shuttle_timer
	
	# wrap around edges
	if position.x>Generate.width:
		position.x -= Generate.width
	elif position.x<0:
		position.x += Generate.width
	if position.y>Generate.height:
		position.y -= Generate.height
	elif position.y<0:
		position.y += Generate.height
	
	# move camera
	if position!=last_position && tiles.has_node("Tile_"+str(last_position.x)+"_"+str(last_position.y)) && tiles.has_node("Tile_"+str(position.x)+"_"+str(position.y)):
		Generate.update_tiles()
		camera_position = tiles.get_node("Tile_"+str(last_position.x)+"_"+str(last_position.y)).position
		camera_last_position = tiles.get_node("Tile_"+str(position.x)+"_"+str(position.y)).position
	last_position = position
	camera_position += delta*camera_speed*(camera_last_position-camera_position)
	camera.position = Vector2(round(camera_position.x),round(camera_position.y))
	
	HUD.get_node("FPS").set_text(tr("FPS")+": "+str(Engine.get_frames_per_second()))
	
	# mouse input
	if mouse_pos!=last_mouse_pos:
		var new_mouse_tile := Vector2(0,0)
		# scale to range -1.0..1.0
		var scaled_mp = (mouse_pos-OS.window_size/2.0)/OS.window_size*2.0
		var center := Vector2(0.0,-0.4)
		var radius := 1.2
		var r
		var normal
		
		# apply transform of the viewport texture
		scaled_mp += center
		r = min(sqrt(scaled_mp.x*scaled_mp.x+scaled_mp.y*scaled_mp.y)/radius,1.0)
		normal = scaled_mp/cos(PI/2.0*r/2.0)/sqrt(2.0)
		scaled_mp = normal - center
		scaled_mp.y *= OS.window_size.x/OS.window_size.y*3.0/4.0
		scaled_mp *= Vector2(1.25,1.5)
		# calculate position in multiples of tile vectors
		new_mouse_tile.x = fposmod(round(scaled_mp.dot(Generate.TILE_VEC_X)/Generate.TILE_VEC_X.length()*7.5+position.x),Generate.width)
		new_mouse_tile.y = fposmod(round(scaled_mp.dot(Generate.TILE_VEC_Y)/Generate.TILE_VEC_Y.length()*7.5+position.y),Generate.height)
		if new_mouse_tile!=mouse_tile:
			_tile_clear(mouse_tile)
			mouse_tile = new_mouse_tile
			_tile_hover(mouse_tile)
		last_mouse_pos = mouse_pos
	
	if pause:
		return
	
	time += delta*time_scale
	shuttle_timer -= delta*time_scale
	Mission.on_process(delta*time_scale)
	
	# update characters
	for name in characters.keys():
		if !characters.has(name):
			continue
		
		var c = characters[name]
		# Reduce life time if summoned.
		if c.summoned:
			c.duration -= delta*time_scale
			if c.duration<=0.0:
				c.equipment.hp = 0
				c.equipment.disabled = true
				c.damaged()
				continue
		# Reduce action delay.
		c.time -= delta*time_scale
		if c.time<=0.0:
			c.action = null
			reset_time_scale()
			for _c in c.node.get_node("Base").get_children():
				if _c.has_node("Animation") && _c.get_node("Animation").has_animation("walk"):
					_c.get_node("Animation").play("walk")
			if c.path!=null && c.path.size()>0:
				# Continue to move on the path.
				var dir = c.path[0]-c.position
				if abs(dir.x)>abs(dir.x-Generate.width):
					dir.x -= Generate.width
				elif abs(dir.x)>abs(dir.x+Generate.width):
					dir.x += Generate.width
				if abs(dir.y)>abs(dir.y-Generate.height):
					dir.y -= Generate.height
				elif abs(dir.y)>abs(dir.y+Generate.height):
					dir.y += Generate.height
				if dir!=null:
					var moved = move(name,dir)
					if !moved:
						c.path = null
				if c.path!=null:
					c.path.pop_front()
			elif c.ai_type=="player":
				# Pause, wait for player input.
				pause = true
			else:
				# AI controled.
				Enemy.call(c.ai_type,name)
		
		# Action cooldowns.
		for act in c.actions:
			if c.cooldown.has(act["ID"]):
				c.cooldown[act["ID"]] -= delta*time_scale
		# Repair, increase ep and sp.
		c.auto_repair(delta*time_scale)
		c.sp += c.stats["sp_reg"]*delta*time_scale
		if c.sp>c.stats["sp"]:
			c.sp = c.stats["sp"]
		c.ep += c.stats["ep_reg"]*delta*time_scale
		if c.ep>c.stats["ep"]:
			c.ep = c.stats["ep"]
		# Noise reduction.
		if c.action=="attack":
			c.noise *= 0.995
		else:
			c.noise *= 0.98
		c.noise -= 0.1*delta*time_scale
		if c.noise<0.0:
			c.noise = 0.0
		# Reduce status effect dration.
		for st in c.status:
			st["time"] -= delta*time_scale
			if st["time"]<=0.0:
				remove_status(name,st)
	if characters.has("player"):
		HUD.update_status_duration("player")
	


### user input

func _tile_selected(pos):
	# Select the tile at a certain position.
	# Attack if a attack is selected and an enemy present, move there otherwise.
	if !characters.has("player"):
		return
	
	var attacked = false
	characters["player"].path = null
	if action_selected!=null && characters["player"].actions[action_selected]["type"]=="attack":
		var target = get_character_at_pos(pos)
		if target!=null:
			# Try to attack.
			_tile_clear(pos)
			attacked = attack("player",target,characters["player"].actions[action_selected])
			return
	if !attacked:
		if characters["player"].position==pos:
			# Player tile was selected. Wait in this case.
			wait("player")
		elif get_distance(characters["player"].position,pos)==1:
			# Move by one tile.
			var dir = pos-characters["player"].position
			# Wrapping near map edges.
			if abs(dir.x)>abs(dir.x-Generate.width):
				dir.x -= Generate.width
			elif abs(dir.x)>abs(dir.x+Generate.width):
				dir.x += Generate.width
			elif abs(dir.y)>abs(dir.y-Generate.height):
				dir.y -= Generate.height
			elif abs(dir.y)>abs(dir.y+Generate.height):
				dir.y += Generate.height
			_tile_clear(pos)
			move("player",dir)
		else:
			# Find path to target position.
			_tile_clear(pos)
			characters["player"].path = Generate._get_path(characters["player"],pos)
			characters["player"].path.pop_front()
			pause = false

func _tile_hover(pos):
	# Focus tile at a certain position.
	if !characters.has("player"):
		return
	
	var txt = tr("DISTANCE")+": "+str(get_distance(characters["player"].position,pos))
	var target = get_character_at_pos(pos)
	if target!=null && is_visible("player",target):
		var t = characters[target]
		txt += "\n"+tr(t.name)+" "+tr("LVL")+" "+str(t.level)+"\n"+tr("HP")+": "+str(round(100*t.hp/t.hp_max))+"%"
	if Generate.items.has(pos):
		txt += "\n"+str(Generate.items[pos].size())+" "+tr("ITEM_S")
	HUD.get_node("Tooltip/Text").set_text(txt)
	HUD.get_node("Tooltip").set_global_position(HUD.get_node("Tooltip").get_global_mouse_position())
	HUD.get_node("Tooltip").show()
	tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/Outlines").set_modulate(COLOR_HOVER)
	tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/Outlines").show()

func _tile_clear(pos):
	# Clear tile focus.
	var c = get_character_at_pos(pos)
	HUD.get_node("Tooltip").hide()
	if !tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)):
		return
	if c!=null && characters.has("player"):
		tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/Outlines").set_modulate(COLOR_ALLY[int(characters[c].faction==characters["player"].faction)])
		tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/Outlines").set_visible(is_visible("player",c))
	else:
		tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)+"/Outlines").hide()

func inspect(target):
	# View target equipment.
	view_character = target
	get_node("/root/Menu").update_inspector()
	HUD.get_node("Tooltip").hide()

func move_view_to(pos):
	position = pos
	HUD.get_node("Tooltip").hide()

func _select_action(action):
	var act
	var c = characters["player"]
	if action>=c.actions.size():
		return
	
	action_selected = action
	for i in range(c.actions.size()):
		HUD.get_node("Actions/Button"+str(i)).set_pressed(false)
	HUD.get_node("Actions/Button"+str(action)).set_pressed(true)
	
	act = c.actions[action_selected]
	if act["type"]=="outfit" && act["target"]=="self" && c.ep>=act["ep_cost"] && !(c.cooldown.has(act["ID"]) && c.cooldown[act["ID"]]>0.0):
		var success = act["use"].call_func(act["part"],characters["player"])
		if success:
			var pi = load("res://scenes/particles/"+act["animation"]+".tscn")
			if pi!=null && pi.can_instance():
				pi = pi.instance()
				c.node.add_child(pi)
			c.action = "use_outfit"
			c.time = act["attack_delay"]
			c.noise = min(c.noise+max(act["noise"],0),MAX_NOISE)
			c.ep -= act["ep_cost"]
			if act.has("cool_down"):
				c.cooldown[act["ID"]] = act["cool_down"]
			speed_up_time()

func move_player(character,direction={"move_n":Input.is_action_pressed("move_n"),"move_nw":Input.is_action_pressed("move_nw"),"move_ne":Input.is_action_pressed("move_ne"),"move_s":Input.is_action_pressed("move_s"),"move_sw":Input.is_action_pressed("move_sw"),"move_se":Input.is_action_pressed("move_se")}):
	# Move the player. Input is a dictionary with movement keys pressed.
	var dir = {"move_n":Vector2(-1,-1),"move_nw":Vector2(-1,0),"move_ne":Vector2(0,-1),"move_s":Vector2(1,1),"move_sw":Vector2(0,1),"move_se":Vector2(1,0)}
	for k in dir.keys():
		if direction.has(k) && direction[k]:
			_tile_clear(characters[character].position)
			if is_empty(characters[character].position+dir[k]):
				move(character,dir[k])
			elif action_selected!=null && characters[character].actions[action_selected]["type"]=="attack":
				var target = get_character_at_pos(characters[character].position+dir[k])
				attack(character,target,characters[character].actions[action_selected])
			return

func can_launch():
	# Check if player can leave the planet. The mission has to be done and no enemy within range 8.
	if !cleared && shuttle_timer>0.0:
		return false
	for n in characters.keys():
		if characters[n].faction=="player":
			continue
		if get_distance(characters["player"].position,characters[n].position)<8:
			return false
	return true

func launch():
	# Leave the planet.
	if can_launch():
		Mission.on_launch()
		for i in range(characters["player"].inventory.size()-1,-1,-1):
			if characters["player"].inventory[i]["type"] in ["waste","toxic_waste","radioactive_waste","contaminated_tree","contaminated_bush","contaminated_grass","drill"]:
				characters["player"].inventory.remove(i)
		mission = null
		if !cleared:
			mission_penalty = (mission_penalty+1.0)/2.0
		else:
			mission_penalty /= 2.0
		Generate.control_points.clear()
		get_node("/root/Menu").change_planet(Generate.level+1)
		_tile_clear(characters["player"].position)
	else:
		text.push_color(COLOR_DEFAULT)
		text.add_text(tr("CANT_LEAVE"))
		if cleared || shuttle_timer<=0.0:
			text.add_text(" "+tr("ENEMIES_NEAR"))
		text.add_text("\n")

func _input(event):
	if !characters.has("player"):
		return
	
	if !(event is InputEventMouseMotion):
		characters["player"].path = null
	if event is InputEventKey:
		if event.is_action_pressed("wait"):
			if position!=characters["player"].position:
				move_view_to(characters["player"].position)
			else:
				wait("player")
			return
		elif event.is_action_pressed("pick_up"):
			pick_up_item("player")
			return
		elif event.is_action_pressed("launch"):
			launch()
			return
		
		for i in range(10):
			if event.is_action_pressed("action"+str(i+1)):
				_select_action(i)
				return
		move_player("player")
	elif event is InputEventScreenDrag:
		if abs(event.position.x-OS.get_window_size().x/2)<OS.get_window_size().x/4 && abs(event.position.y-OS.get_window_size().y/2)<OS.get_window_size().y/4:
			var offset = Vector2()
			offset.x = -round(0.001*(event.speed.dot(Generate.TILE_VEC_X)))
			offset.y = -round(0.001*(event.speed.dot(Generate.TILE_VEC_Y)))
			move_view_to(position+offset)
	
	if characters.has("player") && characters["player"].action!=null:
		pause = false

func _viewport_input(event):
	if event is InputEventMouseButton && event.pressed:
		if !pause || get_node("/root/Menu/Panel").visible || get_node("/root/Menu/Equipment").visible || get_node("/root/Menu/Inventory").visible || get_node("/root/Menu/Shop").visible || get_node("/root/Menu/Stats").visible || get_node("/root/Menu/Skills").visible || get_node("/root/Menu/Inspect").visible || get_node("/root/Menu/Map").visible || get_node("/root/Menu/Load").visible:
			if !get_node("/root/Menu/Map").visible && !get_node("/root/Menu/Load").visible:
				get_node("/root/Menu").toggle_menu()
			return
		if event.button_index==1:
			_tile_selected(mouse_tile)
		elif event.button_index==2:
			var target = get_character_at_pos(mouse_tile)
			if target!=null:
				Main.inspect(target)
			Main.move_view_to(mouse_tile)

func _ready():
	randomize()
	text.set_scroll_follow(true)
	get_node("TextureRect").texture = get_node("Viewport").get_texture()
	get_node("TextureRect").connect("gui_input",self,"_viewport_input")
