extends Node

var current := ""
var next

onready var player = get_node("StreamPlayer")
onready var animation = get_node("AnimationPlayer")


func change_music(new):
	# Change the music to a new track. Fade out/fade in if music is already playing.
	if player.is_playing():
		if animation.is_playing():
			if animation.get_current_animation()!="fade_out":
				yield(animation,"animation_finished")
				animation.queue("fade_out")
		else:
			animation.play("fade_out")
		yield(animation,"animation_finished")
	
	current = new
	next = null
	player.set_stream(load("res://music/"+current))
	player.play()
	animation.play("fade_in")

func queue(new):
	# Change the track to the new one after the last one is finished.
	if player.is_playing():
		yield(player,"finished")
	change_music(new)

func fade_out(time):
	animation.play("fade_out",-1,0.2/time)

func _finished():
	if next!=null:
		current = next
		next = null
		player.set_stream(load("res://music/"+current))
		player.set_volume_db(0)
		player.play()

func _ready():
	player.connect("finished",self,"_finished")
