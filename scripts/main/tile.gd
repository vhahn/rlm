extends TextureButton

func input(event,pos):
	if event is InputEventMouseButton and event.button_index==2:
		var target = Main.get_character_at_pos(pos)
		if target!=null:
			Main.inspect(target)
		Main.move_view_to(pos)
