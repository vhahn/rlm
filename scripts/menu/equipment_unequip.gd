extends ScrollContainer

func can_drop_data(_position,data):
	return data["type"]=="equipment" || data["type"]=="shop"

func drop_data(_position,data):
	if data["type"]=="equipment":
		get_node("/root/Menu").unequip(data["path"])
	elif data["type"]=="shop":
		get_node("/root/Menu").buy_item(data["ID"])
