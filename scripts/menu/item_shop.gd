extends Control

var ID
var tooltip
var slot


func get_drag_data(_position):
	if ID==null:
		return
	
	var pi = self.duplicate(0)
	set_drag_preview(pi)
	return {"ID":ID,"type":"shop"}

func _gui_input(event):
	if !(event is InputEventMouseButton && !event.is_pressed() && event.button_index==BUTTON_LEFT) || ID==null:
		return
	
	get_node("/root/Menu").buy_item(ID)

func _show_tooltip():
	if tooltip==null:
		return
	get_node("/root/Menu").show_tooltip(tooltip)
	get_node("/root/Menu").highlight_slot_type(slot)

func _ready():
	connect("gui_input",self,"_gui_input")
	connect("mouse_entered",self,"_show_tooltip")
	connect("mouse_exited",get_node("/root/Menu"),"_hide_tooltip")
