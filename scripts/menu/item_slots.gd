extends Control

const CORNER_POS = [Vector2(-17,-28),Vector2(17,-28),Vector2(26,-14),Vector2(26,14),Vector2(17,28),Vector2(-17,28),Vector2(-26,14),Vector2(-26,-14)]
var slots := []


func draw_line_int(from,to,offset,color,f,t):
	draw_line((1-f)*from+f*to+offset,(1.0-t)*from+t*to+offset,color,4)

func _draw():
	var num = slots.size()
	var offset = get_rect().size/2
	for i in range(slots.size()):
		var length = float(CORNER_POS.size())/num
		var l0 = length*i
		var l1 = length*(i+1)
		var color = get_node("/root/Menu").SLOT_COLOR[slots[i]]
		color.a = 0.5
		for j in range(CORNER_POS.size()):
			if l0<1.0+j && l1>float(j):
				var f = clamp(l0-j,0.0,1.0)
				var t = clamp(l1-j,0.0,1.0)
				draw_line_int(CORNER_POS[j],CORNER_POS[(j+1)%CORNER_POS.size()],offset,color,f,t)
	for i in range(slots.size()):
		var length = float(CORNER_POS.size())/num
		var l1 = length*(i+1)
		for j in range(CORNER_POS.size()):
			if l1<=j+1:
				var w = clamp(l1-j,0.0,1.0)
				draw_circle((1-w)*CORNER_POS[j]+w*CORNER_POS[(j+1)%CORNER_POS.size()]+offset,3,Color(0.0,0.0,0.0,0.5))
				break
