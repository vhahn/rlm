extends CanvasLayer

const BG_WIDTH = 30
const BG_HEIGHT = 35
const BG_ZONE_TILES = ["tundra01","dirt01","grass01","ocean01","grass01","grass01","dirt01","desert01","dirt01","dirt01","tundra01","tundra01"]
const BG_TILE_SCENES = {
	"tundra01":preload("res://scenes/tiles/tundra01.tscn"),
	"dirt01":preload("res://scenes/tiles/dirt01.tscn"),
	"grass01":preload("res://scenes/tiles/grass01.tscn"),
	"ocean01":preload("res://scenes/tiles/ocean01.tscn"),
	"desert01":preload("res://scenes/tiles/desert01.tscn")
}
const ACTIONS = [
	"move_nw","move_n","move_ne","move_se","move_s","move_sw","wait","pick_up",
	"inventory","equipment","skills","launch","action1","action2","action3",
	"action4","action5","action6","action7","action8","action9","action10"]
const SLOT_SHORT = {
	"main":"MAIN","internal":"INT","external":"EXT","head":"HEAD",
	"arm":"ARM","hand":"HAND","leg":"LEG","propulsion":"PROP",
	"handle":"HND","barrel":"BAR","weapon":"WPN","hardpoint":"HP"
}
const SLOT_COLOR = {
	"main":Color("#c0ff40"),"head":Color("8fb347"),
	"internal":Color("#7030c0"),"external":Color("#20c040"),
	"arm":Color("#ff8020"),"hand":Color("#ff0000"),
	"leg":Color("#4040e0"),"propulsion":Color("#40a0e0"),
	"handle":Color("#995c4d"),"barrel":Color("#994d5c"),
	"weapon":Color("#c04080"),"hardpoint":Color("#8a40e0")
}
const SKILL_CAT = {
	"FIGHTING":[
		"melee","backstab","ranged","sniper"],
	"DEFENSE":[
		"shield","armor","evasion","engineering","construction","emergency_shuttle"],
	"MOVEMENT":[
		"propulsion","sensors","stealth"]
}
const SPECIES = ["HUMAN_MALE","HUMAN_FEMALE","ANDROID"]
const SPECIES_DESC = [
	["HUMAN_DESC"],["HUMAN_DESC"],
	["ANDROID_DESC_1","ANDROID_DESC_2","ANDROID_DESC_3"]
]
const STATS = [
	{"str":11,"int":10,"dex":10},
	{"str":10,"int":10,"dex":11},
	{"str":10,"int":13,"dex":10}
]
const TRAITS = [
	{"exp_multiplier":-0.1},
	{"exp_multiplier":-0.1},
	{"hp_reg":1.0}
]

const CLASSES = ["WARRIOR","GUNNER","ASSASIN","PYROMANCER","ENGINEER"]
const CLASS_DESC = ["WARRIOR_DESC","GUNNER_DESC","ASSASIN_DESC","PYROMANCER_DESC","ENGINEER_DESC"]
const SKILLS = [
	{"armor":3,"evasion":1,"melee":3,"shield":3,"propulsion":1,"sensors":2},
	{"armor":2,"evasion":2,"ranged":3,"propulsion":2,"sensors":2},
	{"armor":1,"evasion":3,"backstab":2,"melee":2,"ranged":2,"propulsion":2,"sensors":1,"stealth":3},
	{"armor":3,"evasion":1,"ranged":3,"propulsion":2,"sensors":2},
	{"evasion":2,"ranged":2,"propulsion":2,"sensors":2,"engineering":3,"construction":2}
]
const INVENTORY = [
	[
		{"type":"light_mech","level":1,"amount":1},
		{"type":"light_head","level":1,"amount":1},
		{"type":"light_arm","level":1,"amount":2},
		{"type":"light_leg","level":1,"amount":2},
		{"type":"great_sword","level":2,"amount":1},
		{"type":"missile_launcher","level":1,"amount":1},
		{"type":"light_armor","level":1,"amount":7},
		{"type":"radar","level":1,"amount":1},
		{"type":"foot","level":1,"amount":2},
		{"type":"metal_alloys","level":1,"amount":3},
		{"type":"gold","level":0,"amount":200}
	],
	[
		{"type":"light_mech","level":1,"amount":1},
		{"type":"light_head","level":1,"amount":1},
		{"type":"light_leg","level":1,"amount":2},
		{"type":"medium_arm","level":1,"amount":1},
		{"type":"light_arm","level":1,"amount":1},
		{"type":"rifle","level":2,"amount":1},
		{"type":"missile_launcher","level":1,"amount":1},
		{"type":"light_armor","level":1,"amount":4},
		{"type":"radar","level":1,"amount":2},
		{"type":"targeting_computer","level":1,"amount":1},
		{"type":"wheels","level":1,"amount":2},
		{"type":"metal_alloys","level":1,"amount":2},
		{"type":"gold","level":0,"amount":100}
	],
	[
		{"type":"light_mech","level":1,"amount":1},
		{"type":"light_head","level":1,"amount":1},
		{"type":"light_arm","level":1,"amount":2},
		{"type":"light_leg","level":1,"amount":2},
		{"type":"dagger","level":2,"amount":1},
		{"type":"chain_gun","level":1,"amount":1},
		{"type":"light_stealth_armor","level":1,"amount":2},
		{"type":"smoke_bomb","level":1,"amount":1},
		{"type":"ecm","level":1,"amount":1},
		{"type":"radar","level":1,"amount":1},
		{"type":"foot","level":1,"amount":2},
		{"type":"gold","level":0,"amount":100}
	],
	[
		{"type":"light_mech","level":1,"amount":1},
		{"type":"light_head","level":1,"amount":1},
		{"type":"light_leg","level":1,"amount":2},
		{"type":"shoulder","level":1,"amount":1},
		{"type":"light_arm","level":1,"amount":2},
		{"type":"weapon_mount","level":2,"amount":1},
		{"type":"missile_launcher","level":1,"amount":2},
		{"type":"swarm_missile_launcher","level":2,"amount":1},
		{"type":"light_armor","level":1,"amount":4},
		{"type":"reinforcement","level":1,"amount":1},
		{"type":"radar","level":1,"amount":1},
		{"type":"targeting_computer","level":1,"amount":1},
		{"type":"wheels","level":1,"amount":2},
		{"type":"metal_alloys","level":1,"amount":2},
		{"type":"gold","level":0,"amount":100}
	],
	[
		{"type":"light_mech","level":1,"amount":1},
		{"type":"light_head","level":1,"amount":1},
		{"type":"light_arm","level":1,"amount":2},
		{"type":"light_leg","level":1,"amount":2},
		{"type":"chain_gun","level":1,"amount":2},
		{"type":"radar","level":1,"amount":1},
		{"type":"emergency_shield","level":1,"amount":1},
		{"type":"portable_turret","level":1,"amount":1},
		{"type":"light_armor","level":1,"amount":4},
		{"type":"reinforcement","level":1,"amount":1},
		{"type":"foot","level":1,"amount":2},
		{"type":"gold","level":0,"amount":100}
	]
]
const LEVELS = ["terran","warm","desert","ice_ball","swamp","lava","toxic"]
const PLANET_TEXTURES = {
	"terran":["planets/planet01_diffuse.png","planets/planet02_diffuse.png"],
	"warm":["planets/planet01_diffuse.png","planets/planet02_diffuse.png"],
	"desert":["planets/desert01_diffuse.png","planets/desert02_diffuse.png"],
	"ice_ball":["planets/tundra01_diffuse.png"],
	"lava":["planets/inferno01_diffuse.png"],
	"swamp":["planets/swamp01_diffuse.png"],
	"toxic":["planets/toxic01_diffuse.png"]
}
const PLANET_DIFFICULTY = {
	"terran":"EASY",
	"warm":"EASY",
	"desert":"DANGEROUS",
	"ice_ball":"DANGEROUS",
	"swamp":"DANGEROUS",
	"lava":"HARD",
	"toxic":"HARD"
}
const locales = ["en"]
const VEC_X = Vector2( 30,18)
const VEC_Y = Vector2(-30,18)


var species_selected := 0
var class_selected := 0
var player_name := tr("PLAYER")
var started := false
var save_files := []
var file_info := {}
var available_planets := []
var shop := []
var shopping := false
var sell_price := 0.2
var killed_by
var equipment_popup
var gui_used_slots := {}
var gui_center_position := Vector2(64,64)
var gui_min_position := Vector2(0,0)
#var thread := Thread.new()

var fullscreen
var maximized
var screen_size
var music
var music_volume
var sound
var sound_volume
var locale
var key_binds
var action_selected
var new_event


func start():
	var inventory := []
	var ofs := 0
	inventory.resize(INVENTORY[class_selected].size())
	for i in range(INVENTORY[class_selected].size()):
		inventory[i] = INVENTORY[class_selected][i].duplicate()
	
	Main.reset()
	Main.characters["player"] = Main.Character.new(player_name,1,0,STATS[species_selected].duplicate(),{},TRAITS[species_selected].duplicate(),inventory,Main.Part.new("empty",0,"/",null),"player",SPECIES[species_selected],Vector2(50,0),"player")
	for s in Stats.SKILLS:
		Main.characters["player"].skills[s] = 0
		if SKILLS[class_selected].has(s):
			Main.characters["player"].skills[s] += SKILLS[class_selected][s]
	for i in range(Main.characters["player"].inventory.size()):
		var equiped = false
		for _k in range(Main.characters["player"].inventory[i-ofs]["amount"]):
			if Parts.items[Main.characters["player"].inventory[i-ofs]["type"]].has("slot"):
				equiped = Main.equip(Main.characters["player"],i-ofs)
		ofs += int(equiped)
	Main.characters["player"].update()
	Main.characters["player"].hp = Main.characters["player"].stats["hp"]
	Main.characters["player"].sp = Main.characters["player"].stats["sp"]
	Main.characters["player"].ep = Main.characters["player"].stats["ep"]
	Main.characters["player"].update()
	Main.change_level("city",0,null)
	Main.text.push_color(Main.COLOR_DEFAULT)
	Main.text.add_text("\n"+tr("WELCOME_TEXT")+"\n"+tr("GET_STARTED_TEXT").format({"key":OS.get_scancode_string(Main.get_scancode("launch"))})+"\n\n")
	started()

func start_tutorial():
	start()
	var mission = Mission.new_tutorial()
	Main.mission = mission

func _select_planet(ID):
	Main.change_level(available_planets[ID]["type"],available_planets[ID]["level"],available_planets[ID]["position"])
	if Main.mission!=null:
		Main.mission.free()
		Main.mission = null
	if available_planets[ID].has("mission"):
		var mission = Mission.call("new_"+available_planets[ID]["mission"])
		Main.mission = mission
	started()

#warning-ignore:function_conflicts_variable
func started():
#	if thread.is_active():
#		thread.wait_to_finish()
	started = true
	killed_by = null
	Main.set_process(true)
	Main.set_process_input(true)
	update_gui()
	hide_all()
	HUD.show()

func change_planet(level):
	var timer = Timer.new()
	var num_planets = 3
	var num_items = 16+randi()%4
	available_planets.resize(num_planets)
	
	Main.set_process(false)
	Main.set_process_input(false)
	HUD.hide()
	Main.characters["player"].restore()
	
	for i in range(num_planets):
		var t = clamp(round(level*rand_range(0.25,0.67)+rand_range(-1.0,1.0)),0,LEVELS.size()-1)
		var type = LEVELS[t]
		var lvl = max(round(level*rand_range(0.8,1.2)+rand_range(-1.0,1.0)),1)
		available_planets[i] = {"type":type,"level":lvl,"texture":PLANET_TEXTURES[type][randi()%PLANET_TEXTURES[type].size()],"glow":"glow_"+type+".png","position":null,"difficulty":PLANET_DIFFICULTY[type]}
		if Mission.MISSIONS.has(type):
			available_planets[i]["mission"] = Mission.MISSIONS[type][randi()%Mission.MISSIONS[type].size()]
	
	shop.resize(num_items)
	for i in range(num_items):
		shop[i] = {"type":Generate.DROPS_STD[randi()%Generate.DROPS_STD.size()],"level":int(max((0.75*Generate.level+0.25*Main.characters["player"].level)*rand_range(0.9,1.05)+rand_range(-1.5,2.5),1)),"amount":1}
		if shop[i]["type"]=="metal_alloys":
			shop[i]["amount"] = randi()%6+2
	
	if Generate.type=="city":
		show_map()
	else:
		timer.set_wait_time(0.5)
		timer.set_one_shot(true)
		add_child(timer)
		timer.start()
		
		yield(timer,"timeout")
		timer.queue_free()
		Main.change_level("city",Main.characters["player"].level,Generate.get_random_position())
		started()

func game_over():
	var character = Main.player
	var title = Main.get_title(character)
	started = false
	Main.set_process(false)
	Main.set_process_input(false)
	HUD.hide()
	show_menu()
	get_node("Dead/Text").set_text(tr("YOU_DIED")+"\n")
	if killed_by!=null:
		var enemy_name = tr("LVL")+" "+str(killed_by.level)+" "+tr(killed_by.name)
		get_node("Dead/Text").add_text(tr("PLAYER_WAS_KILLED_BY").replace("<name>",character.name+" ("+tr("LVL")+" "+str(character.level)+" "+tr(character.species)+" "+tr(title)+")").replace("<by>",enemy_name)+"\n")
		if killed_by.faction=="monsters":
			if character.species=="HUMAN_MALE" || character.species=="HUMAN_FEMALE":
				get_node("Dead/Text").add_text("\n"+tr("FLESHY_PLAYER").replace("<monster>",tr(killed_by.name))+"\n")
			elif character.species=="ANDROID":
				get_node("Dead/Text").add_text("\n"+tr("MECH_PLAYER").replace("<monster>",tr(killed_by.name))+"\n")
		elif killed_by.faction=="pirates":
			get_node("Dead/Text").add_text("\n"+tr("PLAYER_PLUNDERED").replace("<monster>",tr(killed_by.name))+"\n")
		
	else:
		get_node("Dead/Text").add_text(tr("PLAYER_WAS_KILLED").replace("<name>",character.name+" ("+tr("LVL")+" "+str(character.level)+" "+tr(character.species)+" "+tr(title)+")")+"\n")
	get_node("Dead").show()
	get_node("Panel/VBoxContainer/Button5").hide()
	Music.fade_out(2.0)
	Music.change_music("menu/game_over.ogg")
	Music.queue("menu/Juhani_Junkala_[Chiptune_Adventures]_4_Stage_Select.ogg")


func _quit():
	if started:
		Main._save()
	save_config()
	get_tree().quit()

func _load(ID):
	var success
	if started:
		Main._save()
	success = Main._load(save_files[ID])
	if success:
		started()

func update_save_files(info=true):
	var dir = Directory.new()
	var error = dir.open("user://saves")
	save_files.clear()
	if error!=OK:
		printt("Can't open directory user://saves!")
		return
	
	var filename
	dir.list_dir_begin(true)
	filename = dir.get_next()
	while filename!="":
		save_files.push_back(filename)
		if info:
			var file = File.new()
			var err = file.open("user://saves/"+filename,File.READ)
			if err==OK:
				var currentline = JSON.parse(file.get_line())
				currentline = JSON.parse(file.get_line())
				if currentline.error==OK:
					currentline = currentline.result
					if currentline.has("player"):
						file_info[filename] = {}
						if currentline["player"].has("level"):
							file_info[filename]["level"] = currentline["player"]["level"]
						if currentline["player"].has("title"):
							file_info[filename]["title"] = currentline["player"]["title"]
						currentline = JSON.parse(file.get_line())
						if currentline.error==OK:
							currentline = currentline.result
							if currentline.has("type") && currentline.has("level"):
								file_info[filename]["location"] = currentline["type"]
								file_info[filename]["difficulty"] = currentline["level"]
				file.close()
		filename = dir.get_next()
	dir.list_dir_end()


func find_hud_pos(pos0):
	if !gui_used_slots.has(pos0):
		return pos0
	
	for k in range(1,100):
		if !gui_used_slots.has(pos0+k*Vector2(-1,-1)):
			return pos0+k*Vector2(-1,-1)
		elif !gui_used_slots.has(pos0+k*Vector2(0,-1)):
			return pos0+k*Vector2(0,-1)
		elif !gui_used_slots.has(pos0+k*Vector2(-1,0)):
			return pos0+k*Vector2(-1,0)
		elif !gui_used_slots.has(pos0+k*Vector2(0,1)):
			return pos0+k*Vector2(0,1)
		elif !gui_used_slots.has(pos0+k*Vector2(1,0)):
			return pos0+k*Vector2(1,0)
		elif !gui_used_slots.has(pos0+k*Vector2(1,1)):
			return pos0+k*Vector2(1,1)
	return pos0

func create_parts(equipment):
	gui_used_slots.clear()
	gui_min_position = Vector2(0,0)
	if equipment.type=="empty":
		return
	
	var main = HUD.get_node("Components/Panel").duplicate()
	var damaged = clamp(1.0-equipment.hp/equipment.stats["hp"],0.0,1.0)
	main.tooltip = tr(equipment.type.to_upper())+"\n"+tr("CHASSIS")+equipment.path.replace("/",">")+"\n"
	if equipment.disabled:
		damaged = 1.0
		main.tooltip += tr("DISABLED")
	main.tooltip += tr("STRUCTURE")+": "+str(equipment.hp).pad_decimals(1)+"/"+str(equipment.stats["hp"]).pad_decimals(1)
	main.get_node("Icon").set_texture(load("res://images/icons/items/"+Parts.items[equipment.type]["icon"]+".png"))
	main.set_name("Main")
	main.set_position(gui_center_position)
	main.get_node("Structure").set_max(equipment.stats["hp"])
	main.get_node("Structure").set_value(equipment.hp)
	main.get_node("Icon").set_modulate(Color(1.0,1.0-damaged,max(1.0-1.25*damaged,0.0),0.5))
	main.get_node("Type").set_text(SLOT_SHORT[Parts.items[equipment.type]["slot"]])
	main.show()
	HUD.get_node("Components").add_child(main)
	HUD.get_node("Components").lines.clear()
	HUD.get_node("Status").parts = [equipment]
	HUD.get_node("Status").special_parts = {"W":[],"E":[],"D":[]}
	gui_used_slots[Vector2(0,0)] = main
	create_sub_part(equipment,Vector2(0,0))
	equipment.icon = main
	HUD.get_node("Components").update()
	HUD.get_node("Status").update()

func create_sub_part(part,last_pos):
	var num_slots = part.slots.size()
	var icons = []
	var positions = []
	icons.resize(num_slots)
	positions.resize(num_slots)
	for i in range(num_slots):
		var p = part.slots.values()[i]
		if p.type=="empty" || p.copy:
			continue
		
		var pos = find_hud_pos(last_pos)
		var damaged = 0
		var icon = HUD.get_node("Components/Panel").duplicate()
		var type = Parts.items[p.type]["type"]
		icon.get_node("Icon").set_texture(load("res://images/icons/items/"+Parts.items[p.type]["icon"]+".png"))
		icon.set_name(p.type.capitalize())
		icon.set_position(pos.x*VEC_X+pos.y*VEC_Y+gui_center_position)
		if p.stats.has("hp"):
			damaged = clamp(1.0-p.hp/p.stats["hp"],0.0,1.0)
			icon.get_node("Structure").set_max(p.stats["hp"])
			icon.get_node("Structure").set_value(p.hp)
		if p.disabled:
			damaged = 1.0
		icon.get_node("Icon").set_modulate(Color(1.0,1.0-damaged,max(1.0-1.25*damaged,0.0),0.5))
		icon.get_node("Type").set_text(SLOT_SHORT[Parts.items[p.type]["slot"].split("_")[0]])
		icon.show()
		icon.tooltip = tr(p.type.to_upper())+"\n"+p.path.substr(1,p.path.length()-1).replace("/",">")+"\n"
		if p.disabled:
			icon.tooltip += tr("DISABLED")
			icon.get_node("Disabled").show()
		elif p.stats.has("hp"):
			icon.tooltip += tr("STRUCTURE")+": "+str(p.hp).pad_decimals(1)+"/"+str(p.stats["hp"]).pad_decimals(1)
		HUD.get_node("Components").add_child(icon)
		HUD.get_node("Status").parts.push_back(p)
		if type=="weapon":
			HUD.get_node("Status").special_parts["W"].push_back(p)
		elif type=="propulsion":
			HUD.get_node("Status").special_parts["E"].push_back(p)
		elif type=="sensors":
			HUD.get_node("Status").special_parts["D"].push_back(p)
		gui_used_slots[pos] = icon
		p.icon = icon
		HUD.get_node("Components").lines.push_back([last_pos.x*VEC_X+last_pos.y*VEC_Y+gui_center_position+Vector2(18,16),icon.get_position()+Vector2(18,16)])
		if icon.get_position().x<gui_min_position.x:
			gui_min_position.x = icon.get_position().x
		if icon.get_position().y<gui_min_position.y:
			gui_min_position.y = icon.get_position().y
		icons[i] = icon
		positions[i] = pos
	# Add the further components recursively in a seperate for loop to ensure components belonging to parent are close to each other.
	for i in range(num_slots):
		var p = part.slots.values()[i]
		if p.type=="empty" || p.copy:
			continue
		
		create_sub_part(p,positions[i])

func update_inventory():
	if !Main.characters.has("player"):
		return
	var mass = Stats.get_inventory_mass(Main.characters["player"].inventory)
	for c in get_node("Inventory/ScrollContainer/GridContainer").get_children():
		c.hide()
	for i in range(Main.characters["player"].inventory.size()):
		var bi
		var tooltip
		var item = Parts.items[Main.characters["player"].inventory[i]["type"]]
		var texture = load("res://images/icons/items/"+item["icon"]+".png")
		var lvl = Main.characters["player"].inventory[i]["level"]
		if !has_node("Inventory/ScrollContainer/GridContainer/Item"+str(i+1)):
			bi = get_node("Inventory/Item0").duplicate()
			bi.set_name("Item"+str(i+1))
			get_node("Inventory/ScrollContainer/GridContainer").add_child(bi)
		else:
			bi = get_node("Inventory/ScrollContainer/GridContainer/Item"+str(i+1))
		if texture==null && item.has("slot"):
			var s = item["slot"].replace("_","")
			for _i in range(10):
				s = s.replace(str(i),"")
			texture = load("res://images/icons/slots/"+s+".png")
		bi.get_node("Spacing").set_custom_minimum_size(Vector2(0,34*(i%2)))
		bi.get_node("Panel").ID = i
		bi.get_node("Panel/Icon").set_texture(texture)
		if Main.characters["player"].inventory[i]["level"]>0:
			bi.get_node("Panel/Level").set_text(tr("LVL")+str(lvl))
		else:
			bi.get_node("Panel/Level").set_text("")
		bi.get_node("Panel/Amount").set_text(str(Main.characters["player"].inventory[i]["amount"])+"x")
		if item.has("slot"):
			bi.get_node("Panel/Type").set_modulate(SLOT_COLOR[item["slot"].split("_")[0]])
			bi.get_node("Panel").slot = item["slot"]
		else:
			bi.get_node("Panel/Type").hide()
			bi.get_node("Panel").slot = ""
		if item.has("slots"):
			bi.get_node("Panel/Slots").slots = item["slots"]
			bi.get_node("Panel/Slots").update()
		else:
			bi.get_node("Panel/Slots").hide()
		bi.modulate = Color(1.0,1.0,1.0)
		bi.show()
		if lvl>0:
			tooltip = tr("LVL")+str(lvl)+" "
		else:
			tooltip = ""
		tooltip += tr(Main.characters["player"].inventory[i]["type"].to_upper())+" x"+str(Main.characters["player"].inventory[i]["amount"])+"\n\n"
		if item.has("slot"):
			tooltip += tr(item["type"].to_upper())+" ("+tr(item["slot"].to_upper())+")\n"
		if item.has("skill"):
			tooltip += tr("SKILL")+": "+tr(item["skill"].to_upper())+"\n"
		if item.has("slots"):
			tooltip += tr("SLOTS")+": "
			for s in item["slots"]:
				tooltip += SLOT_SHORT[s]+" "
			tooltip += "\n"
		if Main.characters["player"].inventory[i].has("hp") && item.has("hp"):
			bi.get_node("Panel/Structure").set_max(item["hp"])
			bi.get_node("Panel/Structure").set_value(Main.characters["player"].inventory[i]["hp"])
			bi.get_node("Panel/Structure").show()
			tooltip += tr("HP")+": "+str(Main.characters["player"].inventory[i]["hp"]).pad_decimals(1)+"/"+str(item["hp"]).pad_decimals(1)
			if Main.characters["player"].inventory[i]["hp"]<=0:
				tooltip += " ("+tr("DISABLED")+")\n"
			else:
				tooltip += "\n"
		else:
			bi.get_node("Panel/Structure").hide()
		for s in Stats.STATS_ALL:
			if s=="hp":
				continue
			if s in Stats.STATS_INC:
				if item.has(s):
					tooltip += tr(s.to_upper())+": "+str(item[s]*(0.9+0.1*lvl)).pad_decimals(1)+"\n"
				if item.has("local_"+s):
					tooltip += tr("LOCAL")+" "+tr(s.to_upper())+": "+str(item["local_"+s]*(0.9+0.1*lvl)).pad_decimals(1)+"\n"
				if item.has("global_"+s):
					tooltip += tr("GLOBAL")+" "+tr(s.to_upper())+": "+str(item["global_"+s]*(0.9+0.1*lvl)).pad_decimals(1)+"\n"
				if item.has("parent_"+s):
					tooltip += tr("PARENT")+" "+tr(s.to_upper())+": "+str(item["parent_"+s]*(0.9+0.1*lvl)).pad_decimals(1)+"\n"
			else:
				if item.has(s):
					tooltip += tr(s.to_upper())+": "+str(item[s])+"\n"
				if item.has("local_"+s):
					tooltip += tr("LOCAL")+" "+tr(s.to_upper())+": "+str(item["local_"+s])+"\n"
				if item.has("global_"+s):
					tooltip += tr("GLOBAL")+" "+tr(s.to_upper())+": "+str(item["global_"+s])+"\n"
				if item.has("parent_"+s):
					tooltip += tr("PARENT")+" "+tr(s.to_upper())+": "+str(item["parent_"+s])+"\n"
		for m in ["","local_","global_","parent_"]:
			if item.has(m+"status"):
				tooltip += tr("APPLY")+" "
				for st in item[m+"status"]:
					tooltip += tr(st.name.to_upper())+" "
				tooltip += "\n"
		
		if shopping:
			var price = sell_price*(1.0-Main.mission_penalty)*Parts.get_total_price(Main.characters["player"].inventory[i])
			bi.get_node("Panel/Price").set_text(str(price))
			bi.get_node("Panel/Price").show()
			tooltip += tr("PRICE")+": "+str(price)+"\n"
		else:
			bi.get_node("Panel/Price").hide()
		tooltip += tr(Main.characters["player"].inventory[i]["type"].to_upper()+"_DESC")
		bi.get_node("Panel").tooltip = tooltip
	
	get_node("Inventory").rect_size.y = 178+floor((OS.get_real_window_size().y-350)/68)*68
	get_node("Inventory/Mass").set_text(str(mass).pad_decimals(1)+"kg / "+str(Main.get_inventory_size())+"kg")
	get_node("Inventory/VScrollBar").set_max(max(get_node("Inventory/ScrollContainer/GridContainer").get_size().y-get_node("Inventory/ScrollContainer").get_size().y,0))
	get_node("Inventory").show()

func add_sub_parts(target,part,slot,path,type="Equipment",highlight=""):
	var bi = get_node(type+"/Item0").duplicate()
	var slot_type = slot
	for i in range(10):
		slot_type = slot_type.replace(str(i),"")
	bi.get_node("Control").set_custom_minimum_size(Vector2(58*path.split("/",false).size(),0))
	get_node(type+"/ScrollContainer/VBoxContainer").add_child(bi)
	bi.show()
	bi.get_node("Panel").path = path
	bi.get_node("Panel").slot_type = slot_type
	if part.type=="empty":
		bi.set_name(slot)
		bi.get_node("Panel/Icon").set_texture(load("res://images/icons/slots/"+slot_type+".png"))
		bi.get_node("Panel/Slot").set_text(SLOT_SHORT[slot_type]+" ")
		bi.get_node("Panel/Type").set_modulate(SLOT_COLOR[slot_type])
		bi.get_node("Panel/Name").hide()
		bi.get_node("Panel/Level").hide()
		if highlight!="" && slot_type!=highlight:
			bi.get_node("Panel").self_modulate = Color(0.5,0.5,0.5)
			bi.get_node("Panel/Icon").self_modulate = Color(0.5,0.5,0.5)
			bi.get_node("Panel/Type").self_modulate = Color(0.5,0.5,0.5)
			bi.get_node("Panel/Name").self_modulate = Color(0.5,0.5,0.5)
			bi.get_node("Panel/Level").self_modulate = Color(0.5,0.5,0.5)
		return
	
	var ID
	var item
	var tooltip
	var c = 0
	bi.set_name(part.type)
	if part.copy:
		var _path = part.parent.split("/",false)
		var pt = Main.characters[target].equipment
		for p in _path:
			pt = pt.slots[p]
		ID = pt.type
		item = Parts.items[pt.type]
		bi.set_modulate(Color(1.0,1.0,1.0,0.5))
		bi.get_node("Panel/Name").hide()
		bi.get_node("Panel/Level").hide()
	else:
		ID = part.type
		item = Parts.items[part["type"]]
		bi.get_node("Panel/Level").set_text(tr("LVL")+str(part.level))
	slot_type = item["slot"].split("_")[0]
	bi.get_node("Panel/Icon").set_texture(load("res://images/icons/items/"+item["icon"]+".png"))
	bi.get_node("Panel/Slot").set_text(SLOT_SHORT[slot_type]+" ")
	bi.get_node("Panel/Type").set_modulate(SLOT_COLOR[slot_type])
	if highlight!="" && slot_type!=highlight:
		bi.get_node("Panel/Icon").self_modulate = Color(0.5,0.5,0.5)
		bi.get_node("Panel/Slot").self_modulate = Color(0.5,0.5,0.5)
		bi.get_node("Panel/Type").self_modulate *= Color(0.5,0.5,0.5)
		bi.get_node("Panel/Name").self_modulate = Color(0.5,0.5,0.5)
		bi.get_node("Panel/Level").self_modulate = Color(0.5,0.5,0.5)
	if item.has("slots"):
		bi.get_node("Panel/Slots").slots = item["slots"]
		bi.get_node("Panel/Slots").update()
	else:
		bi.get_node("Panel/Slots").hide()
	bi.get_node("Panel/Name").set_text(tr(part.type.to_upper()))
	if part.level>0:
		tooltip = tr("LVL")+str(part.level)+" "
	else:
		tooltip = ""
	tooltip += tr(part.type.to_upper())+"\n\n"
	tooltip += tr(item["type"].to_upper())+" ("+tr(item["slot"].to_upper())+")\n"+tr("SKILL")+": "+tr(item["skill"].to_upper())+"\n"
	if part.stats.has("hp"):
		tooltip += tr("HP")+": "+str(part.hp).pad_decimals(1)+"/"+str(part.stats["hp"]).pad_decimals(1)
		bi.get_node("Panel/Structure").set_max(part.stats["hp"])
		bi.get_node("Panel/Structure").set_value(part.hp)
		bi.get_node("Panel/Structure").show()
		if part.disabled:
			tooltip += " ("+tr("DISABLED")+")\n"
		else:
			tooltip += "\n"
	elif part.disabled && !part.copy:
		bi.get_node("Panel/Structure").show()
		bi.get_node("Panel/Structure").set_value(0)
		tooltip += "-"+tr("DISABLED")+"-\n"
	else:
		bi.get_node("Panel/Structure").hide()
	for s in Stats.STATS_ALL:
		if s=="hp":
			continue
		if part.stats.has(s):
			tooltip += tr(s.to_upper())+": "+str(part.stats[s])+"\n"
		if part.stats.has("local_"+s):
			tooltip += tr("LOCAL")+" "+tr(s.to_upper())+": "+str(part.stats["local_"+s])+"\n"
		if part.stats.has("global_"+s):
			tooltip += tr("GLOBAL")+" "+tr(s.to_upper())+": "+str(part.stats["global_"+s])+"\n"
		if part.stats.has("parent_"+s):
			tooltip += tr("PARENT")+" "+tr(s.to_upper())+": "+str(part.stats["parent_"+s])+"\n"
	for m in ["","local_","global_","parent_"]:
		if item.has(m+"status"):
			tooltip += tr("APPLY")+" "
			for st in item[m+"status"]:
				tooltip += tr(st.name.to_upper())+" "
			tooltip += "\n"
	tooltip += tr(ID.to_upper()+"_DESC")
	bi.get_node("Panel").tooltip = tooltip
	for s in part.slots.keys():
		var ci := Control.new()
		add_sub_parts(target,part.slots[s],s,path+"/"+s,type,highlight)
		# Add spacings between the child nodes. Note that the VBoxContainer is using a spacing if -30.
		ci.set_custom_minimum_size(Vector2(0,30*(1+int(c<part.slots.size()-1))))
		ci.mouse_filter = Control.MOUSE_FILTER_IGNORE
		get_node(type+"/ScrollContainer/VBoxContainer").add_child(ci)
		c += 1

func update_equipment():
	var text = ""
	Main.characters["player"].update()
	text += tr("TOTAL_HP")+": "+str(Main.characters["player"].hp).pad_decimals(1)+"/"+str(Main.characters["player"].hp_max).pad_decimals(1)+"\n"
	for s in Stats.STATS_ALL:
		if Main.characters["player"].stats[s]==0:
			continue
		if typeof(Main.characters["player"].stats[s])==TYPE_REAL:
			text += tr(s.to_upper())+": "+str(Main.characters["player"].stats[s]).pad_decimals(1)+"\n"
		else:
			text += tr(s.to_upper())+": "+str(Main.characters["player"].stats[s])+"\n"
	get_node("/root/Menu/Equipment/Panel/Text").set_text(text)
	
	for c in get_node("Equipment/ScrollContainer/VBoxContainer").get_children():
		c.hide()
		c.queue_free()
	add_sub_parts("player",Main.characters["player"].equipment,"main","")
	
	get_node("Equipment/VBoxContainer/Label").set_text(Main.characters["player"].name+"\n"+tr("LVL")+str(Main.characters["player"].level)+" "+tr(Main.get_title("player")))
	if Main.characters["player"].stat_points>0 || Main.characters["player"].skill_points>0:
		get_node("Equipment/VBoxContainer/Label").text += "\n"+tr("LEVEL_UP")
	get_node("Equipment/VBoxContainer/Exp").set_value(Main.characters["player"].experience)
	get_node("Equipment/VBoxContainer/Exp").set_max(Main.characters["player"].get_max_exp())
	get_node("Equipment/VBoxContainer/Exp/Label").set_text(str(Main.characters["player"].experience).pad_decimals(0)+"/"+str(Main.characters["player"].get_max_exp()).pad_decimals(0))
	get_node("Equipment/Panel/Points").set_text(tr("STAT_POINTS")+": "+str(Main.characters["player"].stat_points)+"\n"+tr("SKILL_POINTS")+": "+str(Main.characters["player"].skill_points))
	get_node("Equipment").rect_size.y = 172+floor((OS.get_real_window_size().y-300)/68)*68
	get_node("Equipment/VScrollBar").set_max(get_node("Equipment/ScrollContainer/VBoxContainer").get_size().y-get_node("Equipment/ScrollContainer").get_size().y)
	get_node("Equipment").show()

func update_inspector():
	var text = ""
	if Main.view_character==null || !Main.characters.has(Main.view_character):
		return
	Main.characters[Main.view_character].update()
	text += tr("TOTAL_HP")+": "+str(Main.characters[Main.view_character].hp).pad_decimals(1)+"/"+str(Main.characters[Main.view_character].hp_max).pad_decimals(1)+"\n"
	for s in Stats.STATS_ALL:
		if Main.characters[Main.view_character].stats[s]==0:
			continue
		if typeof(Main.characters[Main.view_character].stats[s])==TYPE_REAL:
			text += tr(s.to_upper())+": "+str(Main.characters[Main.view_character].stats[s]).pad_decimals(1)+"\n"
		else:
			text += tr(s.to_upper())+": "+str(Main.characters[Main.view_character].stats[s])+"\n"
	get_node("/root/Menu/Inspect/Panel/Text").set_text(text)
	
	for c in get_node("Inspect/ScrollContainer/VBoxContainer").get_children():
		c.hide()
		c.queue_free()
	add_sub_parts(Main.view_character,Main.characters[Main.view_character].equipment,"main","","Inspect")
	
	get_node("Inspect/VBoxContainer/Label").set_text(Main.characters[Main.view_character].name+"\n"+tr("LVL")+str(Main.characters[Main.view_character].level)+" "+tr(Main.get_title(Main.view_character)))
	get_node("Inspect").rect_size.y = 172+floor((OS.get_real_window_size().y-300)/68)*68
	get_node("Inspect/VScrollBar").set_max(get_node("Inspect/ScrollContainer/VBoxContainer").get_size().y-get_node("Inspect/ScrollContainer").get_size().y)
	get_node("Inspect").show()

func update_gui():
	for c in HUD.get_node("Components").get_children():
		if c.get_name()=="Panel":
			continue
		c.hide()
	create_parts(Main.characters["player"].equipment)
	HUD.get_node("Components").set_position(-gui_min_position+Vector2(22,192))

func update_stats():
	for s in ["str","int","dex"]:
		get_node("Stats/VBoxContainer/"+s+"/Panel/Label").set_text(tr(s.to_upper()))
		get_node("Stats/VBoxContainer/"+s+"/Panel/Level").set_text(str(Main.characters["player"].base_stats[s]))
	get_node("Stats/Points").set_text(tr("STAT_POINTS")+": "+str(Main.characters["player"].stat_points))
	get_node("Stats").show()

func update_skills():
	for c in SKILL_CAT:
		for i in range(SKILL_CAT[c].size()):
			var s = SKILL_CAT[c][i]
			if s in Main.characters["player"].skills.keys():
				get_node("Skills/ScrollContainer/VBoxContainer/"+c.capitalize()+"/VBoxContainer/Skill"+str(i)+"/Panel/Label").set_text(tr(s.to_upper()))
				get_node("Skills/ScrollContainer/VBoxContainer/"+c.capitalize()+"/VBoxContainer/Skill"+str(i)+"/Panel/Level").set_text(str(Main.characters["player"].skills[s]))
	get_node("Skills/Text").set_text(tr("SKILL_POINTS")+": "+str(Main.characters["player"].skill_points))
	get_node("Skills").rect_size.y = 178+floor((OS.get_real_window_size().y-600)/68)*68
	get_node("Skills/VScrollBar").set_max(get_node("Skills/ScrollContainer/VBoxContainer").get_size().y-get_node("Skills/ScrollContainer").get_size().y)
	get_node("Skills").show()

func update_files():
	for c in get_node("Load/ScrollContainer/VBoxContainer").get_children():
		c.hide()
	for i in range(save_files.size()):
		var bi
		if !has_node("Load/ScrollContainer/VBoxContainer/File"+str(i)):
			bi = get_node("Load/ScrollContainer/VBoxContainer/File0").duplicate()
			bi.set_name("File"+str(i))
			get_node("Load/ScrollContainer/VBoxContainer").add_child(bi)
		else:
			bi = get_node("Load/ScrollContainer/VBoxContainer/File"+str(i))
		if !bi.get_node("Button").is_connected("pressed",self,"_load"):
			bi.get_node("Button").connect("pressed",self,"_load",[i])
		bi.get_node("Button/Label").set_text(save_files[i].split(".",true,1)[0])
		if file_info.has(save_files[i]):
			var info = file_info[save_files[i]]
			if info.has("title"):
				bi.get_node("Status").set_text(tr("LVL")+str(info["level"])+" "+tr(info["title"]))
			else:
				bi.get_node("Status").set_text(tr("LVL")+str(info["level"]))
			bi.get_node("Location").set_text(tr("LVL")+str(info["difficulty"])+" "+tr(info["location"].to_upper()))
		bi.show()

func update_map():
	for c in get_node("Map/ScrollContainer/HBoxContainer").get_children():
		c.hide()
	
	for i in range(available_planets.size()):
		var pi
		var mat
		var mission = ""
		if !has_node("Map/ScrollContainer/HBoxContainer/Planet"+str(i)):
			pi = get_node("Map/ScrollContainer/HBoxContainer/Planet0").duplicate(14)
			pi.set_name("Planet"+str(i))
			get_node("Map/ScrollContainer/HBoxContainer").add_child(pi)
		else:
			pi = get_node("Map/ScrollContainer/HBoxContainer/Planet"+str(i))
		if !pi.get_node("Desc/Button").is_connected("pressed",self,"_select_planet"):
			pi.get_node("Desc/Button").connect("pressed",self,"_select_planet",[i])
		if available_planets[i].has("mission"):
			mission = tr("MISSION")+": "+tr(available_planets[i]["mission"].to_upper())+"\n"+tr(available_planets[i]["mission"].to_upper()+"_DESC")
		pi.get_node("Panel/Label").set_text(tr(available_planets[i]["type"].to_upper()))
		pi.get_node("Desc/Label").set_text("   "+tr("LEVEL")+": "+str(available_planets[i]["level"])+"\n "+tr("DIFFICULTY")+": "+tr(available_planets[i]["difficulty"])+"\n\n"+mission+"\n\n"+tr(available_planets[i]["type"].to_upper()+"_DESC"))
		mat = pi.get_node("Panel/Icon/Planet").get_material().duplicate()
		mat.set_shader_param("tex",load("res://textures/"+available_planets[i]["texture"]))
		pi.get_node("Panel/Icon/Planet").set_material(mat)
		pi.get_node("Panel/Icon").set_texture(load("res://images/planets/"+available_planets[i]["glow"]))
		pi.show()
	
	get_node("Map").rect_size.x = 260+floor(0.75*(OS.get_real_window_size().x-200)/116)*116
	get_node("Map").rect_position.x = (OS.get_real_window_size().x-get_node("Map").rect_size.x)/2

func update_shop():
	for c in get_node("Shop/ScrollContainer/GridContainer").get_children():
		c.hide()
	
	for i in range(shop.size()):
		var bi
		var tooltip
		var item = Parts.items[shop[i]["type"]]
		var price = int(Parts.get_total_price(shop[i])*(1.0+Main.mission_penalty))
		var texture = load("res://images/icons/items/"+item["icon"]+".png")
		if !has_node("Shop/ScrollContainer/GridContainer/Item"+str(i+1)):
			bi = get_node("Shop/Item0").duplicate()
			bi.set_name("Item"+str(i+1))
			bi.get_node("Panel").connect("gui_input",self,"_buy_item",[i])
			get_node("Shop/ScrollContainer/GridContainer").add_child(bi)
		else:
			bi = get_node("Shop/ScrollContainer/GridContainer/Item"+str(i+1))
		if texture==null && item.has("slot"):
			var s = item["slot"].replace("_","")
			for _i in range(10):
				s = s.replace(str(i),"")
			texture = load("res://images/icons/slots/"+s+".png")
		bi.get_node("Spacing").set_custom_minimum_size(Vector2(0,34*(i%2)))
		bi.get_node("Panel/Icon").set_texture(texture)
		if shop[i]["level"]>0:
			bi.get_node("Panel/Level").set_text(tr("LVL")+str(shop[i]["level"]))
		else:
			bi.get_node("Panel/Level").set_text("")
		bi.get_node("Panel/Amount").set_text(str(shop[i]["amount"])+"x")
		bi.get_node("Panel/Price").set_text(str(price))
		if item.has("slot"):
			bi.get_node("Panel/Type").set_modulate(SLOT_COLOR[item["slot"].split("_")[0]])
			bi.get_node("Panel").slot = item["slot"]
		else:
			bi.get_node("Panel/Type").hide()
			bi.get_node("Panel").slot = ""
		if item.has("slots"):
			bi.get_node("Panel/Slots").slots = item["slots"]
			bi.get_node("Panel/Slots").update()
		else:
			bi.get_node("Panel/Slots").hide()
		bi.modulate = Color(1.0,1.0,1.0)
		bi.show()
		if shop[i]["level"]>0:
			tooltip = tr("LVL")+str(shop[i]["level"])+" "
		else:
			tooltip = ""
		tooltip += tr(shop[i]["type"].to_upper())+" x"+str(shop[i]["amount"])+"\n\n"
		if item.has("slot"):
			tooltip += tr(item["type"].to_upper())+" ("+tr(item["slot"].to_upper())+")\n"
		if item.has("skill"):
			tooltip += tr("SKILL")+": "+tr(item["skill"].to_upper())+"\n"
		if item.has("slots"):
			tooltip += tr("SLOTS")+": "
			for s in item["slots"]:
				tooltip += SLOT_SHORT[s]+" "
			tooltip += "\n"
		for s in Stats.STATS_ALL:
			if s in Stats.STATS_INC:
				if item.has(s):
					tooltip += tr(s.to_upper())+": "+str(item[s]*(0.9+0.1*shop[i]["level"])).pad_decimals(1)+"\n"
				if item.has("local_"+s):
					tooltip += tr("LOCAL")+" "+tr(s.to_upper())+": "+str(item["local_"+s]*(0.9+0.1*shop[i]["level"])).pad_decimals(1)+"\n"
				if item.has("global_"+s):
					tooltip += tr("GLOBAL")+" "+tr(s.to_upper())+": "+str(item["global_"+s]*(0.9+0.1*shop[i]["level"])).pad_decimals(1)+"\n"
				if item.has("parent_"+s):
					tooltip += tr("PARENT")+" "+tr(s.to_upper())+": "+str(item["parent_"+s]*(0.9+0.1*shop[i]["level"])).pad_decimals(1)+"\n"
			else:
				if item.has(s):
					tooltip += tr(s.to_upper())+": "+str(item[s])+"\n"
				if item.has("local_"+s):
					tooltip += tr("LOCAL")+" "+tr(s.to_upper())+": "+str(item["local_"+s])+"\n"
				if item.has("global_"+s):
					tooltip += tr("GLOBAL")+" "+tr(s.to_upper())+": "+str(item["global_"+s])+"\n"
				if item.has("parent_"+s):
					tooltip += tr("PARENT")+" "+tr(s.to_upper())+": "+str(item["parent_"+s])+"\n"
		for m in ["","local_","global_","parent_"]:
			if item.has(m+"status"):
				tooltip += tr("APPLY")+" "
				for st in item[m+"status"]:
					tooltip += tr(st.name.to_upper())+" "
				tooltip += "\n"
		tooltip += tr("PRICE")+": "+str(price)+"\n"
		tooltip += tr(shop[i]["type"].to_upper()+"_DESC")
		bi.get_node("Panel").tooltip = tooltip
	
	get_node("Shop").rect_size.y = 178+floor((OS.get_real_window_size().y-350)/68)*68
	get_node("Shop/VScrollBar").set_max(get_node("Shop/ScrollContainer/GridContainer").get_size().y-get_node("Shop/ScrollContainer").get_size().y)
	get_node("Shop").show()

func show_tooltip(text):
	get_node("Tooltip/Text").set_text(text)
	get_node("Tooltip").set_size(Vector2(256,20+24*(get_node("Tooltip/Text").get_line_count()+1)))
	get_node("Tooltip").set_position(Vector2(clamp(get_node("Tooltip").get_global_mouse_position().x,0,OS.get_window_size().x-get_node("Tooltip").get_size().x),clamp(get_node("Tooltip").get_global_mouse_position().y,0,OS.get_window_size().y-get_node("Tooltip").get_size().y)))
	get_node("Tooltip").show()

func _hide_tooltip():
	get_node("Tooltip").hide()
	reset_highlight()

func highlight_slot_type(type,no_equip:=false):
	for i in range(Main.characters["player"].inventory.size()):
		if !has_node("Inventory/ScrollContainer/GridContainer/Item"+str(i+1)):
			continue
		var bi = get_node("Inventory/ScrollContainer/GridContainer/Item"+str(i+1))
		var item = Parts.items[Main.characters["player"].inventory[i]["type"]]
		if item.has("slot"):
			var slot = item["slot"]
			if slot==type:
				bi.modulate = Color(1.0,1.0,1.0)
			else:
				bi.modulate = Color(0.5,0.5,0.5)
		elif type=="":
			bi.modulate = Color(1.0,1.0,1.0)
		else:
			bi.modulate = Color(0.5,0.5,0.5)
	for i in range(shop.size()):
		if !has_node("Shop/ScrollContainer/GridContainer/Item"+str(i+1)):
			continue
		var bi = get_node("Shop/ScrollContainer/GridContainer/Item"+str(i+1))
		var item = Parts.items[shop[i]["type"]]
		if item.has("slot"):
			var slot = item["slot"]
			if slot==type:
				bi.modulate = Color(1.0,1.0,1.0)
			else:
				bi.modulate = Color(0.5,0.5,0.5)
		elif type=="":
			bi.modulate = Color(1.0,1.0,1.0)
		else:
			bi.modulate = Color(0.5,0.5,0.5)
	
	if !no_equip:
		for c in get_node("Equipment/ScrollContainer/VBoxContainer").get_children():
			c.hide()
			c.queue_free()
		add_sub_parts("player",Main.characters["player"].equipment,"main","","Equipment",type)

func reset_highlight():
	for c in get_node("Inventory/ScrollContainer/GridContainer").get_children():
		c.modulate = Color(1.0,1.0,1.0)
	for c in get_node("Shop/ScrollContainer/GridContainer").get_children():
		c.modulate = Color(1.0,1.0,1.0)
	for c in get_node("Equipment/ScrollContainer/VBoxContainer").get_children():
		c.hide()
		c.queue_free()
	add_sub_parts("player",Main.characters["player"].equipment,"main","")


func select_item(ID):
	var item = Parts.items[Main.characters["player"].inventory[ID]["type"]]
	if item.has("slot"):
		var slot = item["slot"].split("_")[0]
		var equip_paths = Main.find_paths("",Main.characters["player"].equipment,slot,[])
		var equip_paths_used = Main.find_used_paths("",Main.characters["player"].equipment,slot,[])
		for c in get_node("Inventory/PopupItem/VBoxContainer/ButtonE").get_children():
			if c is PopupMenu:
				equipment_popup = c
				break
		if equipment_popup!=null:
			equipment_popup.clear()
			if equipment_popup.is_connected("index_pressed",self,"_equip"):
				equipment_popup.disconnect("index_pressed",self,"_equip")
			equipment_popup.connect("index_pressed",self,"_equip",[ID])
			for i in range(equip_paths.size()):
				equipment_popup.add_item(tr("CHASSIS")+equip_paths[i].replace("/",">"))
				equipment_popup.set_item_metadata(i,equip_paths[i])
			for i in range(equip_paths_used.size()):
				equipment_popup.add_item(tr("CHASSIS")+equip_paths_used[i][0].replace("/",">")+" ("+tr("REPLACE")+")")
				equipment_popup.set_item_metadata(equip_paths.size()+i,equip_paths_used[i][0])
			get_node("Inventory/PopupItem/VBoxContainer/ButtonE").show()
		else:
			get_node("Inventory/PopupItem/VBoxContainer/ButtonE").hide()
	else:
		get_node("Inventory/PopupItem/VBoxContainer/ButtonE").hide()
	if get_node("Inventory/PopupItem/VBoxContainer/ButtonU").is_connected("pressed",self,"_use"):
		get_node("Inventory/PopupItem/VBoxContainer/ButtonU").disconnect("pressed",self,"_use")
	get_node("Inventory/PopupItem/VBoxContainer/ButtonU").connect("pressed",self,"_use",[ID])
	get_node("Inventory/PopupItem/VBoxContainer/ButtonU").set_visible(item.has("use"))
	if get_node("Inventory/PopupItem/VBoxContainer/ButtonD").is_connected("pressed",self,"_drop"):
		get_node("Inventory/PopupItem/VBoxContainer/ButtonD").disconnect("pressed",self,"_drop")
	get_node("Inventory/PopupItem/VBoxContainer/ButtonD").connect("pressed",self,"_drop",[ID])
	if shopping:
		if get_node("Inventory/PopupItem/VBoxContainer/ButtonS").is_connected("pressed",self,"sell_item"):
			get_node("Inventory/PopupItem/VBoxContainer/ButtonS").disconnect("pressed",self,"sell_item")
		get_node("Inventory/PopupItem/VBoxContainer/ButtonS").connect("pressed",self,"sell_item",[ID])
		get_node("Inventory/PopupItem/VBoxContainer/ButtonS").show()
	else:
		get_node("Inventory/PopupItem/VBoxContainer/ButtonS").hide()
	get_node("Inventory/PopupItem/VBoxContainer/Panel/Label").set_text(tr(Main.characters["player"].inventory[ID]["type"].to_upper())+" "+tr("LVL")+" "+str(Main.characters["player"].inventory[ID]["level"]))
	get_node("Inventory/PopupItem").popup(Rect2(get_node("Inventory/ScrollContainer/GridContainer/Item"+str(ID+1)).get_global_position(),Vector2(128,90)))
	

func _drop(ID):
	Main.drop_item("player",ID)
	update_inventory()
	get_node("Inventory/PopupItem").hide()

func _use(ID):
	var player = Main.characters["player"]
	var item = Parts.items[player.inventory[ID]["type"]]
	get_node("Inventory/PopupItem").hide()
	if !item.has("use") || !item.has("target"):
		return
	
	var success = false
	if item["target"]=="user_equipment_all":
		success = item["use"].call_func(player.equipment,player.inventory[ID]["level"],player.skills)
	elif item["target"]=="user":
		success = item["use"].call_func(player,player.inventory[ID]["level"],player.skills)
	
	if success && (!item.has("consumed") || item["consumed"]):
		Mission.on_item_used(ID)
		Main.remove_item(player,ID,1)
		update_inventory()

func _equip(idx,ID):
	if !Main.safe:
		Main.text.add_text(tr("CHANGE_EQUIPMENT_DISABLED")+"\n")
		return
	Main.equip(Main.characters["player"],ID,equipment_popup.get_item_metadata(idx))
	update_equipment()
	update_inventory()
	update_gui()
	get_node("Inventory/PopupItem").hide()

func equip_to(ID,path):
	if !Main.safe:
		Main.text.add_text(tr("CHANGE_EQUIPMENT_DISABLED")+"\n")
		return
	Main.equip(Main.characters["player"],ID,path)
	update_equipment()
	update_inventory()
	update_gui()

func unequip(path):
	if !Main.safe:
		Main.text.add_text(tr("CHANGE_EQUIPMENT_DISABLED")+"\n")
		return
	Main.unequip(Main.characters["player"],path)
	update_equipment()
	update_inventory()
	update_gui()

func _sell_item(event,ID):
	if event is InputEventMouseButton && event.is_pressed() && event.button_index==BUTTON_LEFT:
		if Main.characters["player"].inventory[ID]["type"]=="gold":
			return
		
		Main.add_item(Main.characters["player"],"gold",0,int(Parts.get_total_price(Main.characters["player"].inventory[ID])*sell_price*(1.0-Main.mission_penalty)))
		shop.push_back(Main.characters["player"].inventory[ID])
		Main.characters["player"].inventory.remove(ID)
		update_map()

func sell_item(ID):
	if Main.characters["player"].inventory[ID]["type"]=="gold":
		return
	
	Main.add_item(Main.characters["player"],"gold",0,int(Parts.get_total_price(Main.characters["player"].inventory[ID])*sell_price*(1.0-Main.mission_penalty)))
	shop.push_back(Main.characters["player"].inventory[ID])
	Main.characters["player"].inventory.remove(ID)
	update_shop()
	update_inventory()
	get_node("Inventory/PopupItem").hide()

func _buy_item(event,ID):
	if event is InputEventMouseButton && event.is_pressed() && event.button_index==BUTTON_LEFT:
		var has = Main.rm_item(Main.characters["player"],"gold",0,int(Parts.get_total_price(shop[ID])*(1.0+Main.mission_penalty)))
		if !has:
			return
		
		Main.add_item(Main.characters["player"],shop[ID]["type"],shop[ID]["level"],shop[ID]["amount"])
		shop.remove(ID)
		update_shop()
		update_inventory()

func buy_item(ID):
	var has = Main.rm_item(Main.characters["player"],"gold",0,int(Parts.get_total_price(shop[ID])*(1.0+Main.mission_penalty)))
	if !has:
		return
	
	Main.add_item(Main.characters["player"],shop[ID]["type"],shop[ID]["level"],shop[ID]["amount"])
	shop.remove(ID)
	update_shop()
	update_inventory()

func _scroll_inventory(value):
	get_node("Inventory/ScrollContainer").set_v_scroll(value)

func _scroll_update_inventory():
	get_node("Inventory/VScrollBar").set_value(get_node("Inventory/ScrollContainer").get_v_scroll())

func _scroll_shop(value):
	get_node("Shop/ScrollContainer").set_v_scroll(value)

func _scroll_update_shop():
	get_node("Shop/VScrollBar").set_value(get_node("Shop/ScrollContainer").get_v_scroll())

func _scroll_v_equipment(value):
	get_node("Equipment/ScrollContainer").set_v_scroll(value)

func _scroll_h_equipment(value):
	get_node("Equipment/ScrollContainer").set_h_scroll(value)

func _scroll_update_equipment():
	get_node("Equipment/VScrollBar").set_value(get_node("Equipment/ScrollContainer").get_v_scroll())
	get_node("Equipment/HScrollBar").set_value(get_node("Equipment/ScrollContainer").get_h_scroll())

func _scroll_v_inspector(value):
	get_node("Inspect/ScrollContainer").set_v_scroll(value)

func _scroll_h_inspector(value):
	get_node("Inspect/ScrollContainer").set_h_scroll(value)

func _scroll_update_inspector():
	get_node("Inspect/VScrollBar").set_value(get_node("Inspect/ScrollContainer").get_v_scroll())
	get_node("Inspect/HScrollBar").set_value(get_node("Inspect/ScrollContainer").get_h_scroll())

func _scroll_skills(value):
	get_node("Skills/ScrollContainer").set_v_scroll(value)

func _scroll_update_skills():
	get_node("Skills/VScrollBar").set_value(get_node("Skills/ScrollContainer").get_v_scroll())

func _scroll_load(value):
	get_node("Load/ScrollContainer").set_v_scroll(value)

func _scroll_update_load():
	get_node("Load/VScrollBar").set_value(get_node("Load/ScrollContainer").get_v_scroll())

func _scroll_options(value):
	get_node("Options/ScrollContainer").set_v_scroll(value)

func _scroll_update_options():
	get_node("Options/VScrollBar").set_value(get_node("Options/ScrollContainer").get_v_scroll())

func _scroll_map(value):
	get_node("Map/ScrollContainer").set_h_scroll(value)

func _scroll_update_map():
	get_node("Map/HScrollBar").set_value(get_node("Map/ScrollContainer").get_h_scroll())


func _inc_base(stat):
	if Main.characters["player"].stat_points>0:
		Main.characters["player"].stat_points -= 1
		Main.characters["player"].base_stats[stat] += 1
		Main.characters["player"].update()
		update_stats()
		if get_node("Equipment").is_visible():
			update_equipment()

func _inc_skill(skill):
	if Main.characters["player"]["skill_points"]>0:
		Main.characters["player"]["skill_points"] -= 1
		Main.characters["player"]["skills"][skill] += 1
		update_skills()
		if get_node("Equipment").is_visible():
			update_equipment()


func show_menu():
	get_node("Block").show()
	get_node("Panel").show()
	get_node("NewGame").hide()
	get_node("Options").hide()
	get_node("Load").hide()
	get_node("Dead").hide()
	get_node("Credits").hide()
	if started:
		get_node("Panel/VBoxContainer/Button7").show()
	else:
		get_node("Panel/VBoxContainer/Button7").hide()

func _show_new_game():
	get_node("NewGame/Name/Overwritten").set_visible(player_name+".sav" in save_files)
	show_menu()
	get_node("NewGame").show()

func _show_load():
	update_save_files()
	update_files()
	show_menu()
	get_node("Load").rect_size.y = 178+floor((OS.get_real_window_size().y-350)/68)*68
	get_node("Load").show()

func _show_options():
	var idx = 0
	fullscreen = OS.is_window_fullscreen()
	maximized = OS.is_window_maximized()
	screen_size = OS.get_window_size()
	music = !AudioServer.is_bus_mute(1)
	music_volume = 100*db2linear(AudioServer.get_bus_volume_db(1))
	sound = !AudioServer.is_bus_mute(2)
	sound_volume = 100*db2linear(AudioServer.get_bus_volume_db(2))
	locale = TranslationServer.get_locale()
	for i in range(get_node("Options/ScrollContainer/Control/Language/HBoxContainer/OptionButton").get_item_count()):
		if get_node("Options/ScrollContainer/Control/Language/HBoxContainer/OptionButton").get_item_text(i)==locale:
			idx = i
			break
	restore_key_binds()
	update_key_binds()
	
	get_node("Options/ScrollContainer/Control/Video/CheckBox").set_pressed(fullscreen)
	get_node("Options/ScrollContainer/Control/Video/VBoxContainer/SpinBoxX").set_value(screen_size.x)
	get_node("Options/ScrollContainer/Control/Video/VBoxContainer/SpinBoxY").set_value(screen_size.y)
	get_node("Options/ScrollContainer/Control/Audio/Music").set_pressed(music)
	get_node("Options/ScrollContainer/Control/Audio/VBoxContainer/Music/SpinBox").set_value(music_volume)
	get_node("Options/ScrollContainer/Control/Audio/Sound").set_pressed(sound)
	get_node("Options/ScrollContainer/Control/Audio/VBoxContainer/Sound/SpinBox").set_value(sound_volume)
	get_node("Options/ScrollContainer/Control/Language/HBoxContainer/OptionButton").select(idx)
	get_node("Options/ScrollContainer/Control").set_custom_minimum_size(Vector2(0,get_node("Options/ScrollContainer/Control/Controls").get_size().y+get_node("Options/ScrollContainer/Control/Controls").get_position().y+6))
	show_menu()
	get_node("Options").show()

func _show_credits():
	show_menu()
	get_node("Credits").show()

func show_map():
	Music.change_music("menu/Map.ogg")
	update_map()
	get_node("Map").show()

func show_shop(shop_items):
	shop = shop_items
	update_inventory()
	update_shop()

func _show_add_key(action):
	action_selected = action
	new_event = null
	get_node("Options/AddKey/Panel/Label").set_text(tr(action.capitalize().to_upper()))
	get_node("Options/AddKey/Panel/LabelPressed").set_text(tr("PRESS_ANY_KEY"))
	get_node("Options/AddKey").show()

func hide_all():
	for c in get_children():
		if c.has_method("hide"):
			c.hide()
	get_node("Panel/VBoxContainer/Button5").show()
	get_node("Panel/VBoxContainer/Button2").set_text(tr("LOAD"))
	set_process(false)

func toggle_menu():
	if get_node("Map").is_visible():
		return
	if started && (!get_node("Inventory").is_visible() && !get_node("Equipment").is_visible() && !get_node("Inspect").is_visible() && !get_node("Stats").is_visible() && !get_node("Skills").is_visible()):
		if started && get_node("Panel").is_visible():
			hide_all()
		else:
			show_menu()
	else:
		get_node("Inventory").hide()
		get_node("Shop").hide()
		get_node("Equipment").hide()
		get_node("Inspect").hide()
		get_node("Stats").hide()
		get_node("Skills").hide()
		get_node("NewGame").hide()
		get_node("Options").hide()
		get_node("Load").hide()
		get_node("Dead").hide()
		get_node("Credits").hide()
		if started:
			hide_all()


func _options_accept():
	_options_apply()
	get_node("Options").hide()

func _options_apply():
	OS.set_window_size(screen_size)
	OS.set_window_maximized(maximized)
	OS.set_window_fullscreen(fullscreen)
	AudioServer.set_bus_mute(1,!music)
	AudioServer.set_bus_volume_db(1,linear2db(music_volume/100.0))
	AudioServer.set_bus_mute(2,!sound)
	AudioServer.set_bus_volume_db(2,linear2db(sound_volume/100.0))
	TranslationServer.set_locale(locale)
	update_key_binds()
	save_config()

func save_config():
	var file = ConfigFile.new()
	file.set_value("video","fullscreen",fullscreen)
	file.set_value("video","maximized",OS.is_window_maximized())
	file.set_value("video","screenw",screen_size.x)
	file.set_value("video","screenh",screen_size.y)
	file.set_value("audio","music",music)
	file.set_value("audio","music_volume",int(music_volume))
	file.set_value("audio","sound",music)
	file.set_value("audio","sound_volume",int(sound_volume))
	file.set_value("locale","locale",locale)
	file.set_value("controls","key_binds",key_binds)
	file.save("user://config.cfg")

func load_config():
	var file = ConfigFile.new()
	var default_locale = OS.get_locale()
	if !(default_locale in locales):
		default_locale = default_locale.split("_")[0]
		if !(default_locale in locales):
			default_locale = "en"
	file.load("user://config.cfg")
	fullscreen = file.get_value("video","fullscreen",false)
	maximized = file.get_value("video","maximized",true)
	screen_size = Vector2(file.get_value("video","screenw",1024),file.get_value("video","screenh",768))
	music = file.get_value("audio","music",true)
	music_volume = file.get_value("audio","music_volume",100)
	sound = file.get_value("audio","sound",true)
	sound_volume = file.get_value("audio","sound_volume",100)
	locale = file.get_value("locale","locale",default_locale)
	key_binds = file.get_value("controls","key_binds")
	_options_apply()

func restore_key_binds():
	key_binds = {}
	for action in ACTIONS:
		key_binds[action] = []+InputMap.get_action_list(action)
	for action in ACTIONS:
		for c in get_node("Options/ScrollContainer/Control/Controls/VBoxContainer/"+action.capitalize().replace(" ","")).get_children():
			c.hide()

func update_key_binds():
	if key_binds==null || key_binds.size()==0:
		restore_key_binds()
	else:
		for action in ACTIONS:
			for event in InputMap.get_action_list(action):
				if !(event in key_binds[action]):
					InputMap.action_erase_event(action,event)
			for event in key_binds[action]:
				if !InputMap.action_has_event(action,event):
					InputMap.action_add_event(action,event)
	for action in ACTIONS:
		var _name = action.capitalize().replace(" ","")
		var keys = InputMap.get_action_list(action)
		get_node("Options/ScrollContainer/Control/Controls/VBoxContainer/"+_name+"/Label").show()
		get_node("Options/ScrollContainer/Control/Controls/VBoxContainer/"+_name+"/ButtonAdd").show()
		for event in keys:
			if has_node("Options/ScrollContainer/Control/Controls/VBoxContainer/"+_name+"/Button_"+str(event.scancode)):
				get_node("Options/ScrollContainer/Control/Controls/VBoxContainer/"+_name+"/Button_"+str(event.scancode)).show()
			elif event is InputEventKey:
				var bi = get_node("Options/ScrollContainer/Control/Controls/VBoxContainer/Action/ButtonAdd").duplicate()
				bi.set_text(OS.get_scancode_string(event.scancode))
				bi.connect("pressed",self,"_remove_key",[action,event])
				bi.set_name("Button_"+str(event.scancode))
				get_node("Options/ScrollContainer/Control/Controls/VBoxContainer/"+_name).add_child(bi)
				bi.show()

func add_key(action,event):
	if key_binds[action].has(event):
		return
	
	var _name = action.capitalize().replace(" ","")
	key_binds[action].push_back(event)
	if !has_node("Options/ScrollContainer/Control/Controls/VBoxContainer/"+_name+"/Button_"+str(event.scancode)):
		var bi = get_node("Options/ScrollContainer/Control/Controls/VBoxContainer/Action/ButtonAdd").duplicate()
		bi.set_text(OS.get_scancode_string(event.scancode))
		bi.connect("pressed",self,"_remove_key",[action,event])
		bi.set_name("Button_"+str(event.scancode))
		get_node("Options/ScrollContainer/Control/Controls/VBoxContainer/"+_name).add_child(bi)
		bi.show()

func _remove_key(action,event):
	var _name = action.capitalize().replace(" ","")
	key_binds[action].erase(event)
	if has_node("Options/ScrollContainer/Control/Controls/VBoxContainer/"+_name+"/Button_"+str(event.scancode)):
		get_node("Options/ScrollContainer/Control/Controls/VBoxContainer/"+_name+"/Button_"+str(event.scancode)).queue_free()

func _add_new_key():
	add_key(action_selected,new_event)
	get_node("Options/AddKey").hide()

func _set_fullscreen(enabled):
	fullscreen = enabled

func _set_screenw(value):
	screen_size.x = value

func _set_screenh(value):
	screen_size.y = value

func _set_music(enabled):
	music = enabled

func _set_music_volume(value):
	music_volume = value

func _set_sound(enabled):
	sound = enabled

func _set_sound_volume(value):
	sound_volume = value

func _set_locale(idx):
	locale = get_node("Options/ScrollContainer/Control/Language/HBoxContainer/OptionButton").get_item_text(idx)


func select_species(ID):
	var text = ""
	for t in SPECIES_DESC[ID]:
		text += tr(t)+"\n"
	text += "\n"+tr("SPECIES_TRAITS")+":\n"
	for s in STATS[ID].keys():
		text += tr(s.to_upper())+":\t"+str(STATS[ID][s])+"\n"
	for t in TRAITS[ID].keys():
		text += tr(t.to_upper())+":\t"+str(TRAITS[ID][t])+"\n"
	get_node("NewGame/SpeciesDesc/Label").set_text(text)
	get_node("NewGame/SpeciesDesc/Title").set_text(tr(SPECIES[ID].to_upper()))
	species_selected = ID

func select_class(ID):
	var text = tr(CLASS_DESC[ID])
	text += "\n\n"+tr("CLASS_SKILLS")+":\n"
	for s in SKILLS[ID].keys():
		text += tr(s.to_upper())+":\t"+str(SKILLS[ID][s])+"\n"
	get_node("NewGame/ClassDesc/Label").set_text(text)
	get_node("NewGame/ClassDesc/Title").set_text(tr(CLASSES[ID]))
	$NewGame/ClassDesc/Warning.visible = ID>2
	class_selected = ID

func _set_name(new):
	player_name = new
	get_node("NewGame/Name/Overwritten").set_visible(player_name+".sav" in save_files)


func _notification(what):
	match what:
		MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
			_quit()
		MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
			toggle_menu()

func _input(event):
	if event is InputEventKey && event.is_pressed():
		if get_node("Options/AddKey").is_visible():
			new_event = event
			get_node("Options/AddKey/Panel/Label").set_text(OS.get_scancode_string(event.scancode))
			get_node("Options/AddKey/Panel/HBoxContainer/Button1").grab_focus()
		elif event.is_action("inventory") && started:
			update_inventory()
		elif event.is_action("equipment") && started:
			update_equipment()
		elif event.is_action("skills") && started:
			update_stats()
			update_skills()
		elif event.is_action("close_menus"):
			toggle_menu()

func shell_open(arg):
	OS.shell_open(arg)

func _main_save():
	Main._save()


func _set_title_screen_bg(_data=null):
#	Generate.generate_default("title_screen")
	
	Generate.wall_tiles.clear()
	Generate.ground_tiles.clear()
	for x in range(BG_WIDTH):
		for y in range(BG_HEIGHT):
			if started:
				return
			
			var num_zones = BG_ZONE_TILES.size()-1
			var zone = 1.0*y/BG_HEIGHT
			var w = 2.0*abs(1.0*x/BG_WIDTH-0.5)
			var type
			zone = zone*(1.0-w)+0.5*w
			zone = floor(clamp(zone+rand_range(-0.05,0.05),0.0,1.0)*num_zones)
			type = BG_ZONE_TILES[zone]
			
			Generate.ground_tiles[Vector2(x,y)] = type
			if Generate.OBSTACLES.has(type) && Generate.OBSTACLE_CHANCE.has(type) && randf()<Generate.OBSTACLE_CHANCE[type]:
				Generate.wall_tiles[Vector2(x,y)] = Generate.OBSTACLES[type][randi()%Generate.OBSTACLES[type].size()]
	
	for pos in Generate.ground_tiles.keys():
		if started:
			return
		
		var ti = BG_TILE_SCENES[Generate.ground_tiles[pos]].instance()
		var oi = Sprite.new()
		ti.set_name("Tile_"+str(pos.x)+"_"+str(pos.y))
		ti.set_z_index(-1)
		oi.set_name("Outlines")
		oi.set_texture(preload("res://images/ui/tile_outlines.png"))
		oi.set_self_modulate(Color(1.0,1.0,1.0,0.5))
		oi.hide()
		ti.add_child(oi)
		Main.tiles.add_child(ti)
	
	for pos in Generate.wall_tiles.keys():
		if started:
			return
		
		if !Main.tiles.has_node("Tile_"+str(pos.x)+"_"+str(pos.y)):
			continue
		var ti = load("res://scenes/tiles/"+Generate.wall_tiles[pos]+".tscn")
		if ti==null || !ti.can_instance():
			continue
		
		ti = ti.instance()
		ti.set_z_index(1)
		ti.position = Vector2(rand_range(0.0,8.0),0.0).rotated(2.0*PI*randf())
		ti.position = Vector2(round(ti.get_position().x),round(ti.get_position().y))
		Main.tiles.get_node("Tile_"+str(pos.x)+"_"+str(pos.y)).add_child(ti)
	
	call_deferred("_title_screen_bg_done")

func _title_screen_bg_done(_data=null):
	if started:
		return
	set_process(true)
#	Generate.update_tiles()
	Main.get_node("AnimationPlayer").play("fade_in")


func _process(delta):
	var lp = Vector2(round(Main.position.x),round(Main.position.y))
	Main.position.y -= 0.75*delta
	Main.position.x -= 0.75*delta
	if Main.position.x>Generate.width:
		Main.position.x -= Generate.width
	elif Main.position.x<0:
		Main.position.x += Generate.width
	if Main.position.y>Generate.height:
		Main.position.y -= Generate.height
	elif Main.position.y<0:
		Main.position.y += Generate.height
	
	if lp.x!=round(Main.position.x) || lp.y!=round(Main.position.y):
		Generate.update_tiles()
	
	# move camera
	Main.camera.position = 16.0*(Main.position.x*Generate.TILE_VEC_X+Main.position.y*Generate.TILE_VEC_Y)
	Main.camera.position = Vector2(round(Main.camera.position.x),round(Main.camera.position.y+32.0))
	

func _ready():
	var dir = Directory.new()
	set_process_input(true)
	
	# create user directory and list save files
	if !dir.dir_exists("user://"):
		dir.make_dir_recursive("user://")
	if !dir.dir_exists("user://saves"):
		dir.make_dir_recursive("user://saves")
	
	for i in range(SPECIES.size()):
		var bi := get_node("NewGame/Button0").duplicate()
		bi.set_text(tr(SPECIES[i].to_upper()))
		bi.set_name("Button"+str(i+1))
		bi.show()
		bi.connect("pressed",self,"select_species",[i])
		get_node("NewGame/Species/VBoxContainer").add_child(bi)
	
	for i in range(CLASSES.size()):
		var bi := get_node("NewGame/Button0").duplicate()
		bi.set_text(tr(CLASSES[i].to_upper()))
		bi.set_name("Button"+str(i+1))
		bi.show()
		bi.connect("pressed",self,"select_class",[i])
		get_node("NewGame/Class/VBoxContainer").add_child(bi)
	
	for s in Stats.STATS_BASE:
		var bi := get_node("Stats/Base").duplicate()
		bi.set_name(s)
		bi.get_node("Panel/Label").set_text(tr(s.to_upper()))
		bi.show()
		bi.get_node("Button").connect("pressed",self,"_inc_base",[s])
		bi.tooltip = tr(s.to_upper()+"_DESC")+"\n"
		get_node("Stats/VBoxContainer").add_child(bi)
	
	for c in SKILL_CAT.keys():
		var ci := get_node("Skills/Base").duplicate()
		ci.set_name(c.capitalize())
		for i in range(SKILL_CAT[c].size()):
			var bi
			var s = SKILL_CAT[c][i]
			if i==0:
				bi = ci.get_node("VBoxContainer/Skill0")
			else:
				bi = ci.get_node("VBoxContainer/Skill0").duplicate()
				bi.set_name("Skill"+str(i))
			bi.get_node("Panel/Label").set_text(tr(s.to_upper()))
			bi.get_node("Panel/Icon").set_texture(load("res://images/icons/skills/"+s+".png"))
			bi.get_node("Button").connect("pressed",self,"_inc_skill",[s])
			bi.tooltip = tr(s.to_upper())+"\n"+tr(s.to_upper()+"_DESC")+"\n\n"
			if i>0:
				ci.get_node("VBoxContainer").add_child(bi)
		ci.set_custom_minimum_size(Vector2(74,64+68*SKILL_CAT[c].size()))
		get_node("Skills/ScrollContainer/VBoxContainer").add_child(ci)
		ci.show()
	
	for action in ACTIONS:
		var string = action.capitalize().replace(" ","_")
		var button := get_node("Options/ScrollContainer/Control/Controls/VBoxContainer/Action").duplicate()
		button.set_name(string.replace("_",""))
		button.get_node("Label").set_text(tr(string.to_upper()))
		button.get_node("ButtonAdd").connect("pressed",self,"_show_add_key",[action])
		get_node("Options/ScrollContainer/Control/Controls/VBoxContainer").add_child(button)
		button.show()
	get_node("Options/ScrollContainer/Control/Controls").set_custom_minimum_size(Vector2(0,64+28*get_node("Options/ScrollContainer/Control/Controls/VBoxContainer").get_child_count()))
	
	get_node("Version").set_text(Main.VERSION)
	if OS.has_feature("mobile"):
		get_node("Options/ScrollContainer/Control/Video/VBoxContainer").hide()
	if OS.has_feature("web"):
		get_node("Panel/VBoxContainer/Button4").hide()
	
	load_config()
	select_species(0)
	select_class(0)
	update_save_files()
	show_menu()
	
	# connect buttons
	get_node("Panel/VBoxContainer/Button1").connect("pressed",self,"_show_new_game")
	get_node("Panel/VBoxContainer/Button2").connect("pressed",self,"_show_load")
	get_node("Panel/VBoxContainer/Button3").connect("pressed",self,"_show_options")
	get_node("Panel/VBoxContainer/Button4").connect("pressed",self,"_quit")
	get_node("Panel/VBoxContainer/Button6").connect("pressed",self,"_show_credits")
	get_node("Panel/VBoxContainer/Button5").connect("pressed",self,"hide_all")
	get_node("Panel/VBoxContainer/Button7").connect("pressed",self,"_main_save")
	
	get_node("NewGame/HBoxContainer/Button1").connect("pressed",self,"show_menu")
	get_node("NewGame/HBoxContainer/Button2").connect("pressed",self,"start")
	get_node("NewGame/HBoxContainer/Button3").connect("pressed",self,"start_tutorial")
	get_node("NewGame/Close").connect("pressed",self,"show_menu")
	get_node("Inventory/VScrollBar").connect("value_changed",self,"_scroll_inventory")
	get_node("Inventory/ScrollContainer").connect("scroll_ended",self,"_scroll_update_inventory")
	get_node("Inventory/ScrollContainer").connect("scroll_started",self,"_scroll_update_inventory")
	get_node("Inventory/Close").connect("pressed",get_node("Inventory"),"hide")
	get_node("Shop/VScrollBar").connect("value_changed",self,"_scroll_shop")
	get_node("Shop/ScrollContainer").connect("scroll_ended",self,"_scroll_update_shop")
	get_node("Shop/ScrollContainer").connect("scroll_started",self,"_scroll_update_shop")
	get_node("Shop/Close").connect("pressed",get_node("Shop"),"hide")
	get_node("Equipment/Close").connect("pressed",get_node("Equipment"),"hide")
	get_node("Equipment/VScrollBar").connect("value_changed",self,"_scroll_v_equipment")
	get_node("Equipment/HScrollBar").connect("value_changed",self,"_scroll_h_equipment")
	get_node("Equipment/ScrollContainer").connect("scroll_ended",self,"_scroll_update_equipment")
	get_node("Equipment/ScrollContainer").connect("scroll_started",self,"_scroll_update_equipment")
	get_node("Inspect/Close").connect("pressed",get_node("Inspect"),"hide")
	get_node("Inspect/VScrollBar").connect("value_changed",self,"_scroll_v_equipment")
	get_node("Inspect/HScrollBar").connect("value_changed",self,"_scroll_h_equipment")
	get_node("Inspect/ScrollContainer").connect("scroll_ended",self,"_scroll_update_equipment")
	get_node("Inspect/ScrollContainer").connect("scroll_started",self,"_scroll_update_equipment")
	get_node("Stats/Close").connect("pressed",get_node("Stats"),"hide")
	get_node("Skills/Close").connect("pressed",get_node("Skills"),"hide")
	get_node("Skills/VScrollBar").connect("value_changed",self,"_scroll_skills")
	get_node("Skills/ScrollContainer").connect("scroll_ended",self,"_scroll_update_skills")
	get_node("Skills/ScrollContainer").connect("scroll_started",self,"_scroll_update_skills")
	get_node("Load/Close").connect("pressed",get_node("Load"),"hide")
	get_node("Load/VScrollBar").connect("value_changed",self,"_scroll_load")
	get_node("Load/ScrollContainer").connect("scroll_ended",self,"_scroll_update_load")
	get_node("Load/ScrollContainer").connect("scroll_started",self,"_scroll_update_load")
	get_node("Dead/Close").connect("pressed",get_node("Dead"),"hide")
	get_node("Credits/Close").connect("pressed",get_node("Credits"),"hide")
	get_node("Map/HScrollBar").connect("value_changed",self,"_scroll_map")
	get_node("Map/ScrollContainer").connect("scroll_ended",self,"_scroll_update_map")
	get_node("Map/ScrollContainer").connect("scroll_started",self,"_scroll_update_map")
	get_node("Options/Close").connect("pressed",get_node("Options"),"hide")
	get_node("Options/VScrollBar").connect("value_changed",self,"_scroll_options")
	get_node("Options/ScrollContainer").connect("scroll_ended",self,"_scroll_update_options")
	get_node("Options/ScrollContainer").connect("scroll_started",self,"_scroll_update_options")
	get_node("Options/HBoxContainer/Button1").connect("pressed",self,"_options_accept")
	get_node("Options/HBoxContainer/Button2").connect("pressed",self,"_options_apply")
	get_node("Options/HBoxContainer/Button3").connect("pressed",get_node("Options"),"hide")
	get_node("Options/ScrollContainer/Control/Video/CheckBox").connect("toggled",self,"_set_fullscreen")
	get_node("Options/ScrollContainer/Control/Video/VBoxContainer/SpinBoxX").connect("value_changed",self,"_set_screenw")
	get_node("Options/ScrollContainer/Control/Video/VBoxContainer/SpinBoxY").connect("value_changed",self,"_set_screenh")
	get_node("Options/ScrollContainer/Control/Audio/Music").connect("toggled",self,"_set_music")
	get_node("Options/ScrollContainer/Control/Audio/VBoxContainer/Music/SpinBox").connect("value_changed",self,"_set_music_volume")
	get_node("Options/ScrollContainer/Control/Audio/Sound").connect("toggled",self,"_set_sound")
	get_node("Options/ScrollContainer/Control/Audio/VBoxContainer/Sound/SpinBox").connect("value_changed",self,"_set_sound_volume")
	get_node("Options/ScrollContainer/Control/Language/HBoxContainer/OptionButton").connect("item_selected",self,"_set_locale")
	get_node("Options/AddKey/Panel/HBoxContainer/Button1").connect("pressed",self,"_add_new_key")
	get_node("Options/AddKey/Panel/HBoxContainer/Button2").connect("pressed",get_node("Options/AddKey"),"hide")
	get_node("Options/AddKey/Panel/Close").connect("pressed",get_node("Options/AddKey"),"hide")
	
	# Set up credits.
	for i in range(locales.size()):
		get_node("Options/ScrollContainer/Control/Language/HBoxContainer/OptionButton").add_item(locales[i],i)
	
	get_node("Credits/Text").clear()
	get_node("Credits/Text").add_text(tr("ENGINE")+":\n Godot (")
	get_node("Credits/Text").append_bbcode("[url=https://godotengine.org]godotengine.org[/url]")
	get_node("Credits/Text").add_text(")\n\n"+tr("PROGRAMMING")+":\n - Viktor Hahn\n\n")
	get_node("Credits/Text").add_text(tr("GRAPHICS")+":\n - "+tr("PLAYER_CHAR_SPRITES")+" Stephen Challener (Redshrike) ")
	get_node("Credits/Text").append_bbcode("[url=https://opengameart.org/content]opengameart.org[/url]\n")
	get_node("Credits/Text").add_text(" - OceansDream\n - Viktor Hahn\n\n")
	get_node("Credits/Text").add_text(tr("MUSIC")+":\n - Metaruka\n - SketchyLogic\n - Juhani Junkala\n\n")
	get_node("Credits/Text").add_text(tr("SOUNDS")+":\n - Juhani Junkala\n\n")
	get_node("Credits/Text").add_text(tr("FONTS")+":\n - Jonas Hecksher\n - Pix3M")
	get_node("Credits/Text").connect("meta_clicked",self,"shell_open")
	
	
	# Set up title screen background.
	set_process(false)
	Main.get_node("TextureRect").set_modulate(Color(0.0,0.0,0.0,1.0))
	Main.get_node("Atmosphere").set_modulate(Color(0.2,0.6,1.0,1.0))
	Main.get_node("Atmosphere").material.set_shader_param("light",Color(0.8,0.8,0.6,0.5))
	Main.get_node("Atmosphere").material.set_shader_param("shadow",Color(0.0,0.3,0.5,0.5))
	Main.get_node("TextureRect").material.set_shader_param("light",Color(0.8,0.8,0.6,0.5))
	Main.get_node("TextureRect").material.set_shader_param("shadow",Color(0.0,0.3,0.5,0.5))
	Generate.level = 0
	Generate.type = "terran"
	Generate.width = 30
	Generate.height = 35
	Music.change_music("menu/Juhani_Junkala_[Chiptune_Adventures]_4_Stage_Select.ogg")
	yield(get_tree(),"idle_frame")
#	thread.start(self,"_set_title_screen_bg",null,Thread.PRIORITY_HIGH)
	_set_title_screen_bg()
