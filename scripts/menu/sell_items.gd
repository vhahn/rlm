extends ScrollContainer

func can_drop_data(_position,data):
	return data["type"]=="inventory"

func drop_data(_position,data):
	if data["type"]!="inventory":
		return
	
	get_node("/root/Menu").sell_item(data["ID"])
