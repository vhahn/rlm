extends HBoxContainer

var tooltip


func _show_tooltip():
	get_node("/root/Menu").show_tooltip(tooltip)

func _ready():
	connect("mouse_entered",self,"_show_tooltip")
	connect("mouse_exited",get_node("/root/Menu"),"_hide_tooltip")
