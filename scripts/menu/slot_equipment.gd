extends Control

var path
# warning-ignore:unused_class_variable
var slot_type
var tooltip


func get_drag_data(_position):
	if path==null:
		return
	
	var pi = self.duplicate(0)
	pi.mouse_filter = MOUSE_FILTER_IGNORE
	set_drag_preview(pi)
	return {"path":path,"type":"equipment"}

func can_drop_data(_position,data):
	return data["type"]=="inventory" || data["type"]=="equipment"

func drop_data(_position,data):
	if path==null:
		return
	if data["type"]=="inventory":
		get_node("/root/Menu").equip_to(data["ID"],path)
	elif data["type"]=="equipment":
		var ID
		var part = Main.get_part(data["path"],Main.characters["player"].equipment)
		var type = part.type
		var level = part.level
		get_node("/root/Menu").unequip(data["path"])
		ID = Main.find_item(Main.characters["player"],type,level)
		if (ID!=null):
			get_node("/root/Menu").equip_to(ID,path)

func _gui_input(event):
	if !(event is InputEventMouseButton && !event.is_pressed() && event.button_index==BUTTON_LEFT) || path==null:
		return
	
	get_node("/root/Menu").unequip(path)

func _show_tooltip():
	get_node("/root/Menu").highlight_slot_type(slot_type,true)
	if tooltip==null:
		return
	get_node("/root/Menu").show_tooltip(tooltip)

func _ready():
	connect("gui_input",self,"_gui_input")
	connect("mouse_entered",self,"_show_tooltip")
	connect("mouse_exited",get_node("/root/Menu"),"_hide_tooltip")
