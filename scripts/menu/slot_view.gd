extends Control

# warning-ignore:unused_class_variable
var path
# warning-ignore:unused_class_variable
var slot_type
var tooltip


func _show_tooltip():
	if tooltip==null:
		return
	get_node("/root/Menu").show_tooltip(tooltip)

func _ready():
	connect("mouse_entered",self,"_show_tooltip")
	connect("mouse_exited",get_node("/root/Menu"),"_hide_tooltip")
