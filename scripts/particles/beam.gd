extends Node2D

var creator
var target


func _ready():
	set_position(creator.get_global_position())
	if has_node("Sprite"):
		get_node("Sprite").look_at(target.get_global_position())
		get_node("Sprite").scale.x = get_global_position().distance_to(target.get_global_position())/32.0
