extends "res://scripts/particles/shot_base.gd"

onready var num_frames = get_node("Sprite").get_hframes()*get_node("Sprite").get_vframes()


func _process(_delta):
	get_node("Sprite").set_frame(weight*num_frames)
