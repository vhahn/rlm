extends Node2D

export (float) var speed = 0.5
var time := 0.0
var weight := 0.0
var creator
var target


func _process(delta):
	var dist = creator.get_global_position().distance_to(target.get_global_position())
	weight = min(time/speed*128.0/max(dist,1.0),1.0)
	time += delta
	set_position(creator.get_global_position()+weight*(target.get_global_position()-creator.get_global_position()))
	set_modulate(Color(1.0,1.0,1.0,1.0))
	if weight>=1.0:
		if has_node("Animation"):
			get_node("Animation").play("impact")
			set_process(false)
		else:
			get_node("Sound").connect("finished",self,"queue_free")
			get_node("Sound").play()
			set_modulate(Color(1.0,1.0,1.0,0.0))
			set_process(false)

func _ready():
	if has_node("Sprite"):
		get_node("Sprite").look_at(target.get_global_position())
